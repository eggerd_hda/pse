# Projekt Systementwicklung
*5\. Semester, 2016*  
*Aleksejs Okolovskis*  

> Abbildung einer dezentralen und datenschutzbewussten sozialen Plattform auf privat genutzten Online-Speichern

Ziel des Projekts war es eine soziale Plattform zu erstellen, bei der Datenschutz an erster Stelle steht. Bei herkömmlichen Plattformen, wie z.B. Facebook, werden alle Informationen auf einem Server abgelegt, um diese für andere Nutzer zugänglich zu machen – der Server dient also als eine zentrale Informationsquelle für alle Nutzer. Dies bringt jedoch das Problem mit sich, dass man die veröffentlichten Daten (Posts, Bilder oder auch nur Freundschaften) nicht nur seinen Freunden, sondern auch dem Betreiber der Plattform zugänglich macht.

Bei *Postbox* wird genau dies vermieden. Statt die geteilten Daten auf einem zentralen Server des Betreibers abzulegen, werden diese verschlüsselt und in der Dropbox des Benutzers gespeichert. Nur Freunde dieses Benutzers sind dann in der Lage die gespeicherten Daten aus der Dropbox zu laden und zu entschlüsseln.

Sämtliche Funktionalität, wie z.B. Ver- & Entschlüsselung oder das Laden von Daten, findet dabei direkt im Browser des Benutzers statt. Es existiert also keinerlei Kommunikation mit einem Server des Betreibers und somit erhält dieser auch zu keinem Zeitpunkt Zugriff auf die geteilten Daten.

Geplant war zudem die Unterstützung weiterer Cloud-Speicher, wie z.B. Google Drive oder OwnCloud. Aus zeitlichen Gründen wurde jedoch die Entwicklung und Verfeinerung des Chats priorisiert. Die Architektur des Projekts ist jedoch bereits dafür ausgelegt worden, auch weitere Cloud-Speicher zu unterstützen.


## Anforderungen
- Realisierung eines sozialen Netzwerks welches die gängigsten Funktionen umfasst:
    - Posten von Textnachrichten, Bildern oder Videos (nur für Freunde sichtbar)
    - Benutzerprofile die mindestens den Namen und ein Profilbild umfassen
    - Eine Timeline in welcher die eigenen Posts sowie die von Freunden in chronologischer Reihenfolge angezeigt werden
    - Die Möglichkeit mit einem oder mehreren Freunden gleichzeitig zu chatten
- Datenschutz soll an erster Stelle stehen und bei jeder Design-Entscheidung berücksichtigt werden
- Es darf keinen zentralen Server geben der Daten in irgendeiner Form verarbeitet oder speichert
- Unterstützung für gängige Cloud-Dienste, wie z.B. Dropbox, Google Drive, Owncloud etc.
- Die Daten eines Benutzers (wie z.B. Posts, Bilder, Freundesliste etc.) dürfen ausschließlich in dessen privater Cloud gespeichert werden
- Sämtliche Daten eines Benutzers müssen verschlüsselt gespeichert werden
- Die verwendete Verschlüsselung soll möglichst zukunftssicher aber dennoch einfach austauschbar sein
- Da der Speicherplatz von Cloud-Diensten beschränkt ist, sollten die gespeicherten Daten so weit wie möglich komprimiert werden

Sowie weitere implizite Anforderungen die sich im Laufe der Design-Phase oder während der Entwicklung noch ergeben haben.


## Endergebnis

##### Features
- Texte & Bilder mit Freunden teilen
- geteilte Texte & Bilder eines Freundes einsehen
- alle Posts (Freunde & eigene) in einer chronologischen Timeline betrachten
- Freunde durch Austausch eines URL hinzufügen
- (Gruppen-) Chat
- Gewährleistung der Integrität aller Daten
- Verschlüsselung der geposteten Inhalte mittels ECC & AES-256
- Komprimierung der geposteten Inhalte mittels LZMA


##### Dokumentation
Im [Wiki](https://gitlab.com/eggerd_hda/pse/-/wikis) sind zudem die folgenden Dokumentationen zu finden:
- [Abschlussdokumentation](https://gitlab.com/eggerd_hda/pse/-/wikis/final-documentation)
- [Systemarchitektur](https://gitlab.com/eggerd_hda/pse/-/wikis/architecture)
- [Verschlüsselung](https://gitlab.com/eggerd_hda/pse/-/wikis/encryption)


##### Screenshots
| Eigene Wall | Timeline | Freundesliste |
| --- | --- | --- |
| ![Wall](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_wall.png) | ![Timeline](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_timeline.png) | ![Freundesliste](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_friendslist.png) |

| Registration | (Gruppen-) Chat | Wall eines Freundes |
| --- | --- | --- |
| ![Registration](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_registration.png) | ![Chat](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_chat.png) | ![Profil](https://gitlab.com/eggerd_hda/pse/-/raw/assets/screenshot_friend_profile.png) |


## Demo
Zur Verwendung von Postbox wird ein kostenloser [Dropbox-Account](https://www.dropbox.com/register) benötigt. Zudem wird empfohlen einen der folgenden Browser zu verwenden, für die Postbox damals getestet wurde - neuere Versionen der genannten Browser sollten allerdings ebenfalls ohne Probleme funktionieren.
- Mozilla Firefox 50
- Google Chrome 54
- Opera 41

Unter [archiv.dustin-eckhardt.de](https://archiv.dustin-eckhardt.de/postbox) steht die aktuellste Version von Postbox zum Testen zur Verfügung.


## Beteiligte Personen
- [Daniel Huth](https://gitlab.com/DanielHuth)
- [Dennis Müßig](https://gitlab.com/dennismuessig)
- [Dustin Eckhardt](https://gitlab.com/eggerd)
- [Erkan Kondu](https://gitlab.com/erkank)
- [Fabian Bihn](https://gitlab.com/Been)
- [Fabian Spahn](https://gitlab.com/fabian1s)
- [Jonas P. Heß](https://gitlab.com/JonasHess)
- [Lukas Schardt](https://gitlab.com/lschardt)
- [Marouane Naghmouchi](https://gitlab.com/Maui88)
- [Ricardo Roth](https://gitlab.com/rixco)
- [Tim Bender](https://gitlab.com/TiBend)
- [Tim Kolberger](https://gitlab.com/TimKolb)


## Verwendete Software
- [Angular](https://www.npmjs.com/package/@angular/core) (2.1.0)
- [Crypto-JS](https://www.npmjs.com/package/crypto-js) (3.1.6)
- [Stanford Javascript Crypto Library (SJCL)](https://crypto.stanford.edu/sjcl/) (1.0.6)
- [LZMA](https://www.npmjs.com/package/lzma) (2.3.2)
- [Binary-Shannon-Entropy](https://www.npmjs.com/package/binary-shannon-entropy) (1.1.3)
- [Cropper.js](https://www.npmjs.com/package/cropperjs) (1.0.0-alpha)
- [Angular2-qrcode](https://www.npmjs.com/package/angular2-qrcode) (1.0.5)
- [Await-Semaphore](https://www.npmjs.com/package/await-semaphore) (0.1.1)
- [Core-JS](https://www.npmjs.com/package/core-js) (2.4.1)
- [jQuery](https://www.npmjs.com/package/jquery) (3.1.1)
- [jQuery Emoji Picker](https://www.npmjs.com/package/jquery-emoji-picker) (0.2.0)
- [Material Design Lite](https://www.npmjs.com/package/material-design-lite) (1.2.1)
- [Material Design Icons](material-design-icons) (3.0.1)
- [Moment](https://www.npmjs.com/package/moment) (2.17.1)
- [RxJS](https://www.npmjs.com/package/rxjs) (5.0.0-beta.12)
- [Zone.js](https://www.npmjs.com/package/zone.js) (0.6.23)