var gulp = require('gulp');
var sriHash = require('gulp-sri-hash');

gulp.task('add-integrity-checksums', function() {
  return gulp.src('./dist/index.html')
    .pipe(sriHash())
    .pipe(gulp.dest('./dist/'));
});
