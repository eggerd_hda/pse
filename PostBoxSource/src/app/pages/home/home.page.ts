import {Component} from "@angular/core";
import {Post} from "../../entities/post";
import {ChannelRepositoryService} from "../../repositories/channel-repository.service";

@Component({
  selector: 'HomePage',
  templateUrl: './home.page.html'
})
export class HomePage {
  private posts: Post[];

  constructor(private channelRepository: ChannelRepositoryService) {
    channelRepository.getAllPostsOfAllBuddies().subscribe(res => {
      this.posts = [];
      this.posts = res;
    });
  }

  ngOnInit(): void {
  }

}
