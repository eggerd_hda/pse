import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ICloudStorageAdapterInterface} from "../../../services/interfaces/cloud-storage-adapter.interface";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {BuddyRepositoryService} from "../../../repositories/buddy-repository.service";
import {CryptoService} from "../../../services/crypto.service";
import {FileSystemService, EntityType} from "../../../services/filesystem.service";
import {InitService} from "../../../repositories/init.service";
import {DropboxAdapterService} from "../../../services/dropbox-adapter.service";

@Component({
  selector: 'profile-login',
  templateUrl: 'profile-login.component.html',
})
export class ProfileLoginComponent implements OnInit {

  /**
   * Will be provided by the input text.
   */
  public userPassword: string;

  private errorMessage: string;
  private showError: boolean = false;

  private storageAdapter: ICloudStorageAdapterInterface;

  private loading: boolean = false;

  constructor(private profileRepository: ProfileRepositoryService,
              private buddyRepo: BuddyRepositoryService,
              private cryptoService: CryptoService,
              private fileSystem: FileSystemService,
              private initService: InitService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.storageAdapter = new DropboxAdapterService();
  }

  loginUser(): void {
    this.showError = false;
    this.loading = true;

    this.cryptoService.init(this.userPassword, this.storageAdapter).then(() => {
      this.fileSystem.initializeFromStorage(this.storageAdapter, this.buddyRepo).then(() => {
        // Save profile in profielRepo
        console.log("Init from storage done");
        this.fileSystem.load(EntityType.PROFILE).then(profile => {
          this.profileRepository.profile = profile;
          this.initService.initializeRepositories(this.profileRepository.profile).then(() => {
            this.showError = false;
            this.router.navigate(['/user', this.profileRepository.profile.id]);
          }).catch(error => {
            this.showErrorDialog(error);
            this.loading = false;
          });
        });
      }).catch(error => {
        this.showErrorDialog(error);
        this.loading = false;
      });
    }).catch(error => {
      this.showErrorDialog(error);
      this.loading = false;
    });
  }

  private showErrorDialog(error): void {
    this.errorMessage = "Das hat nicht funktioniert.";
    this.showError = true;
    console.error("[Profile-Login-Comp.] ", error);
  }

}
