import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ICloudStorageAdapterInterface} from "../../../services/interfaces/cloud-storage-adapter.interface";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {BuddyRepositoryService} from "../../../repositories/buddy-repository.service";
import {CryptoService} from "../../../services/crypto.service";
import {FileSystemService, EntityType} from "../../../services/filesystem.service";
import {InitService} from "../../../repositories/init.service";
import {DropboxAdapterService} from "../../../services/dropbox-adapter.service";


@Component({
  selector: 'profile-creation',
  templateUrl: 'profile-creation.component.html'
})
export class ProfileCreationComponent {

  /**
   * Will be provided by the input text.
   */
  userName: string;
  userPassword: string;

  loading: boolean = false;

  private storageAdapter: ICloudStorageAdapterInterface;

  constructor(private profileRepository: ProfileRepositoryService,
              private cryptoService: CryptoService,
              private router: Router,
              private initService: InitService,
              private buddyRepo: BuddyRepositoryService,
              private fileSystem: FileSystemService) {
    this.storageAdapter = new DropboxAdapterService();

  }

  createUser(): void {
    this.loading = true;

    this.cryptoService.init(this.userPassword, this.storageAdapter).then(() => {
      this.fileSystem.initalize(this.storageAdapter, this.buddyRepo).then(() => {
        console.log("Created publicNode");
        this.profileRepository.create(this.userName, this.userPassword).then(() => {
          this.initService.initializeRepositories(this.profileRepository.profile).then(() => {
            this.router.navigate(['/user', this.profileRepository.profile.id]);
          });
        }).catch(err => {
          console.log("could not create profile", err);
          this.loading = false;
        });
      }).catch(err => {
        console.log("Could not init fileSystem", err);
        this.loading = false;
      });
    });


  }
}
