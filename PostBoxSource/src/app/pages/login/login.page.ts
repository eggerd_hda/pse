import { Component } from '@angular/core';
import {ProfileRepositoryService} from "../../repositories/profile-repository.service";
import {Router} from "@angular/router";
import {ICloudStorageAdapterInterface} from "../../services/interfaces/cloud-storage-adapter.interface";
import {DropboxAdapterService} from "../../services/dropbox-adapter.service";
import {CryptoService} from "../../services/crypto.service";
import {environment} from "../../../environments/environment";
import {AddBuddyDialog} from "../buddylist/addBuddyDialog/add-buddy.dialog";
import {MdlDialogService} from "angular2-mdl";
import {ProfileImageUploadComponent} from "../user/image-upload-dialog/profile-image-upload.component";
import {EmojiService} from "../../services/emoji/emoji.service";

declare let $: any;

@Component({
  selector: 'LoginPage',
  templateUrl: './login.page.html'
})
export class LoginPage {

  private isAuthenticated: boolean = false;
  private showProfileCreation: boolean = false;
  private showLogin: boolean = false;

  /**
   * The cloud adapter. We need it here to check whether
   * or not the current user is authenticated.
   */
  private cloudAdapter: ICloudStorageAdapterInterface;

  constructor(private profileRepository: ProfileRepositoryService,
              private router: Router,
              private dialogService: MdlDialogService,
              dropboxAdapter: DropboxAdapterService) {

    this.cloudAdapter = dropboxAdapter;

    this.isAuthenticated = this.cloudAdapter.isAuthenticated();

    if (this.isAuthenticated) {
      // load the data for the current user.
      //localStorage["userToken"]
      this.loadUserData();
    }
  }

  toUnicode(code): string {
    let codes = code.split('-').map(function(value, index) {
      return parseInt(value, 16);
    });
    return String.fromCodePoint.apply(null, codes);
  }

  /**
   * We will load the current profile and check if it already exists.
   * If not we give the user the option to create the profile.
   */
  loadUserData(): void {


    /**
      TODO: If you want to show the username on login
     this.cloudAdapter.getFile(environment.PUBLIC_NODE).then(publicNode => {
     this.profileRepository.profile = new Profile();
     console.log("GOt pUblic", publicNode);
     publicNode = JSON.parse(publicNode);
     this.profileRepository.profile.name = publicNode.publicProfileInformations.userName;
     this.showLogin = true;
     }).catch(error => {
     console.log("The publicNode does not exist or json file is damaged -> registrationPage is displayed. Error: " + error);
     this.showProfileCreation = true;
     });
     */

    this.cloudAdapter.getFile(environment.PRIVATE_NODE)
      .then((serializedPrivateNode) => {
        console.log("the privateNode is existing -> loginPage is displayed!");
        this.showLogin = true;
      })
      .catch((error) => {
        console.log("The privateNode does not exist or json file is damaged -> registrationPage is displayed. Error: " + error);
        this.showProfileCreation = true;
      });
  }

  onLogin(): void {

  }
}
