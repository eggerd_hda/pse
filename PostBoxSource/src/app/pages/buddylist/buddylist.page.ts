import {Component} from '@angular/core';
import {BuddyRepositoryService} from '../../repositories/buddy-repository.service';
import {Buddy} from '../../entities/buddy';
import {Router, CanActivate} from '@angular/router';
import {AddBuddyDialog} from './addBuddyDialog/add-buddy.dialog';
import {MdlDialogReference, MdlDialogService} from 'angular2-mdl';
import {AccordionSection} from "../../components/accordion/AccordionSection";

@Component({
  selector: 'BuddylistPage',
  templateUrl: './buddylist.page.html',
  styleUrls: ['./buddylist.page.scss']
})
export class BuddylistPageComponent {
  /**
   * Holds the sections for our accordion.
   *
   * @type {Array}
   */
  private accordionSections: AccordionSection[] = [];

  private buddies: Buddy[] = [];
  private invitations: Buddy[] = [];

  constructor(private router: Router,
              private buddyRepo: BuddyRepositoryService,
              private dialogService: MdlDialogService) {
  }

  ngOnInit(): void {
    console.log('getting buddies');

    this.buddyRepo.findAll().subscribe((data) => {
      console.log('Buddies', data);
      // and getPrivate notified about changes
      // repository could implement some kind of getNewPost
      // and then new posts can be highlighted on arrival
      this.filterBuddies(data);
    }, (errData) => {
      // error
    }, () => {
      // complete
    });
  }

  filterBuddies(buddies: Buddy[]): void {
    this.buddies = buddies.filter((buddy) => {
      return buddy.isAccepted;
    });

    this.invitations = buddies.filter((buddy) => {
      return !buddy.isAccepted;
    });

    this.accordionSections = [ ];
    this.accordionSections.push(new AccordionSection("Buddies", this.buddies));
    if (this.invitations.length > 0) {
      this.accordionSections.push(new AccordionSection("Offene Anfragen", this.invitations));
    }
  }

  showAddBuddyDialog(): void {
    let addDialog = this.dialogService.showCustomDialog({
      component: AddBuddyDialog,
      isModal: true
    });

    this.buddyRepo.findAll();
  }

}
