import {Component, OnInit, HostListener} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {MdlDialogReference} from 'angular2-mdl';
import {BuddyRepositoryService} from '../../../repositories/buddy-repository.service';
import {Buddy} from '../../../entities/buddy';
import {PublicFileSystem} from "../../../services/public-filesystem.service";
import {FileSystemService, EntityType} from "../../../services/filesystem.service";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {ChannelRepositoryService} from "../../../repositories/channel-repository.service";

/*
  Nice Example: http://mseemann.io/frontend/2016/10/10/angular-2-mdl-mdl-dialog-advanced-configuration.html
 */

@Component({
  selector: 'addBuddyDialog',
  templateUrl: 'add-buddy.dialog.html',
  styleUrls: [ 'add-buddy.dialog.scss' ]
})
export class AddBuddyDialog implements OnInit {
  public form: FormGroup;
  public userLink = new FormControl('', Validators.required);

  /**
   * Is bound to the box url input of the dialog.
   */
  buddyIDToAdd: string = null;
  showError: boolean = false;
  errorMessage: string;

  constructor(
    private dialog: MdlDialogReference,
    private fb: FormBuilder,
    private buddyRepo: BuddyRepositoryService,
    private publicFS: PublicFileSystem,
    private fileSystem: FileSystemService,
    private profileRepo: ProfileRepositoryService,
    private channelRepo: ChannelRepositoryService
  ) {
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      'UserID': this.userLink
    });
  }

  public addBuddy(): void {
    this.showError = false;

    if (!this.buddyIDToAdd) {
      this.errorMessage = "Bitte geben Sie eine valide BuddyID an";
      this.showError = true;
      return;
    }

    if (this.buddyIDToAdd === this.profileRepo.profile.id) {
      this.errorMessage = "Sie können nicht Ihr eigenes Profil hinzufügen";
      this.showError = true;
      return;
    }
    
    this.buddyIDToAdd = this.buddyIDToAdd.trim();

    this.dialog.hide();

    this.buddyRepo.create(this.buddyIDToAdd)
      .then((result) => {


        console.log("Res", result);

        this.channelRepo.addBuddyToMainChannel(result).then(res => {

          this.showError = false;
          console.log("buddy added sucessfully");

          this.fileSystem.save(EntityType.PROFILE, this.profileRepo.profile).catch(err =>{
            this.errorMessage = "Beim laden des Buddys ist ein Fehler aufgetreten. Bitte versuche es erneut";
            this.showError = true;
            console.error("[AddBuddyDialog]", err);          });
        }).catch(err => {
          this.errorMessage = "Beim laden des Buddys ist ein Fehler aufgetreten. Bitte versuche es erneut";
          this.showError = true;
          console.error("[AddBuddyDialog]", err);
        });
      })
      .catch((error) => {
        this.errorMessage = "Beim laden des Buddys ist ein Fehler aufgetreten. Bitte versuche es erneut";
        this.showError = true;
        console.error("[AddBuddyDialog]", error);
      });
  }

  /**
   * TODO: HostListener does not work yet
   */
  @HostListener('keydown.esc')
  public onEsc(): void {
    this.dialog.hide();
  }
}
