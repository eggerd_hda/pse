import {OnInit, Input} from "@angular/core";
import {Component} from "@angular/core/src/metadata/directives";
import {MdlDialogReference} from "angular2-mdl";
import {FormBuilder} from "@angular/forms";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {PostboxPipeService} from "../../../services/pipe/pipe.service";

let Cropper = require("cropperjs");

@Component({
  selector: 'profile-image-upload',
  templateUrl: 'profile-image-upload.component.html',
  styleUrls: [ 'profile-image-upload.component.scss' ]
})
export class ProfileImageUploadComponent implements OnInit {

  /**
   * The size of the image cropper.
   *
   * @type {number}
   */
  @Input() size: number = 200;


  /**
   * The source of the image to crop.
   *
   * @type {string}
   */
  @Input() imageSourceData: string = "";

  croppedImageSourceData: string = "";


  private cropper: any = null;


  constructor(private profileRepository: ProfileRepositoryService,
              private dialogReference: MdlDialogReference,
              private pipeService: PostboxPipeService) {
  }

  ngOnInit(): void {
    console.log("on init");
    let image = document.getElementById("pb-profile-image__cropper-image");
    image.onload = () => {
      console.log("image loaded");
      this.cropper = new Cropper(image, {
        aspectRatio: 1
      });
    };

    let sourceData = this.pipeService.get("pb-profile-image-data");
    if (!!sourceData) {
      this.imageSourceData = sourceData;
    }
  }

  setImageSource(event: any): void {
    let filesSelected = event.target.files;
    if (filesSelected.length > 0) {
      let fileToLoad = filesSelected[0];

      let fileReader: FileReader = new FileReader();

      fileReader.onload = (imgsrc: any) => {
        this.imageSourceData = imgsrc.target.result;
        this.initializeCropper();
      };
      fileReader.readAsDataURL(fileToLoad);
    }
  }

  initializeCropper(): void {
  }

  /**
   * Crops the image.
   */
  cropProfileImage(): void {
    this.croppedImageSourceData = this.cropper.getCroppedCanvas({
        width: this.size,
        height: this.size
      })
      .toBlob((blob) => {
        this.convertBlobToBase64(blob);
      });
  }

  private convertBlobToBase64(blob): void {
    let fileReader = new FileReader();
    fileReader.onloadend = () => {
      this.croppedImageSourceData = fileReader.result;

      this.uploadProfileImage();
    };
    fileReader.readAsDataURL(blob);
  }

  private uploadProfileImage(): void {
    this.profileRepository.loadProfileData()
      .then((profile) => {
        profile.profileImageData = this.croppedImageSourceData;
        profile.isDirty = true;

        this.profileRepository.save(profile);
        this.dialogReference.hide();
      });
  }

  private closeDialog(): void {
    this.dialogReference.hide();
  }
}
