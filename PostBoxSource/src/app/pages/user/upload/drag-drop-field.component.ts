import {Component, Input, ElementRef, ViewChild, EventEmitter, SimpleChange} from '@angular/core';
import {Output} from "@angular/core/src/metadata/directives";

@Component({
  selector: 'drag-drop-field',
  templateUrl: 'drag-drop-field.component.html',
  styleUrls: ['drag-drop-field.component.scss'],
})
export class DragDropFieldComponent {
  /**
   * The base-64 encoded file.
   */
  @Input() fileIn: string = null;

  @Output() filesOut: any = new EventEmitter();
  @Output() fieldClick: any = new EventEmitter();

  constructor() {

  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}): void {
    console.log("changes", changes);
    if (null !== this.fileIn) {
    }
  }

  private fieldClicked(): void {
    this.fieldClick.emit();
  }


  private onDrop(event): void {
    // fucking ie.
    event = event || window.event;
    event.preventDefault();

    // the data transfer field does not exist
    if (!event.dataTransfer) {
      return;
    }

    let files = event.dataTransfer.files;
    // check if we even have files.
    if (!!files && files.length > 0) {
      // only allow one file to be dropped.
      let file = files[0];

      // create a file reader to read the file into a base-64 encoded string.
      let fileReader = new FileReader();
      fileReader.onload = (imageData: any) => {
        // inform the file uploader.
        this.fileIn = imageData.target.result;
        this.filesOut.emit({ imageData: this.fileIn });
      };
      fileReader.readAsDataURL(file);
    }
  }

  /**
   * Will be called by the drag'n'drop field to prevent
   * the default event.
   * If we are not preventing the drag-over event, the
   * browser will display the file instead of redirecting
   * it to the app.
   *
   * @param event
   */
  private onDragOver(event): void {
    // fucking ie.
    event = event || window.event;

    event.preventDefault();
  }

}
