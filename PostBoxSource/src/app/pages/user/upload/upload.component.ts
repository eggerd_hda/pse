import {Component, Input, ElementRef, ViewChild, Renderer} from '@angular/core';
import {DragDropFieldComponent} from "./drag-drop-field.component";
import {PostType} from "../../../entities/enums/post-type.enum";
import {ChannelRepositoryService} from "../../../repositories/channel-repository.service";

@Component({
  selector: 'app-upload',
  templateUrl: 'upload.component.html',
  styleUrls: ['upload.component.scss'],
})
export class UploadComponent {

  @ViewChild('uploadFile') ulfile: ElementRef;
  @ViewChild('inputFileToLoad') fileInput: ElementRef;

  @ViewChild(DragDropFieldComponent) dragDropField: ElementRef;

  statusUpdate: string = '';
  srcData: string = '';
  isImage: boolean = false;
  hasText: boolean = true;


  /**
   * A flag for the loader.
   * If we set it to true, the loader will be shown.
   *
   * @type {boolean}
   */
  private isLoading: boolean = false;

  constructor(private channelRepository: ChannelRepositoryService,
              private renderer: Renderer) {
  }

  uploadPost(): void {
    if ((0 === this.statusUpdate.trim().length &&
      this.srcData.trim().length === 0) || this.isLoading) {
      // We cannot publish empty status updates.
      return;
    }
    this.isLoading = true;
    if (0 === this.statusUpdate.trim().length && this.srcData.trim().length !== 0) {
      this.channelRepository.addPostToChannel(PostType.Image, this.srcData.trim()).then((response) => {
        console.log("saved post");
        this.isLoading = false;
      }, (error) => {
        console.log('error', error);
        this.isLoading = false;
      });

    } else {
      this.channelRepository.addPostToChannel(PostType.Text, this.statusUpdate.trim()).then((response) => {
        console.log("saved post");
          this.isLoading = false;
        }) .catch((error) => {
        console.log(error);
        this.isLoading = false;
      });

    }
    this.ulfile.nativeElement.value = "";
    this.statusUpdate = "";
    this.srcData = '';
    this.statusUpdate = '';
  }

  changeListener($event): void {
    this.encodeImageFileAsURL($event.target);
  }

  encodeImageFileAsURL(inputFileToLoad: any): void {
    let filesSelected = inputFileToLoad.files;
    if (filesSelected.length > 0) {
      let fileToLoad = filesSelected[0];

      this.ulfile.nativeElement.value = fileToLoad.name;

      let fileReader: FileReader = new FileReader();

      fileReader.onload = (imgsrc: any) => {
        this.srcData = imgsrc.target.result;
        inputFileToLoad.value = "";
      };
      fileReader.readAsDataURL(fileToLoad);
    }
  }

  /**
   *
   * Will be called from the {@link DragDropFieldComponent} if the user
   * click on the drag'n'drop field to toggle the default
   * file chooser dialog.
   */
  private dragDropFieldClicked(): void {

    if (!!this.srcData && this.srcData.length > 0) {
      // We already selected an image. Remove it.

      this.srcData = "";
      return;
    }

    let event = new MouseEvent('click', {bubbles: true});
    this.renderer.invokeElementMethod(
      this.fileInput.nativeElement, 'dispatchEvent', [event]);
  }

  /**
   * Will be called from the {@link DragDropFieldComponent} if the user
   * drops a file on field.
   *
   * @param encodedFileData
   */
  private dragDropFileDropped(encodedFileData: any): void {
    this.srcData = encodedFileData.imageData;
  }
}
