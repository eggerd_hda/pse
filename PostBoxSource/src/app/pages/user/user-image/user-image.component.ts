import {Component, Input, SimpleChange, ElementRef, Renderer} from '@angular/core';
import {User} from "../../../entities/interfaces/user.interface";
import {ProfileImageUploadComponent} from "../image-upload-dialog/profile-image-upload.component";
import {MdlDialogService, MdlDialogReference} from "angular2-mdl";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {ViewChild} from "@angular/core/src/metadata/di";
import {Profile} from "../../../entities/profile/profile";
import {PostboxPipeService} from "../../../services/pipe/pipe.service";

@Component({
  selector: 'app-user-image',
  styleUrls: ['user-image.component.scss'],
  templateUrl: 'user-image.component.html',
})
export class UserImageComponent {

  /**
   * The user from which we want to display the profile image.
   */
  @Input() user: User;

  /**
   * With this input we can disable the file chooser for user images.
   *
   * @type {boolean}
   */
  @Input() disableFileChooser: boolean = false;

  @Input() isInList: boolean = false;

  imageDataUrl: string;
  showPlaceholder: boolean = true;

  isCurrentUser: boolean = false;


  @ViewChild('profileImageFileChooser') fileChooser: ElementRef;

  constructor(private dialogService: MdlDialogService,
              private profileRepository: ProfileRepositoryService,
              private renderer: Renderer,
              private pipeService: PostboxPipeService) {
  }

  ngOnInit(): void {
  }

  /**
   * Will be called when the buddy changes.
   */
  ngOnChanges(changes: {[propertyKey: string]: SimpleChange}): void {
    for (let change in changes) {
      if ('user' === change) {
        // We changed the buddy. Reload the image.
        if (this.isCurrentUser) {
          // the update will be coming from the profile repository.
          continue;
        }

        this.handleUserUpdate();
      }
    }
  }

  private updateUserImage(): void {
    this.showPlaceholder = false;

    if (this.user && this.user.profileImageData) {
      this.imageDataUrl = this.user.profileImageData;
      return;
    }

    this.showPlaceholder = true;
  }

  private handleUserUpdate(): void {
    console.log("user has updated, reload the image");
    this.isCurrentUser = (!!this.user && this.user instanceof Profile);
    console.log("is current user?", this.isCurrentUser);
    if (this.isCurrentUser) {
      this.profileRepository.find()
        .subscribe((profile) => {
          this.user = profile;
          this.updateUserImage();
        });
    } else {
      this.updateUserImage();
    }
  }

  /**
   * Delegates the click event to the native file chooser.
   */
  private showFileChooser(): void {
    console.log("click click");
    let event = new MouseEvent('click', {bubbles: true});
    this.renderer.invokeElementMethod(
      this.fileChooser.nativeElement, 'dispatchEvent', [event]);
  }

  private fileToUploadSelected(event): void {
    console.log("mann");
    let filesSelected = event.target.files;
    if (filesSelected.length > 0) {
      let fileToLoad = filesSelected[0];

      let fileReader: FileReader = new FileReader();

      fileReader.onload = (imgsrc: any) => {
        let srcData = imgsrc.target.result;
        console.log("image data loaded");
        // what i'm about to do now is really bad
        // but this shit is shit
        // TODO: find a better way to pass data into the dialog service
        this.pipeService.set("pb-profile-image-data", srcData);
        this.showProfileImageUploadDialog();

        event.target.value = "";
      };
      fileReader.readAsDataURL(fileToLoad);
    }
  }

  private showProfileImageUploadDialog(): void {
    this.dialogService.showCustomDialog({
        component: ProfileImageUploadComponent,
        isModal: true,
        styles: {'width': '90%', 'height': '90%'},
      });
  }
}
