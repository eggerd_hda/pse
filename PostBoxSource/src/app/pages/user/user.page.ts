import {Component} from '@angular/core';
import {Profile} from '../../entities/profile/profile';
import {ProfileRepositoryService} from '../../repositories/profile-repository.service';
import {Post} from '../../entities/post';
import {FileSystemService} from "../../services/filesystem.service";
import {User} from "../../entities/interfaces/user.interface";
import {ActivatedRoute} from "@angular/router";
import {Buddy} from "../../entities/buddy";
import {BuddyRepositoryService} from "../../repositories/buddy-repository.service";
import {ChannelRepositoryService} from "../../repositories/channel-repository.service";
import {Channel} from "../../entities/channel";

@Component({
    selector: 'UserPage',
    templateUrl: './user.page.html'
})
export class UserPage {

    /**
     * The current user.
     */
    user: User;

    userLoaded: boolean = false;
    isOwnPage: boolean = false;
    private subscription;

    /**
     * The posts of the current user.
     * Used to show in the wall on the own profile.
     */
    posts: Post[];

    showQRCode: boolean = false;

    constructor(private profileRepository: ProfileRepositoryService,
                private buddyRepository: BuddyRepositoryService,
                private channelRepository: ChannelRepositoryService,
                private fileSystem: FileSystemService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.activatedRoute.params.subscribe(
            (param: any) => {
                if (this.profileRepository.profile.id === param.id) {
                    this.isOwnPage = true;
                    this.loadProfile();
                } else {
                    this.loadBuddy(param.id);
                }
            }
        );
    }

    /**
     * Loads the buddy through the {@link BuddyRepositoryService}.
     *
     * @param id      The id of the buddy.
     */
    loadBuddy(id: string): void {
        this.buddyRepository.findById(id).subscribe(
            (buddy: Buddy) => {
                console.log("loaded buddy", buddy);
                this.user = buddy;
                this.userLoaded = true;

                this.loadPosts();
            }
        );
    }

    closeQRCode(): void {
        this.showQRCode = false;
    }

    openQRCode(): void {
        console.log("open qr code");
        this.showQRCode = true;
    }

    /**
     * Loads all the posts associated with the buddy.
     * Needs to be called after the buddy has been loaded.
     */
    loadPosts(): void {
        let subscribedUserID = this.user.id;
        this.channelRepository.pollUserMainChannel(this.user.id);

        console.log("Loading posts for Wall of", this.user.name);

        this.subscription = this.channelRepository.findMainChannelFromUser(this.user.id)
            .subscribe((channel: Channel) => {
              if (subscribedUserID === this.user.id) {
                if (!channel) {
                  return;
                }

                if (this.posts && channel.posts.length === this.posts.length) {
                  return;
                }

                console.log("Got New Posts from Channel for this Wall!", channel);

                this.posts = [];

                for (let post of channel.posts) {
                  if (typeof post === "string") {
                    post = JSON.parse(<string>post);
                  }
                  this.posts.unshift(post);
                }
              } else {
                console.log('SUBSCRIPTION FOR WRONG USER!');
                if (this.subscription) {
                  this.subscription.unsubscribe();
                }
              }
                    // TODO: Hide loading screen/icon
                }, (err) => {
                    console.warn('Could not load posts from buddy', err);
                }
            );
    }

    private loadProfile(): void {
      this.subscription = this.profileRepository.loadProfileData()
            .then((profile) => {
                this.profileRepository.find()
                    .subscribe((profile) => {
                        console.log("profile loaded/update", profile);
                        this.user = profile;
                        this.userLoaded = true;
                        this.loadPosts();
                    });
            })
            .catch((error) => {
                // TODO: inform the user about the error.
                console.log(error);
            });


    }

}
