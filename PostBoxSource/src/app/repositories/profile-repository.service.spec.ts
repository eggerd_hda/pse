/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {BuddyRepositoryService} from './buddy-repository.service';
import {ProfileRepositoryService} from './profile-repository.service';
import {DropboxAdapterService} from '../services/dropbox-adapter.service';
import {CryptoService} from "../services/crypto.service";
import {HttpModule} from "@angular/http";
import {FileSystemService} from "../services/filesystem.service";
import {PrivateNodeService} from "../services/privateNode.service";
import {PublicNodeService} from "../services/publicNode.service";

describe('Service: ProfileRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileRepositoryService, PublicNodeService, PrivateNodeService, FileSystemService, DropboxAdapterService, CryptoService],

      imports: [
        HttpModule
      ]
    });
  });

  it('should ...', inject([ProfileRepositoryService], (service: ProfileRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
