/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {GroupRepositoryService} from './group-repository.service';
import {DropboxAdapterService} from '../services/dropbox-adapter.service';
import {CryptoService} from "../services/crypto.service";

describe('Service: GroupRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupRepositoryService, DropboxAdapterService, CryptoService]
    });
  });

  it('should ...', inject([GroupRepositoryService], (service: GroupRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
