import {TestBed, inject} from '@angular/core/testing';
import {BuddyRepositoryService} from './buddy-repository.service';
import {DropboxAdapterService} from '../services/dropbox-adapter.service';
import {ProfileRepositoryService} from "./profile-repository.service";
import {CryptoService} from "../services/crypto.service";
import {NotificationService} from "../services/notification.service";
import {PublicFileSystem} from "../services/public-filesystem.service";
import {HttpModule} from "@angular/http";
import {FileSystemService} from "../services/filesystem.service";
import {PrivateNodeService} from "../services/privateNode.service";
import {PublicNodeService} from "../services/publicNode.service";

describe('Service: BuddyRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuddyRepositoryService, PrivateNodeService, PublicNodeService, DropboxAdapterService, ProfileRepositoryService, CryptoService, NotificationService, FileSystemService, PublicFileSystem],

      imports: [
        HttpModule
      ]
    });
  });

  it('should ...', inject([BuddyRepositoryService], (service: BuddyRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
