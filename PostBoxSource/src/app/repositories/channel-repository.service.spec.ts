import {TestBed, inject} from '@angular/core/testing';
import {DropboxAdapterService} from '../services/dropbox-adapter.service';
import {ProfileRepositoryService} from "./profile-repository.service";
import {CryptoService} from "../services/crypto.service";
import {NotificationService} from "../services/notification.service";
import {PublicFileSystem} from "../services/public-filesystem.service";
import {HttpModule} from "@angular/http";
import {FileSystemService} from "../services/filesystem.service";
import {PrivateNodeService} from "../services/privateNode.service";
import {PublicNodeService} from "../services/publicNode.service";
import {ChannelRepositoryService} from "./channel-repository.service";
import {Post} from "../entities/post";
import {PostType} from "../entities/enums/post-type.enum";
import {BuddyRepositoryService} from "./buddy-repository.service";

describe('Service: ChannelRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChannelRepositoryService, BuddyRepositoryService, PrivateNodeService, PublicNodeService, DropboxAdapterService, ProfileRepositoryService, CryptoService, NotificationService, FileSystemService, PublicFileSystem],

      imports: [
        HttpModule
      ]
    });



  });

  it('should ...', inject([ChannelRepositoryService], (service: ChannelRepositoryService) => {
    expect(service).toBeTruthy();
  }));

  it('should merge right', inject([ChannelRepositoryService], (service: ChannelRepositoryService) => {

    let post1 = new Post(PostType.Text,"A","A",1);
    let post12 = new Post(PostType.Text,"A","C",3);
    let post13 = new Post(PostType.Text,"A","E",4);

    let post2 = new Post(PostType.Text,"B","B",2);
    let post22 = new Post(PostType.Text,"B","D",3);
    let post23 = new Post(PostType.Text,"B","F",5);

    let arr1 = [post1, post12, post13];
    let arr2 = [post2, post22, post23];

    let merged = service.mergePosts(arr1,arr2);

    expect(merged[2].message).toBe("C");
  }));

  it('should merge equals', inject([ChannelRepositoryService], (service: ChannelRepositoryService) => {

    let post1 = new Post(PostType.Text,"A","A",1);
    let post12 = new Post(PostType.Text,"A","C",3);
    let post13 = new Post(PostType.Text,"A","E",4);

    let post2 = new Post(PostType.Text,"B","B",2);
    let post22 = new Post(PostType.Text,"B","D",3);
    let post23 = new Post(PostType.Text,"B","F",5);

    let arr1 = [post1, post12, post13];
    let arr2 = [post1, post22, post23];
    let merged = service.mergePosts(arr1, arr2);
    expect(merged.length).toBe(5);

  }));

  it('should merge right amount', inject([ChannelRepositoryService], (service: ChannelRepositoryService) => {
    let post1 = new Post(PostType.Text,"A","A",1);

    let arr1 = [];
    let arr2 = [post1];
    expect(service.mergePosts(arr1,arr2).length).toBe(1)

  }));
});
