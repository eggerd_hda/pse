import {Injectable} from '@angular/core';
import {IRepositoryService} from './interfaces/repository.interface';
import {Post} from '../entities/post';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Buddy} from '../entities/buddy';
import {PostType} from '../entities/enums/post-type.enum';
import {Entity} from '../entities/interfaces/entity.interface';
import {Channel, ChannelType} from '../entities/channel';
import {BuddyRepositoryService} from './buddy-repository.service';
import {PublicFileSystem} from '../services/public-filesystem.service';
import {NotificationService, ScanResult} from '../services/notification.service';
import {CryptoBuddy, CryptoService} from "../services/crypto.service";
import {FileSystemService, EntityType} from "../services/filesystem.service";
import {ProfileRepositoryService} from "./profile-repository.service";
import {ChannelMeta} from "../services/publicNode.service";
import {User} from "../entities/interfaces/user.interface";
import {forEach} from "@angular/router/src/utils/collection";

@Injectable()
export class ChannelRepositoryService implements IRepositoryService {

    private channels: any = {};
    private ownChannels: Channel[];
    private allPostsArray: Post[] = [];
    private mainChannelID: string;

    // this one can be subscribed by and gets notifications on changes
    private allChannels: BehaviorSubject<Channel[]>;
    private allPostsSubject: BehaviorSubject<Post[]>;

    //////////

    private mainChannels: any = {};
    private channelSubjects: any = {};

    private chatChannels: BehaviorSubject<BehaviorSubject<Channel>[]>;

    public newMessages: boolean = false;


    constructor(private publicFS: PublicFileSystem,
                private profileRepository: ProfileRepositoryService,
                private fileSystem: FileSystemService,
                private buddyRepo: BuddyRepositoryService,
                private notificationService: NotificationService,
                private cryptoService: CryptoService,
                private buddyRepository: BuddyRepositoryService,) {
        this.channels = [];
        this.ownChannels = [];

        this.chatChannels = new BehaviorSubject<BehaviorSubject<Channel>[]>(null);

        this.allChannels = new BehaviorSubject<Channel[]>([]);
        this.allChannels.next(this.channels); // do this on all post updates

        this.allPostsSubject = new BehaviorSubject<Post[]>([]);

        // Initialise Observer for Wall-Posts
        // every time a new post is seen, it gets added to the array:  this.allPostsArray
        this.buddyRepo.findAll().subscribe(
            buddyList => {
                for (let buddy of buddyList) {
                    this.findMainChannelFromUser(buddy.id).subscribe(channel => {
                        if (!channel) return;
                        this.addPostsToPostArray(channel.posts);
                        this.allPostsSubject.next(this.allPostsArray);
                    });
                }
            }, error => {
                console.error(error);
            });
        // also our own posts are listed:
        this.profileRepository.find().subscribe(
            profile => {
                if (!profile) return;
                this.findMainChannelFromUser(profile.id).subscribe(channel => {
                    if (!channel) return;
                    this.addPostsToPostArray(channel.posts);
                    this.allPostsSubject.next(this.allPostsArray);
                });
            }, error => {
                console.error(error)
            }
        );
    }

    /***
     * Adds an array of posts into the the global collection of all posts, that get shown on the wall
     * Duplicates are filtered.
     * Order by data
     * @param posts
     */
    private addPostsToPostArray(posts: Post[]) {
        if (posts.length === 0) return;
        
        posts.forEach(post => {
            if (!this.allPostsArray.find(existingPost => existingPost.id === post.id)) {
                this.allPostsArray.push(post);
            }
        });

        this.allPostsArray = this.allPostsArray.sort(function (a: Post, b: Post) {
            if (new Date(a.updatedAt) < new Date(b.updatedAt)) return 1;
            if (new Date(a.updatedAt) > new Date(b.updatedAt)) return -1;
            return 0;
        });
    }

    public initialise(): void {
        this.notificationService.initialise(this.buddyRepo, this.publicFS);

        this.notificationService.getNotificationObservable().subscribe((scanResult: ScanResult) => {
            if (scanResult && scanResult.channelMetaHasChanged) {
                this.notifyForChannel(scanResult);
            }
        });

        // Load Cached Chat-Channels
        this.fileSystem.load(EntityType.CHANNELMETAS).then(metas => {
            console.log("Cached metas", metas);

            let promises = [];

            for (let meta in metas) {
                if (!metas.hasOwnProperty(meta)) {
                    continue;
                }

                // Skip Own Wall-Channel
                if (metas[meta].type === ChannelType.WALL_CHANNEL) {
                    continue;
                }

                let prom = this.fileSystem.load(EntityType.CHANNEL, metas[meta].id).then(res => {
                    this.channels[res.id] = res;
                    this.channelSubjects[res.id] = new BehaviorSubject<Channel>(res);
                    //this.chatChannels.next(this.channelSubjects);

                });
                promises.push(prom);
            }
            // When all Chat Channels are loaded
            Promise.all(promises).then(() => {
                console.log("Got all chatChannels", this.channelSubjects);
                this.chatChannels.next(this.channelSubjects);
            });
        });

    }


    public getAllPostsOfAllBuddies(): Observable<Post[]> {
        return this.allPostsSubject.asObservable();
    }

    findAll(): Observable<Channel[]> {
        return this.allChannels.asObservable();
    };

    /**
     * Finds a Channel by ID
     * @param id
     * @returns {Promise<Channel>}
     */
    findById(id: string): Observable<Channel> {
        if (!this.channelSubjects[id]) {
            this.channelSubjects[id] = new BehaviorSubject<Channel>(null);
        }
        return this.channelSubjects[id].asObservable();
    };

    remove(): void {

    }

    /**
     * Finds all Channels of a user; If wanted for a specific type
     * @param userID
     * @param channelType
     * @returns {Promise<Channel[]>}
     */
    findAllFromUser(userID: string, channelType?: ChannelType): Channel[] {
        let channels = [];

        for (let channel of this.channels) {

            if (channelType && channel.channelType !== channelType) {
                continue;
            }
            if (channel.ownerID === userID) {
                channels.push(channel);
            }
        }

        return channels;
    }

    /**
     * Returns all Chat channels as observables
     * But what, if new channels are added?
     * => [Observable]
     *
     */
    findChatChannels(): Observable<BehaviorSubject<Channel>[]> {
        return this.chatChannels.asObservable();
    }

    /**
     * Triggers polling of users main-channel
     * @param userID
     */
    pollUserMainChannel(userID: string): void {
        if (!this.mainChannels[userID]) {
            this.mainChannels[userID] = new BehaviorSubject<Channel>(null);
        }

        // Own Profile
        if (this.profileRepository.profile.id === userID) {
            this.fileSystem.load(EntityType.CHANNEL, this.mainChannelID).then(channel => {
                this.mainChannels[userID].next(channel);
            });
        } else {

            // Rather get cached version of channelMeta + mainChannel

            this.publicFS.readPublicFile(userID).subscribe((publicNode) => {
                let mainChannelMeta = <ChannelMeta> this.publicFS.parseChannelMetas(publicNode)[0];

                console.log("Loaded MainChannel of buddy", mainChannelMeta);

                let channel = new Channel();
                channel.fillFromMeta(mainChannelMeta);

                this.loadChannelPosts(channel.url).then(posts => {
                    channel.posts = posts;
                    this.mainChannels[userID].next(channel);
                }).catch(err => {
                    console.log(err);
                });
            }, (err) => {
                console.log("Could not load publicFile " + err);
            });
        }
    }

    /**
     * Gets the main chanel-observable for a user
     * @param userID
     * @returns {Observable<T>}
     */
    findMainChannelFromUser(userID: string): Observable<Channel> {
        if (!this.mainChannels[userID]) {
            this.mainChannels[userID] = new BehaviorSubject<Channel>(null);
        }

        return this.mainChannels[userID].asObservable();
    }

    public addBuddyToMainChannel(buddy: Buddy): Promise<any> {
        let channel = <Channel> this.channels[this.mainChannelID];


        channel.participants.push({identifier: buddy.id, publicKey: buddy.publicKey, name: buddy.name});
        //channel.participants.push(new CryptoBuddy(buddy.id, buddy.publicKey));
        console.log("Saving channel", channel);

        return this.fileSystem.save(EntityType.CHANNEL, channel);
    }

    /**
     * Adds a post to a channel
     * @param postType
     * @param data
     * @param channelID
     * @returns {Promise<T>}
     */
    addPostToChannel(postType: PostType, data: string, channelID?: string): Promise<any> {
        return new Promise((resolve, reject) => {

            let channel: Channel = null;

            if (channelID) {
                channel = <Channel> this.channels[channelID];

                // Use MainChannel
            } else {
                console.log("Using mainChannel");
                channel = <Channel> this.channels[this.mainChannelID];
            }

            if (!channel) {
                console.log("Channel does not exist local!");
                return;
            }

            let post = new Post(postType, this.profileRepository.profile.id, data, channel.head);

            channel.appendPayload(post);
            channel.hashOfLastPost = post.id;
            this.channels[channel.id] = channel;

            // Save in Cache
            this.notificationService.saveMetaToCache(channel.getMeta());

            // Inform Observers
            if (channel.channelType === ChannelType.CHAT_CHANNEL) {
                if (!this.channelSubjects[channel.id]) {
                    this.channelSubjects[channel.id] = new BehaviorSubject<Channel>(this.channels[channel.id]);
                    this.chatChannels.next(this.channelSubjects);
                } else {
                    this.channelSubjects[channel.id].next(this.channels[channel.id]);
                }
            } else {
                this.mainChannels[this.profileRepository.profile.id].next(this.channels[channel.id]);
            }

            console.log("Saving channel", channel);

            this.fileSystem.save(EntityType.CHANNEL, channel).then(res => {
                resolve();
            }).catch(err => {
                console.log("Rirst try failed", err);
                // TODO: Try again, because smtimes crypto error
                this.addPostToChannel(postType, data, channelID).then(res => {
                    resolve(res);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }

    // TODO: Is never used
    save(channelToSave: Channel): Promise<Channel> {
        return new Promise((resolve, reject) => {
            // check if existent already
            let channel: Channel = this.channels.find(p => p.id === channelToSave.id);
            console.log('save channel', channelToSave);

            if (!channel) {
                channel = channelToSave;
                // TODO: Mabye not push, but channel.id pos
                this.channels.push(channel);

                // TODO: Check
                if (channel) {
                    this.ownChannels.push(channel);
                }
            } else {
                Object.assign(channel.posts, channelToSave.posts);
            }

            this.allChannels.next(this.channels);

            this.fileSystem.save(EntityType.CHANNEL, channel).then(res => {
                console.log('Updated channel');
                resolve();
            }).catch(err => {
                console.error('Could not update channel');
                reject();
            });
        });
    };

    /**
     * Loads the main channel.
     * If not existing, creates it.
     * @returns {Promise<T>}
     */
    public loadMainChannelFromStorage(): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log("Loading main channel..");
            this.fileSystem.load(EntityType.CHANNELMETAS).then(metas => {
                // Create new main, if not existing
                if (metas.length < 1) {
                    this.createNewMainChannel().then(() => {
                        console.log("Created new Main channel");
                        resolve();
                    }).catch(err => {
                        reject("Could not create main channel " + err);
                    });
                } else {
                    // Otherwise load channel
                    console.log("Metas", metas);
                    // Load url..
                    this.fileSystem.load(EntityType.CHANNEL, metas[0].id).then(res => {
                        this.mainChannelID = metas[0].id;
                        this.channels[this.mainChannelID] = res;
                        console.log("Loaded main channel", res);
                        resolve();
                    }).catch(err => {
                        reject("Could not load main channel " + this.mainChannelID + err);
                    });
                }
            }).catch(err => {
                reject(err);
            });

        });
    }


    // TODO: Inform chatChannel-Observer
    /**
     * Creates a new Chat-Channel
     * @param participants
     * @param name
     * @param moderated
     * @returns {Promise<any>}
     */
    public createNewChatChannel(participants: Buddy[], name: string, moderated: boolean = false): Promise<any> {
        return new Promise((resolve, reject) => {

            let channel = new Channel([], ChannelType.CHAT_CHANNEL);

            channel.name = name;

            channel.participants = participants.map(buddy => {
                return {identifier: buddy.id, publicKey: buddy.publicKey, name: buddy.name};
            });

            // Push own User
            channel.participants.push({
                identifier: this.profileRepository.profile.id,
                publicKey: this.profileRepository.profile.publicKey,
                name: this.profileRepository.profile.name
            });

            // Initial message
            let initialPost = new Post(PostType.Text, this.profileRepository.profile.id,
                `Chat von ${this.profileRepository.profile.name} erstellt`, channel.head);
            channel.appendPayload(initialPost);
            channel.hashOfLastPost = initialPost.id;

            if (moderated) {
                channel.ownerID = this.profileRepository.profile.id;
            }

            // Create shared link
            this.fileSystem.getLinkToID(channel.id).then(link => {
                channel.url = link;

                // Save in cache
                this.notificationService.saveMetaToCache(channel.getMeta());

                this.channels[channel.id] = channel;

                this.fileSystem.save(EntityType.CHANNEL, channel).then(() => {
                    console.log("Saved channel");

                    this.channelSubjects[channel.id] = new BehaviorSubject<Channel>(this.channels[channel.id]);
                    this.chatChannels.next(this.channelSubjects);

                    resolve();
                }).catch(err => {
                    reject(err);
                });
            }).catch(err => {
                reject(err);
            });
        });
    }

    // TODO: Implement
    public removeBuddyFromChannel(channelID: string, buddy: Buddy): Promise<any> {
        return new Promise((resolve, reject) => {

            this.fileSystem.load(EntityType.CHANNELMETA, channelID).then(meta => {

                // TODO: Implement removal

                this.fileSystem.save(EntityType.CHANNELMETA, meta)
                    .then(() => {
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }).catch(err => {
                reject(err);
            });
        });
    }

    /**
     * Adds Buddy to a ChannelMeta
     * @param channelID
     * @param buddy
     * @returns {Promise<T>}
     */
    public addBuddyToChannel(channelID: string, buddy: Buddy): Promise<any> {
        return new Promise((resolve, reject) => {

            this.fileSystem.load(EntityType.CHANNEL, channelID).then(channel => {

                if (channel.ownerID && channel.ownerID !== this.profileRepository.profile.id) {
                    reject("You are not the owner of this Chat");
                }

                channel.participants.push({identifier: buddy.id, publicKey: buddy.publicKey, name: buddy.name});

                this.fileSystem.save(EntityType.CHANNEL, channel)
                    .then(() => {
                        resolve();
                        console.log("buddy saved to channel");
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }).catch(err => {
                reject(err);
            });

            /*  this.fileSystem.load(EntityType.CHANNELMETA, channelID).then(meta => {

             console.log("Adding buddy to", meta);
             if (meta.ownerID && meta.ownerID !== this.profileRepository.profile.id) {
             reject("You are not the owner of this Chat");
             }

             meta.participants.push({identifier: buddy.id, publicKey: buddy.publicKey, name: buddy.name});

             this.fileSystem.save(EntityType.CHANNELMETA, meta)
             .then(() => {resolve(); console.log("buddy saved to channel");})
             .catch((err) => {reject(err); });
             }).catch(err => {reject(err); });*/
        });
    }


    private createNewMainChannel(): Promise<any> {
        return new Promise((resolve, reject) => {
            let channel = new Channel([], ChannelType.WALL_CHANNEL);
            channel.ownerID = this.profileRepository.profile.id;
            channel.participants = this.buddyRepo.buddies.map(buddy => {
                return {identifier: buddy.id, publicKey: buddy.publicKey, name: buddy.name};
            });

            // Create shared link
            this.fileSystem.getLinkToID(channel.id).then(link => {
                channel.url = link;
                this.mainChannelID = channel.id;
                this.channels[channel.id] = channel;


                this.fileSystem.save(EntityType.CHANNEL, channel).then(() => {
                    resolve();
                }).catch(err => {
                    reject(err);
                });
            }).catch(err => {
                reject(err);
            });
        });
    }

    /**
     * Loads posts of a remote url and parses them
     * @param url
     * @returns {Promise<T>}
     */
    private loadChannelPosts(url: string): Promise<Post[]> {
        return new Promise((resolve, reject) => {
            this.publicFS.getChannelPosts(url).then(stringifiedPosts => {
                let postArray = JSON.parse(stringifiedPosts);
                postArray = postArray.map(stringPost => {
                    let post = stringPost;
                    if (typeof stringPost === 'string') {
                        post = <Post> JSON.parse(stringPost);
                    }
                    return post;
                });
                resolve(postArray);
            }).catch(err => {
                reject(err);
            });
        });
    }

    /**
     * Merges posts by date and participantsID
     * @param arr1
     * @param arr2
     */
    public mergePosts(arr1: Post[], arr2: Post[]): Post[] {

        let posts = arr1;

        // If not in first array, push new posts
        arr2.forEach(item => {
            let index = posts.find(post => {
                return post.id === item.id;
            });

            if (!index) {
                posts.push(item);
            }
        });

        posts = posts.sort((left, right) => {
            if (left.head < right.head) return -1;
            if (left.head > right.head) return 1;

            if (left.creatorID < right.creatorID) return -1;
            else return 1;
        });

        return posts;
    }

    /**
     * Merges a local Channel with a remote channel
     * @param local
     * @param remote
     */
    private mergeChannels(local: Channel, remote: ChannelMeta): Promise<any> {
        return new Promise((resolve, reject) => {
            /**
             * Load posts from remote
             */
            this.loadChannelPosts(remote.url).then(posts => {

                let postNum = local.posts.length;

                // Merge posts
                let mergedPosts = this.mergePosts(local.posts, posts);

                local.newMessages = mergedPosts.length - postNum;

                local.posts = mergedPosts;

                if (local.hashOfLastPost !== remote.hashOfLastPost) {
                    this.newMessages = true;
                }

                local.head = local.posts.length;
                local.hashOfLastPost = local.posts[local.head - 1].id;

                // Merge participants
                remote.participants.forEach(user => {
                    let found: CryptoBuddy = local.participants.find(item => {
                        return item.identifier === user.identifier;
                    });

                    if (!found) {
                        local.participants.push(user);
                    }
                });

                this.channels[remote.id] = local;

                if (!this.channelSubjects[remote.id]) {
                    this.channelSubjects[remote.id] = new BehaviorSubject<Channel>(this.channels[remote.id]);
                    this.chatChannels.next(this.channelSubjects);
                } else {
                    this.channelSubjects[remote.id].next(this.channels[remote.id]);
                }

                this.fileSystem.save(EntityType.CHANNEL, this.channels[remote.id]).then(res => {
                    console.log("Saved channel locally", local);
                    resolve();
                }).catch(err => {
                    console.log("ERR, could not save channel locally");
                    reject(err);
                });
            }).catch(err => {
                console.log("ERR, could not load posts from remote buddy");
                reject(err);
            });
        });
    }

    /**
     * Loads the posts for a channel and notifies the observers
     * @param channelMeta
     * @param buddy
     */
    private loadPostsOfChannel(channelMeta: ChannelMeta, buddy: Buddy): void {

        if (channelMeta.type === ChannelType.CHAT_CHANNEL) {

            let channel = this.channels[channelMeta.id];

            // If we poll for the first time
            if (!channel) {
                // Load Channel-Copy from Storage
                this.fileSystem.load(EntityType.CHANNEL, channelMeta.id).then(localChannel => {
                    console.log("Got Channel already in FS!");
                    this.mergeChannels(localChannel, channelMeta).then(() => {

                    }).catch(err => {
                        console.log(err);
                    });
                    // If not existing, create new copy
                }).catch(err => {
                    console.log("Creating local copy of channel", channelMeta);

                    channel = new Channel();
                    channel.fillFromMeta(channelMeta);

                    this.fileSystem.getLinkToID(channel.id).then(link => {
                        channel.url = link;
                        console.log("Got link", link);
                        this.mergeChannels(channel, channelMeta).then(() => {
                            this.newMessages = true;

                        }).catch(err => {
                            console.log(err);
                        });
                    }).catch(err => console.log("ERROR creating local channel", err));
                });
            } else {
                this.mergeChannels(channel, channelMeta).then(() => {
                }).catch(err => {
                    console.log(err);
                });
            }
            // Uni-directional wall-channel
        } else {
            let channel = this.channels[channelMeta.id];

            if (!channel) {
                console.log("Channel is not cached yet!");

                channel = new Channel();
                channel.fillFromMeta(channelMeta);

                this.channels[channel.id] = channel;
            }

            // TODO: Check if this is really the requested channel? (For performance)
            // So that you dont need to load the posts unecessary

            this.loadChannelPosts(channel.url).then(posts => {

                console.log("Loaded Channel Posts", posts);

                this.channels[channel.id].posts = posts;

                console.log("For buddy", buddy.name);
                if (!this.mainChannels[buddy.id]) {
                    this.mainChannels[buddy.id] = new BehaviorSubject<Channel>(this.channels[channel.id]);
                } else {
                    this.mainChannels[buddy.id].next(this.channels[channel.id]);
                }

            }).catch(err => {
                console.error(err);
            });
        }
    }

    /**
     * This function gets called every time the NotificationService has found new Content
     * @param scanResult
     */
    private notifyForChannel(scanResult: ScanResult): void {
        for (let i = 0; i < scanResult.changedChannelMeta.length; i++) {
            this.loadPostsOfChannel(scanResult.changedChannelMeta[i], scanResult.buddy);
        }
    }

}
