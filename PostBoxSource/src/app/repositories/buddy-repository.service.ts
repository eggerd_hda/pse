import {Injectable} from '@angular/core';
import {Buddy} from '../entities/buddy';
import {IRepositoryService} from './interfaces/repository.interface';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observer} from 'rxjs/Observer';
import {PublicFileSystem} from "../services/public-filesystem.service";
import {NotificationService, ScanResult} from "../services/notification.service";
import {FileSystemService, EntityType} from "../services/filesystem.service";
import {scan} from "rxjs/operator/scan";

@Injectable()
export class BuddyRepositoryService implements IRepositoryService {

  buddies: Buddy[];

  // this one can be subscribed by and gets notifications on changes
  private allBuddies: BehaviorSubject<Buddy[]>;

  constructor(private fileSystem: FileSystemService,
              private publicFs: PublicFileSystem,
              private notificationService: NotificationService) {
    this.buddies = [];
    this.allBuddies = new BehaviorSubject<Buddy[]>([]);
  }

  /**
   * This function causes the BuddyRepo to subscribe notifications of the NotificationService.
   */
  public initialiseNotificationServiceObserver (): void {
    this.notificationService.getNotificationObservable().subscribe((scanResult: ScanResult) => {
      if (scanResult != null) {
        this.handleChangeNotification(scanResult);
      }
    });
  }


  /**
   * Returns list of buddies.
   * Other than the findAll function, this does not return an observer.
   * @returns {Buddy[]}
   */
  public getAllCurrentBuddies(): Buddy[] {
    return this.buddies;
  }


  findAll(): Observable<Buddy[]> {
    return this.allBuddies.asObservable();
  };

  findById(id: string): Observable<Buddy> {
    return Observable.create((observer: Observer<Buddy>) => {

      // getPrivate desired post
      // maybe we need to lazy load it
      let entity: Buddy = this.buddies.find(item => {
        return item.id === id;
      });

      if (!entity) {
        observer.error(null);
      } else {
        observer.next(entity);
      }

      // finish observable
      observer.complete();
    });
  };

  /**
   * TODO: Test with publicKey, how did it work before?
   * Adds another user as a buddy.
   * Loads additional information from the public node of the buddy to be.
   *
   * @param boxUrl
   *
   * @returns {Promise<Buddy>}
   */
  create(buddyID: string): Promise<Buddy> {
    return new Promise((resolve, reject) => {
      // read the public file of the buddy so that we can getPrivate inforamtion
      // like the key or the user name.
      // let boxUrl = atob(buddyID);
      let boxUrl = buddyID;

      let buddy = new Buddy();
      buddy.isChatOnlyBuddy = false;
      buddy.isDirty = true;
      buddy.id = boxUrl;
      buddy.isAccepted = false;

      this.publicFs.getPublicProfileInformations(buddy)
        .then((profileInformations) => {

          console.log("ProfileInformations", profileInformations);

          // fill the new buddy with information
          buddy.name = profileInformations.userName;
          buddy.publicKey = profileInformations.publicKey;
          buddy.signaturePublicKey = profileInformations.signaturePublicKey;

          console.log("Saving buddy", buddy);

          // save the buddy to the file system
          this.saveOrCreate(buddy)
            .then((result) => {
              // YEAH! it worked.
              resolve(result);
            })
            .catch((error) => {
              // The buddy could not be saved.
              reject(error);
            });

        }).catch((error) => {
          // The public node for this buddy has not been found.
          reject(error);
        });
    });
  };

  /**
   * Saves the buddy if changes has been made.
   *
   * @param buddyToSave
   * @returns {Promise<Buddy>}
   */
  save(buddyToSave: Buddy): Promise<Buddy> {
    return this.saveOrCreate(buddyToSave);
  };

  /**
   * TODO: implement in future release.
   *
   * @param buddy
   * @returns {any}
   */
  remove(buddy: Buddy): Observable<Buddy> {
    return Observable.create((observer: Observer<Buddy>) => {

      // finish observable
      observer.complete();
    });
  };

  /**
   * Gets called, when the NotificationService has found changes inside
   * a buddies public-node
   * TODO: Error thrown
   * @param buddy
   */
  private handleChangeNotification(scanResult: ScanResult): void {

    if ( ! (scanResult.buddyHasAcceptedMyInvitation || scanResult.profileInfoHasChanged)) {
      return;
    }

    let buddy: Buddy = scanResult.buddy;

    if (scanResult.buddyHasAcceptedMyInvitation && ! buddy.isAccepted) {
      buddy.isAccepted = true;
      console.log("[BuddyRepository] Buddy accepted invitation: ", buddy);
    } else {
    }

    if (scanResult.privateProfileInformation) {
      buddy.profileImageData = scanResult.privateProfileInformation.profileImageData;
    }
    buddy.isDirty = true;
    this.saveOrCreate(buddy);
  }

  /**
   * Takes a buddy object and saves it.
   * If the buddy does not exist yet, it will be created.
   *
   * @param buddy
   * @returns {Promise<Buddy>}
   */
  private saveOrCreate(buddy: Buddy): Promise<Buddy> {
    return new Promise((resolve, reject) => {

      let entity: Buddy = this.buddies.find(item => {
        return item.id === buddy.id;
      });

      if (!entity) {
        entity = buddy;
        this.buddies.push(entity);
      } else {
        Object.assign(entity, buddy);
        // write back to filesystem
      }

      if (buddy.isDirty) {
        this.fileSystem.save(EntityType.BUDDY, buddy).then(() => {
          this.allBuddies.next(this.buddies);
          resolve(entity);
        }).catch(err => { reject(err); });
      }else {
        this.allBuddies.next(this.buddies);
        resolve(entity);
      }
    });
  }
}
