import {Entity} from "../../entities/interfaces/entity.interface";
import {Observable} from "rxjs";
export interface IRepositoryService {
  findAll(): Observable<Entity[]>;
  findById(id: string): Observable<Entity>;
  save(entity: Entity): Promise<Entity>;
  remove(entity: Entity): void;
}
