import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {Profile} from '../entities/profile/profile';
import {BehaviorSubject} from "rxjs";
import {IRepositoryService} from "./interfaces/repository.interface";
import {CryptoService} from "../services/crypto.service";
import {Buddy} from "../entities/buddy";
import {BuddyRepositoryService} from "./buddy-repository.service";
import {FileSystemService, EntityType} from "../services/filesystem.service";
let sjcl = require('../../assets/js/sjcl');
/**
 * This is an unusal repository, as we have only one profile and the
 * repository interface has the methods implements to retrieve multiple.
 */
@Injectable()
export class ProfileRepositoryService implements IRepositoryService {

  static ERROR_PROFILE_NOT_CREATED: number = 404;

  /**
   * The current user.
   */
  profile: Profile = null;

  /**
   * We are using a subject to react to changed made to the profile.
   */
  public profileSubject: BehaviorSubject<Profile>;

  constructor(private fileSystem: FileSystemService,
              private cryptoService: CryptoService/*,
               private buddyRepository: BuddyRepositoryService*/) {
    this.profileSubject = new BehaviorSubject<Profile>(this.profile);
  }

  /**
   * Initially loads the user data after the user has logged in.
   *
   * TODO: Should not be necessary to contact nodes, but just the own profile-attribute
   * this attribute should be renewed automatically
   *
   * @returns {Promise<T>}
   */
  loadProfileData(): Promise<Profile> {

    return new Promise((resolve, reject) => {
      console.log("profile data");
      this.fileSystem.load(EntityType.PROFILE)
        .then((profile) => {
          if (!profile) {
            console.log("Could not find profile");
            reject();
          } else {
            // we have loaded the profile
            this.profile = profile;
            this.profileSubject.next(this.profile);

            // resolve the promise as we have loaded the user.
            resolve(this.profile);
          }
        })
        .catch((error) => {
          // inform the caller that we haven't created any profile yet
          reject(error);
        });
    });

  }

  /**
   * We will stick to the pattern and return an array, but with maximum
   * one element in it.
   *
   * @returns {any}
   */
  findAll(): Observable<Profile[]> {
    return Observable.create((observer: Observer<Profile[]>) => {
      if (null === this.profile) {
        // The current profile is null, throw an error.
        observer.error('The current profile is null');
      }

      // Wrap an array around the profile to stick to the pattern.
      observer.next([this.profile]);

      observer.complete();
    });
  };

  findById(id: string): Observable<Profile> {
    return this.find();
  };

  find(): Observable<Profile> {
    return this.profileSubject.asObservable();
  }

  /**
   * This little functions is the root of our application.
   * With this, the user registers at our page
   *
   * @param name
   * @param password
   *
   * @returns {Observable<Profile>}
   */
  create(name: string, password: string): Promise<any> {
      return new Promise((resolve, reject) => {

        let profile: Profile = new Profile();
        profile.name = name;
        profile.symKey = password;
        profile.publicKey = this.cryptoService.getPublicKey();
        profile.signaturePublicKey = this.cryptoService.getSignaturePublicKey();

        this.fileSystem.getLinkToPublicNode().then(link => {
          console.log("Got Link", link);
        //  profile.id = btoa(link);
          profile.id = link;
          this.profile = profile;

          this.fileSystem.save(EntityType.PROFILE, this.profile).then(res => {
            resolve(res);
          }).catch(err => { reject(err); });
        }).catch(err => { reject(err); });
      });

  };

  /**
   * TODO: Maybe not needed
   * @param profile
   * @returns {Promise<T>}
   */
  save(profile: Profile): Promise<Profile> {
    return new Promise((resolve, reject) => {
      // Write the profile to the file system.

      return this.fileSystem.save(EntityType.PROFILE, this.profile).then(res => {
        this.profile = profile;
        this.profileSubject.next(this.profile);
        resolve(profile);
      }).catch(err => {
        reject(err);
      });
    });
  };

  /**
   * This method will be way to overkill for the first release.
   * I assume by removing the profile we are also removing all the data on the dropbox
   * and every trace that this user ever existed
   * (Ladies and gentlemen, if you will look right here *bright flash*).
   *
   * @param profile Profile
   *
   * @returns {null}
   */
  remove(profile: Profile): Observable<Profile> {
    return null;
  };

  toJSON(): string {
    return '';
  }
}
