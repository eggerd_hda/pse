import {Injectable} from "@angular/core";
import {BuddyRepositoryService} from "./buddy-repository.service";
import {ProfileRepositoryService} from "./profile-repository.service";
import {Profile} from "../entities/profile/profile";
import {PublicFileSystem} from "../services/public-filesystem.service";
import {ChannelRepositoryService} from "./channel-repository.service";
import {FileSystemService, EntityType} from "../services/filesystem.service";

@Injectable()
export class InitService {

  constructor(private buddyRepo: BuddyRepositoryService,
              private profileRepo: ProfileRepositoryService,
              private channelRepo: ChannelRepositoryService,
              private fileSystem: FileSystemService,
              private publicFileSystem: PublicFileSystem) {
  }

  initializeRepositories(profile: Profile): Promise<any> {
    return new Promise((resolve, reject) => {

      this.channelRepo.loadMainChannelFromStorage().then(() => {

        this.fileSystem.load(EntityType.BUDDIES).then(buddies => {

          console.log("You got buddies!", buddies);

          let buddySaves = [];

          for (let i = 0; i < buddies.length; i++) {
            buddies[i].isDirty = false;
            buddySaves.push(this.buddyRepo.save(buddies[i]));
          }

          Promise.all(buddySaves).then(res => {
            console.log("Added all buddies");
            this.channelRepo.initialise();
            this.buddyRepo.initialiseNotificationServiceObserver();
            resolve();
          }).catch((error) => {
            console.error("[InitService]", error);
            reject(error);
          });
        }).catch(err => {
          reject(err);
        });
      }).catch(err => {
        reject(err);
      });
    });
  }
}
