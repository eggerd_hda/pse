import {Injectable} from '@angular/core';
import {Group} from '../entities/group';
import {IRepositoryService} from './interfaces/repository.interface';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observer} from 'rxjs/Observer';

// see post repository as reference
@Injectable()
export class GroupRepositoryService implements IRepositoryService {

  groups: Group[];

  // this one can be subscribed by and gets notifications on changes
  private allGroups: BehaviorSubject<Group[]>;

  constructor() {
    this.groups = [];
    this.allGroups = new BehaviorSubject<Group[]>([]);

    /*this.fileBrowser.write('private/groups', this.groups).subscribe((res) => {
     console.log('Read', res);

     if (!isNullOrUndefined(res)) {
     this.groups = res;
     this.allGroups.next(this.groups); //do this on every update
     }
     }, (errData) => { }, () => { });*/

  }

  findAll(): Observable<Group[]> {
    return this.allGroups.asObservable();
  };

  findById(id: string): Observable<Group> {
    return Observable.create((observer: Observer<Group>) => {

      // getPrivate desired post
      // maybe we need to lazy load it
      let group: Group = this.groups.find((g) => {
        return g.id === id;
      });

      if (!group) {
        observer.next(group);
      } else {
        observer.error(null);
      }

      // finish observable
      observer.complete();
    });
  };

  save(groupToSave: Group): Promise<Group> {
    return new Promise((resolve, reject) => {

      // check if existent already
      let group: Group = this.groups.find((g) => {
        return g.id === groupToSave.id;
      });

      if (!group) {
        group = groupToSave;
        this.groups.push(group);
      } else {
        Object.assign(group, groupToSave);
      }

      this.allGroups.next(this.groups); // update!
      resolve(group);

      /*this.fileBrowser.write('private/groups', this.groups).subscribe((data) => {
       console.log('Written Buddies to storage');
       observer.next(group);
       }, (errData) => {
       // error
       console.error('Could not write groups', errData);
       }, () => {
       // complete
       });*/

      // TODO: do some magic here
      // write back to filesystem
    });
  };

  remove(group: Group): Observable<Group> {
    return Observable.create((observer: Observer<Group>) => {

      // TODO: do some magic here

      // finish observable
      observer.complete();
    });
  };
}
