import {PipeTransform, Pipe} from "@angular/core";
import {EmojiService} from "../services/emoji/emoji.service";

@Pipe({ name: 'emoji' })
export class EmojiPipe implements PipeTransform {

  constructor(private emojiService: EmojiService) {

  }

  transform(value: string): string {
    return this.emojiService.replaceWithAppleEmojis(value);
  }
}
