import {Buddy} from './buddy';
import {Entity} from './interfaces/entity.interface';
import {Post} from './post';
import {CryptoBuddy} from "../services/crypto.service";
import {ChannelMeta} from "../services/publicNode.service";

enum ChannelState {
  CREATED,
  REQUESTED,
  ESTABLISHED
}

export enum ChannelType {
  WALL_CHANNEL,
  CHAT_CHANNEL
}

export class Channel extends Entity {
  state: ChannelState;
  participants: any[] = [];
  name: string = '';
  url: string;
  channelType: ChannelType;
  ownerID: string;
  head: number;
  hashOfLastPost: string;

  newMessages: number = 0;

  constructor(public posts: Post[] = [], _channelType: ChannelType = ChannelType.WALL_CHANNEL) {
    super();
    this.channelType = _channelType;
    this.head = this.posts.length;
  }

  appendPayload(newPost: Post): Channel {
    let index = this.posts.findIndex(post => {
      return post.id === newPost.id;
    });
    if (index === -1) {
      this.posts.push(newPost);
    }else {
      this.posts[index] = newPost;
    }
    this.head++;
    return this;
  }

  getPayload(): Post[] {
    return this.posts;
  }

  fillFromMeta(meta: ChannelMeta): void {
    this.id = meta.id;
    this.url = meta.url;
    this.head = meta.head;
    this.channelType = meta.type;
    this.name = meta.name;
    this.ownerID = meta.ownerID;
    this.hashOfLastPost = meta.hashOfLastPost;
    this.participants = meta.participants;
    this.updatedAt = new Date();
  }

  getMeta(): ChannelMeta {
    return new ChannelMeta(
      this.id,
      this.url,
      this.head,
      this.channelType,
      this.participants,
      this.name,
      this.ownerID,
      this.hashOfLastPost);
  }
}
