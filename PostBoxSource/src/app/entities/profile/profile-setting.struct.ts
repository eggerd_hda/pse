/**
 * Simple holder class to hold a name-value pair of settings.
 */
export class ProfileSetting {
  constructor(public key, public value) {
  }
}
