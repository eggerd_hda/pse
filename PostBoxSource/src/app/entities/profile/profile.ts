import {ProfileRepositoryService} from '../../repositories/profile-repository.service';
import {ProfileSetting} from './profile-setting.struct';
import {User} from "../interfaces/user.interface";
import {Entity} from "../interfaces/entity.interface";

/**
 * The current users profile / account.
 *
 * Holds account information for the current user.
 */
export class Profile extends User {
  asymmetricPrivateKey: string = null;
  asymmetricPublicKey: string = null;

  symKey: string;
  profileSettings: ProfileSetting[];

  constructor() {
    super();
  }

  fillWithJSONData(jsonObject: any): void {
    super.fillWithJSONData(jsonObject);

    // load the profile setting object from the json data.
    this.profileSettings = [];
    if (Array.isArray(jsonObject.profileSettings)) {
      for (let setting of jsonObject.profileSettings) {
        // create the profile settings.
        this.profileSettings.push(new ProfileSetting(setting.key, setting.value));
      }
    }
  }
}
