import {Entity} from './entity.interface';
import {Group} from '../group';

export abstract class User extends Entity {
  /**
   * The name of this user.
   *
   * @type {string}
   */
  public name: string = 'Unknown';

  /**
   * The profile image data stored in a BASE-64 encoded string.
   *
   * @type {string}
   */
  public profileImageData: string = '';
  public publicKey: string;
  public signaturePublicKey: string;
  public groups: Group[] = [];

  hasProfileImage(): boolean {
    return !!this.profileImageData && this.profileImageData.length > 0;
  }
}
