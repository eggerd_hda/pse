import {IRepositoryService} from '../../repositories/interfaces/repository.interface';
import {Observable, Observer} from 'rxjs';

/**
 * An abstract implementation of an entity, because
 * the save(), remove(), and toJSON() methods are the same in
 * nearly every entity.
 */
export abstract class Entity {
  createdAt: Date = new Date();
  isDirty: boolean;
  id: string;
  updatedAt: Date = new Date();

  constructor() {
    this.id = Entity.guid();
    this.createdAt = this.updatedAt = new Date();
  }

  /**
   * A guid function to create a random id for any entity.
   * This function is taken from: http://stackoverflow.com/a/105074
   *
   * @returns {string}
   */
  static guid(): string {
    function s4(): string {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  /**
   * Returns the JSON representation of an Entity
   * @returns {string}
   */
  public toJSON(): string {
    return JSON.stringify(Object.assign({}, this), (key, value) => {
      if (['repository'].indexOf(key) !== -1) {
        return;
      }
      return value;
    });
  }

  fillWithJSONData(jsonObject: any): void {
    for (let prop in jsonObject) {
      if (jsonObject.hasOwnProperty(prop)) {
        this[prop] = jsonObject[prop];
      }
    }
  }
}
