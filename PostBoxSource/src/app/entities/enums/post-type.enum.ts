/**
 * This enumeration is used by the post repository service to create a new post.
 */
export enum PostType {
  Image,
  Text,
  Collection
}
