export class EntityDataPlaceholders {
  /**
   * The id of field of the entity that we are currently working with.
   *
   * @type {string}
   */
  static ID = "{{id}}";

  /**
   * The json encoded entity.
   * @type {string}
   */
  static DATA = "{{data}}";
}
