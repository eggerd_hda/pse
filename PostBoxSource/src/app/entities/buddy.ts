import {BuddyRepositoryService} from '../repositories/buddy-repository.service';
import {User} from './interfaces/user.interface';
import {Group} from "./group";

export class Buddy extends User {

  /**
   * Used by the NotificationManager to prioritize the polling.
   * Small numbers mean, the buddy is posting frequently.
   * Higher numbers indicate inactivity.
   *
   * @type {number}
   */
  private pollingFrequency: number = 0;

  /**
   * Used by the NotificationManager to check for changes
   * @type {string}
   */
  private profileHash: string = null;


  /**
   * The groups this buddy is part of.
   *
   * @type {Array}
   */
  public groups: Group[] = [];

  /**
   * A flag that indicates if this buddy has also
   * set the current user as a buddy, because if not, the
   * only thing we have from this buddy is the id.
   *
   * If isAccepted is <code>false</code>, this is no
   * real buddy, but a mere buddy request.
   */
  public isAccepted: boolean = true;


  /**
   *  Used by NotificationService & BuddyRepo
   *
   *  flag for chat-only-buddies
   *
   *  A flag that indicates if this buddy was actually
   *  invited by the user.
   *  The chat needs to be capable of communicating
   *  with third party buddies.
   *  Therefore there are buddies, we do not want to be listed
   *  in your buddyList, as we have never invited them, nor do
   *  we wont them to have access to our posts.
   */
  public isChatOnlyBuddy: boolean = true;


  constructor() {
    super();
  }

  /**
   * Used by the NotificationManager to prioritize the polling.
   * Gets the current PollingFrequency
   *
   * @returns {number} Small numbers mean, the buddy is posting
   * frequently. Higher numbers indicate inactivity.
   */
  public getPollingFrequency(): number {
    return this.pollingFrequency;
  }

  /**
   * Used by the NotificationManager to prioritize the polling.
   * Sets the PollingFrequency, a value that represents the
   * priority of a buddy in the context of notification polling.
   *
   * @param value - Small numbers mean, the buddy is posting frequently. Higher numbers indicate inactivity.
   */
  public setPollingFrequency(value: number): void {
    this.pollingFrequency = value;
  }


  /**
   * Used by the NotificationManager to check if user has been polled before
   * @returns {boolean}
   */
  public hasProfileHasBeenInitialized(): boolean {
    return this.profileHash != null;
  }

  /**
   * Used by the NotificationManager to check if something in buddies users has changed
   * @param newProfileHash
   * @returns {boolean}
   */
  public hasProfileHashChanged(newProfileHash: string) {
    if (this.profileHash != newProfileHash) {
      let hashHasBeenInitialized = this.hasProfileHasBeenInitialized();
      this.profileHash = newProfileHash;
      return hashHasBeenInitialized;
    }
    return false;
  }

}
