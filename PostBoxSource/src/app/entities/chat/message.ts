export class Message {

  public ownMessage: boolean = false;
  public message: string;
  public author: string;
  public date: any;


  public static create(ownMessage: boolean, messageText: string, author: string, date: any) {
    let message = new Message();
    message.ownMessage = ownMessage;
    message.message = messageText;
    message.author = author;
    message.date = date;
    return message;
  }
}
