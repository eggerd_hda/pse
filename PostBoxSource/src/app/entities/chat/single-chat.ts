import {Message} from "./message";
import {Observable} from "rxjs";
import {Channel} from "../channel";
import {User} from "../interfaces/user.interface";
import {BuddyRepositoryService} from "../../repositories/buddy-repository.service";
import {Buddy} from "../buddy";
export class SingleChat {

  /**
   * All messages from this chat.
   */
  public messages: Message[] = [ ];
  public channel: Channel;
  public name: string = "";

  public buddies: Buddy[] = [ ];

  constructor(private buddyRepo: BuddyRepositoryService, public channelObservable: Observable<Channel>, user: User) {
    channelObservable.subscribe(channel => {

      // Fix for double notify
      if (this.messages.length === channel.posts.length) {
        return;
      }

      this.channel = channel;

      this.buddies = channel.participants
        .map(buddy => {
          return this.buddyRepo.buddies.find((element) => { return element.id === buddy.identifier; });
        })
        .filter((element) => {
          return !!element;
        });

      // Load the messages.
      this.messages = channel.posts.map(post => {

        if (post.creatorID === user.id) {
          return Message.create(true, post.message, "You", post.createdAt);
        }

        let creator = this.buddyRepo.buddies.find(buddy => {
          return buddy.id === post.creatorID;
        });

        if (!creator) {
          creator = channel.participants.find(participant => {
            return participant.identifier === post.creatorID;
          });
        }

        if (creator) {
          return Message.create(false, post.message, creator.name, post.createdAt);
        } else {
          return Message.create(false, post.message, "Unknown", post.createdAt);
        }

      });

      this.name = channel.name;
    });
  }

  public isInChat(buddy: Buddy): boolean {
    return !!this.buddies.find(element => { return element.id === buddy.id; });
  }
}
