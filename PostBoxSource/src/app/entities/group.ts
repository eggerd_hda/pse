import {Buddy} from './buddy';
import {Entity} from "./interfaces/entity.interface";

export class Group extends Entity {
  name: string;
  buddies: Buddy[] = [];

  constructor() {
    super();
  }
}
