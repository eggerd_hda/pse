import {PostType} from './enums/post-type.enum';
import {User} from "./interfaces/user.interface";
import {Entity} from "./interfaces/entity.interface";

export class Post extends Entity {

  constructor(public postType: PostType,
              public creatorID: string,
              public message: string,
              public head: number) {
    super();
  }
}
