import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {FileSizePipe} from './pipes/file-size.pipe';
import {BuddyRepositoryService} from './repositories/buddy-repository.service';
import {GroupRepositoryService} from './repositories/group-repository.service';
import {CryptoService} from './services/crypto.service';
import {NotificationService} from './services/notification.service';
import {DropboxAdapterService} from './services/dropbox-adapter.service';
import {WallComponent} from './components/wall/wall.component';
import {RouterModule} from '@angular/router';
import {BuddylistPageComponent} from './pages/buddylist/buddylist.page';
import {NavigationComponent} from './components/navigation/navigation.component';
import {UserImageComponent} from './pages/user/user-image/user-image.component';
import {AddBuddyDialog} from './pages/buddylist/addBuddyDialog/add-buddy.dialog';
import {CommonModule} from '@angular/common';
import {MdlModule, MdlDialogReference} from 'angular2-mdl';
import {PostComponent} from './components/wall/post/post.component';
import {ProfileRepositoryService} from './repositories/profile-repository.service';
import {UploadComponent} from './pages/user/upload/upload.component';
import {FileSystemService} from './services/filesystem.service';
import {ProfileCreationComponent} from './pages/login/profile/profile-creation.component';
import {DropboxLoginComponent} from './components/login/styled-dropbox-login.component';
import {LoginPage} from './pages/login/login.page';
import {PublicFileSystem} from './services/public-filesystem.service';
import {ProfileLoginComponent} from './pages/login/profile/profile-login.component';
import {InitService} from './repositories/init.service';
import {ChannelRepositoryService} from './repositories/channel-repository.service';
import {FullScreenLoaderComponent} from './components/full-screen-loader/full-screen-loader.component';
import {AccordionComponent} from './components/accordion/buddy-list.component';
import {PrivateNodeService} from './services/privateNode.service';
import {PublicNodeService} from './services/publicNode.service';
import {DragDropFieldComponent} from './pages/user/upload/drag-drop-field.component';
import {UserPage} from './pages/user/user.page';
import {OptionsPage} from './pages/options/options.page';
import {HomePage} from './pages/home/home.page';
import {ProfileImageUploadComponent} from './pages/user/image-upload-dialog/profile-image-upload.component';
import {AppRoutingModule} from './app.routing.module';
import {AuthGuard} from "./services/auth-guard.service";
import {ChatComponent} from "./components/chat/chat.component";
import {ChatPage} from "./pages/chat/chat.page";
import {QRCodeModule} from 'angular2-qrcode';
import {AddChatComponent} from "./components/chat/modals/add-chat.component";
import {SelectableListComponent} from "./components/chat/selectable-list/selectable-list.component";
import {PostboxPipeService} from "./services/pipe/pipe.service";
import {MomentService} from "./services/moment/moment.service";
import {MessageContainerComponent} from "./components/chat/message-container.component";
import {EmojiService} from "./services/emoji/emoji.service";
import {FullscreenQRComponent} from "./components/qr/fullscreen-qr.component";
import {EmojiPipe} from "./pipes/emoji.pipe";
import {ListDialogComponent} from "./components/chat/selectable-list/list-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    FileSizePipe,
    WallComponent,
    UserPage,
    HomePage,
    ChatPage,
    BuddylistPageComponent,
    NavigationComponent,
    UserImageComponent,
    AddBuddyDialog,
    PostComponent,
    UploadComponent,
    LoginPage,
    OptionsPage,
    DropboxLoginComponent,
    ProfileCreationComponent,
    FullScreenLoaderComponent,
    ProfileLoginComponent,
    DragDropFieldComponent,
    AccordionComponent,
    ProfileImageUploadComponent,
    ChatComponent,
    AddChatComponent,
    SelectableListComponent,
    MessageContainerComponent,
    FullscreenQRComponent,
    EmojiPipe,
    ListDialogComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdlModule,
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    QRCodeModule,
  ],
  providers: [
    BuddyRepositoryService,
    GroupRepositoryService,
    ProfileRepositoryService,
    PrivateNodeService,
    CryptoService,
    DropboxAdapterService,
    NotificationService,
    PublicFileSystem,
    PublicNodeService,
    FileSystemService,
    InitService,
    ChannelRepositoryService,
    FullScreenLoaderComponent,
    AuthGuard,
    PostboxPipeService,
    MomentService,
    EmojiService,
  ],
  entryComponents: [AddBuddyDialog, ProfileImageUploadComponent, AddChatComponent, ListDialogComponent],
  bootstrap: [AppComponent],
})
export class AppModule {
}
