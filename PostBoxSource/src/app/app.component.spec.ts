/* tslint:disable:no-unused-variable */

import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NavigationComponent} from './components/navigation/navigation.component';
import {BuddyRepositoryService} from './repositories/buddy-repository.service';
import {GroupRepositoryService} from './repositories/group-repository.service';
import {ProfileRepositoryService} from './repositories/profile-repository.service';
import {CryptoService} from './services/crypto.service';
import {DropboxAdapterService} from './services/dropbox-adapter.service';
import {NotificationService} from './services/notification.service';
import {HttpModule} from "@angular/http";
import {PublicFileSystem} from "./services/public-filesystem.service";
import {FileSystemService} from "./services/filesystem.service";
import {PublicNodeService} from "./services/publicNode.service";
import {PrivateNodeService} from "./services/privateNode.service";
import {ChannelRepositoryService} from "./repositories/channel-repository.service";

describe('App: PostBoxSource', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NavigationComponent,
      ],
      imports: [
        RouterTestingModule,
        HttpModule,
      ],
      providers: [
        CryptoService,
        DropboxAdapterService,
        NotificationService,
        BuddyRepositoryService,
        ChannelRepositoryService,
        GroupRepositoryService,
        ProfileRepositoryService,
        PublicFileSystem,
        FileSystemService,
        PrivateNodeService,
        PublicNodeService
      ],
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
