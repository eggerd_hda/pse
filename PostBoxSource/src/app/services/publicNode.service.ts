import {Injectable} from "@angular/core";
import {ICloudStorageAdapterInterface} from "./interfaces/cloud-storage-adapter.interface";
import {Channel, ChannelType} from "../entities/channel";
import {environment} from "../../environments/environment";
import {Profile} from "../entities/profile/profile";
import {CryptoBuddy, CryptoService} from "./crypto.service";
import {PostType} from "../entities/enums/post-type.enum";
import {BuddyRepositoryService} from "../repositories/buddy-repository.service";
import {Post} from "../entities/post";

export class PublicProfileInformation {
  constructor(
    public id: string,
    public userName: string,
    public publicKey: string,
    public signaturePublicKey: string
  ) {}
}

export class PrivateProfileInformation {
  constructor(
    public profileImageData: string
  ) {}
}

export class ChannelMeta {
  constructor(
    public id: string,
    public url: string,
    public head: number,
    public type: ChannelType,
    // PublicUrls of Participants
    public participants: any,
    public name: string,
    public ownerID: string,
    public hashOfLastPost: string
  ) {}
}


@Injectable()
export class PublicNodeService {
  private storageAdapter: ICloudStorageAdapterInterface;
  private buddyRepo: BuddyRepositoryService;

  private publicProfileInformations: any;
  private privateProfileInformations: any;

  private channelMetas: any = {};
  private cachedChannelMetas: any = {};

  private cachedChannels: any = {};

  constructor(private cryptoService: CryptoService) {
  }

  public initialize(storageAdapter: ICloudStorageAdapterInterface, buddyRepo: BuddyRepositoryService, clean: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageAdapter = storageAdapter;
      this.buddyRepo = buddyRepo;
      if (clean) {
        this.storageAdapter.writeFile(environment.PUBLIC_NODE, "{}")
          .then(() => { resolve(); })
          .catch(err => { reject(err); });
      }else {
        resolve();
      }
    });


  }

  /**
   * TODO: Maybe also cache channelmeta
   * Remember dont load all channel data, only metas!
   * @returns {null}
   */
  public loadFromStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageAdapter.getFile(environment.PUBLIC_NODE).then(res => {

          let publicFile = JSON.parse(res);

          console.log("[PublicNode] Got public node", publicFile);

          this.publicProfileInformations = publicFile.publicProfileInformations;
          this.privateProfileInformations = publicFile.privateProfileInformations;
          console.log("Got Profile infos");
          this.channelMetas = publicFile.channelMetas;


          for (let meta in this.channelMetas) {
            if (this.channelMetas.hasOwnProperty(meta)) {
              let decryptedMeta = this.cryptoService.decrypt(this.channelMetas[meta], false);
              let parsed = JSON.parse(decryptedMeta);
              this.cachedChannelMetas[parsed.id] = parsed;
            }
          }

          console.log("Got ChannelMetas", this.cachedChannelMetas);

          resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  /**
   * Saves profile information for a profile
   * @param profile
   * @returns {Promise<any>}
   */
  public saveProfileInformation(profile: Profile): Promise<any> {

    console.log("[PublicNode] Saving profile", profile);

    let publicProfile = new PublicProfileInformation(profile.id, profile.name, profile.publicKey, profile.signaturePublicKey);
    let privateProfile = new PrivateProfileInformation(profile.profileImageData);

    let buddies = this.buddyRepo.buddies.map(buddy => { return new CryptoBuddy(buddy.id, buddy.publicKey); });

    this.publicProfileInformations = JSON.stringify(publicProfile);
    this.privateProfileInformations = this.cryptoService.encrypt(JSON.stringify(privateProfile), buddies);

    return this.save();
  }



  /**
   * TODO: TEST
   * @param channel
   * @returns {Promise<any>}
   */
  public saveChannel(channel: Channel): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.writeChannelFile(channel).then(() => {

          let participants = channel.participants;

          let profileInfo = JSON.parse(this.publicProfileInformations);

          let userIndex = channel.participants.findIndex(buddy => { return buddy.identifier === profileInfo.id; });

          // Add user to participants (TODO: Only when channel creater)
          if (userIndex === -1) {
            console.log("Setting myself as participant", profileInfo);
            participants.push({identifier: profileInfo.id, publicKey: profileInfo.publicKey, name: profileInfo.userName});
          }

          let channelMeta = channel.getMeta();

          console.log("Saving channelmeta..", channelMeta);

          // Save in cache
          this.cachedChannelMetas[channel.id] = channelMeta;


          //let cryptoBuddies = channel.participants.map

        // Save encrypted
          this.channelMetas[channel.id] = this.cryptoService.encrypt(JSON.stringify(channelMeta), channel.participants);

          // Save cache
          this.cachedChannels[channel.id] = channel;

          this.save().then(() => {
            resolve();
          }).catch(err => {
            reject(err);
          });
      }).catch(err => {
        reject(err);
      });
    });
  }

  /**
   * Saves only the meta information
   * @param channelMeta
   */
  public saveChannelMeta(channelMeta: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {

      console.log("Saving channelmeta..", channelMeta);
      this.cachedChannelMetas[channelMeta.id] = channelMeta;
      this.channelMetas[channelMeta.id] = this.cryptoService.encrypt(JSON.stringify(channelMeta), channelMeta.participants);

      this.save().then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  public getChannelMeta(id: string): Promise<ChannelMeta> {
    return new Promise((resolve, reject) => {
        if (this.cachedChannelMetas[id]) {
          resolve(this.cachedChannelMetas[id]);
        } else {
          reject("A ChannelMeta with id " + id + " is not existing!");
        }
    });
  }

  public getChannelMetas(): Promise<ChannelMeta[]> {
    return new Promise((resolve, reject) => {
      let metas = [];
      for (let meta in this.cachedChannelMetas) {
        if (this.cachedChannelMetas.hasOwnProperty(meta)) {
          metas.push(this.cachedChannelMetas[meta]);
        }
      }
      resolve(metas);
    });
  }

  /**
   * TODO: Think of: DO we really want to load all metas at the beginning and can be sure, they are encrypted?
   * => Rather implement fallback
   * @param id
   * @returns {null}
   */
  public getChannel(id: string): Promise<Channel> {
    return new Promise((resolve, reject) => {

      if (this.cachedChannels[id]) {
        resolve(this.cachedChannels[id]);
      }

      /*
      let url;
      if (this.cachedChannelMetas[id]) {
          url = this.cachedChannelMetas[id].url;
      }else {
        // TODO: Just for now, if we read all metas from beginning
        reject("Didnt save meta");
      }
*/
      // Get posts
      this.storageAdapter.getFile(id).then(res => {

          let decryptedPosts = this.cryptoService.decrypt(res, false);

          if (typeof decryptedPosts === "string" || decryptedPosts instanceof String) {
            decryptedPosts = JSON.parse(decryptedPosts);
          }

          let posts = [];

          for (let post of decryptedPosts) {

            let decodedPost = post;

            if (typeof decodedPost === "string") {
              decodedPost = JSON.parse(decodedPost);
            }

            posts.push(decodedPost);
          }


          // TODO: Load all other stuff
          let newChannel = new Channel(posts, this.cachedChannelMetas[id].type);
          newChannel.fillFromMeta(this.cachedChannelMetas[id]);
          // Save in cache
          this.cachedChannels[id] = newChannel;

          console.log("Saved channel in cache");

          resolve(newChannel);
      }).catch(err => {
        reject(err);
      });
    });
  }

  public getLinkToID(id: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.storageAdapter.createSharedLink(id).then(() => {
        resolve(this.storageAdapter.getSharedLink(id));
        // Link is already existing
      }).catch((err) => {
        // File is not existing
        if (err.status === 409) {
          this.storageAdapter.writeFile(id, "{}").then(() => {
            resolve(this.getLinkToID(id));
          }).catch(err => { reject(err); });
        }else {
          resolve(this.storageAdapter.getSharedLink(id));
        }
      });
    });
  }

  /**
   * Saves the PublicNode in storage
   * @returns {Promise<any>}
   */
  private save(): Promise<any> {
    return this.storageAdapter.writeFile(environment.PUBLIC_NODE, this.toJSON());
  }


  /**
   * Transform the public node to JSON-string
   * @returns {string}
   */
  private toJSON(): string {

    let profileHash = this.cryptoService.filenameHash(this.publicProfileInformations + this.privateProfileInformations);

    let jsonObj = {publicProfileInformations: this.publicProfileInformations,
                  privateProfileInformations: this.privateProfileInformations,
                  channelMetas: this.channelMetas,
                  profileHash: profileHash};

    return JSON.stringify(jsonObj);
  }

  /**
   * Writes the payload of a channel to its url file
   * @param channel
   * @returns {Promise<any>}
   */
  private writeChannelFile(channel: Channel): Promise<any> {

    let encryptedChannelPosts = this.cryptoService.encrypt(JSON.stringify(channel.getPayload()), channel.participants);

    return this.storageAdapter.writeFile(channel.id, encryptedChannelPosts);
  }
}
