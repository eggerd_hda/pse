/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {DropboxAdapterService} from './dropbox-adapter.service';

describe('Service: DropboxAdpater', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DropboxAdapterService],
    });
  });

  it('should ...', inject([DropboxAdapterService], (service: DropboxAdapterService) => {
    expect(service).toBeTruthy();
  }));
});
