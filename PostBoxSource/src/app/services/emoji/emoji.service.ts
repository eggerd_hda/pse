import {Injectable} from "@angular/core";
import {toUnicode} from "punycode";

declare let $: any;



@Injectable()
export class EmojiService {

  private isReady: boolean = false;

  private emojiData: any;

  constructor() {
    this.isReady = false;

    $(document).ready(() => {
      this.mapEmojiData();
      this.isReady = true;
    });
  }

  public replaceWithAppleEmojis(value: string): string {
    let regex = /{:(.*?):}/g,
      match;

    if (!this.emojiData) {
      this.mapEmojiData();
    }

    while (match = regex.exec(value)) {
      value = this.replaceSingleShortcodeWithEmoji(value, match);
    }

    return value;
  }

  public replaceWithShortcodes(value: string): string {
    let regex = /(\ud83c[\udf00-\udfff]|\ud83d[\udc00-\ude4f]|\ud83d[\ude80-\udeff])/g,
      match;

    // query through all matches
    while (match = regex.exec(value)) {
      value = this.replaceSingleEmojiWithShortcode(value, match);
    }

    return value;
  }

  private replaceSingleEmojiWithShortcode(original: string, match: any): string {
    let index = match.index;
    let emoji = match[0];

    let shortcode = this.emojiData[emoji];

    if (!!shortcode) {
      return original.substr(0, index) + "{:" + shortcode + ":}" + original.substr(index + 2);
    }

    return original;
  }

  private replaceSingleShortcodeWithEmoji(original: string, match: any): string {
    let valueInString = match[0],
      shortcode = match[1];

    for (let index in this.emojiData) {
      if (this.emojiData.hasOwnProperty(index)) {
        if (this.emojiData[index] === shortcode) {

          let replaceString = `<span class="pb-emoji emoji emoji-${shortcode}"></span>`;
          return original.replace(valueInString, replaceString);
        }
      }
    }

    return original;
  }

  private mapEmojiData(): void {
    let originalData = $.fn.emojiPicker.emojis;

    let emojiData = { };
    for (let row of originalData) {
      if (!row.unicode.apple) {
        // the apple unicode does not exist.
        continue;
      }

      emojiData[this.toUnicode(row.unicode.apple)] = row.shortcode;
    }

    this.emojiData = emojiData;
  }

  private toUnicode(code): string {
    try {
      let codes = code.split('-').map(function (value, index) {
        return parseInt(value, 16);
      });
      return String.fromCodePoint.apply(null, codes);
    } catch (error) {
      console.log(error);
    }
    return "";
  }
}
