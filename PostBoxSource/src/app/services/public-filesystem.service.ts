import {Injectable} from '@angular/core';
import {RequestOptions, Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {CryptoService} from "./crypto.service";
import {Buddy} from "../entities/buddy";
import {ChannelMeta, PublicProfileInformation, PrivateProfileInformation} from "./publicNode.service";
import {environment} from "../../environments/environment";

@Injectable()
export class PublicFileSystem {

  constructor(private http: Http,
              private cryptoService: CryptoService) {
  }

  /**
   * Gets ProfileInformations from a Buddy
   * @param buddyUrl
   * @returns {Promise<PublicProfileInformation>}
   */
  public getPublicProfileInformations(buddy: Buddy): Promise<PublicProfileInformation> {
   // let buddyUrl: string = atob(buddy.id);
    let buddyUrl: string = buddy.id;
    return new Promise((resolve, reject) => {
      this.readPublicFile(buddyUrl).subscribe(res => {
          try {
            resolve(JSON.parse(res.publicProfileInformations));
          }catch (err) {
            console.log("Could not read public information of buddy!");
            reject(err);
          }
      }, err => { reject(err); });
    });
  }


  /*
  public getPrivateProfileInforamtions(buddy: Buddy): Promise<PrivateProfileInformation> {
    // let buddyUrl: string = atob(buddy.id);
    let buddyUrl: string = buddy.id;
    return new Promise((resolve, reject) => {
      this.readPublicFile(buddyUrl).subscribe(res => {
        try {
          let decrypted = this.cryptoService.decrypt(res.privateProfileInformations, false);
          let parsed = JSON.parse(decrypted);
          resolve(parsed);
        } catch (err) {
          console.log("You don't got rights to read this buddies personal information!");
          reject(err);
        }
      });
    });
  }

  */

  public parsePrivateProfileInforamtions(publicFile: any): PrivateProfileInformation {
      try {
        let decrypted = this.cryptoService.decrypt(publicFile.privateProfileInformations, false);
        let parsed = JSON.parse(decrypted);
        return parsed;
      } catch (err) {
        console.log("You don't got rights to read this buddies personal information!");
        return null
      }
  }

  /**
   * Gets all Posts of a CHANNEL
   * @returns {Promise<string>}
   * @param channelUrl
   */
  public getChannelPosts(channelUrl: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.readPublicFile(channelUrl).subscribe(res => {
        if (typeof res !== 'string') {
          res = JSON.stringify(res);
        }
        let decryptedPosts = this.cryptoService.decrypt(res, false);
        resolve(decryptedPosts);
      }, err => { reject(err); });
    });
  }

  /**
   * Used by NotiManager to getPrivate all ChannelMeta-Objects from a buddy, that are available for the user
   * @returns {Promise<ChannelMeta[]>}
   * @param buddy
   */
  public parseChannelMetas(publicNode: any): ChannelMeta[] {

      // console.debug("[PublicFileSystem] parsing ChannelMetas from publicNode: ", publicNode);

      try {
        let channelMetas = [];
        if (!environment.production && publicNode.channelMetas.length === 0) {
          console.debug("[PublicFileSystem] parsing ChannelMetas did not find any channels");
        }

        for (let meta in publicNode.channelMetas) {
          if (publicNode.channelMetas.hasOwnProperty(meta)) {
            try {
              let decryptedMeta = this.cryptoService.decrypt(publicNode.channelMetas[meta], false);

              if (decryptedMeta) {
                channelMetas.push(JSON.parse(decryptedMeta));
              }
            } catch (err) {
            //  console.log("Skipping meta.. not in header");
            }
          }
        }
        return channelMetas;
      }
      catch (err) {
       console.error(err);
       return[];
      }
  }

  public readPublicFile(url: string): Observable<any> {
    let headers = new Headers({
      //   'Authorization': 'Bearer ' + token, // Das ist nicht nötig
      'Content-Type': 'application/json',
    });
    let options = new RequestOptions({headers: headers});
    return this.http.get(url + "?r=" + Date.now(), options)
      .map((resp: Response) => resp.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
