import {Injectable} from "@angular/core";
import {ICloudStorageAdapterInterface} from "./interfaces/cloud-storage-adapter.interface";
import {Buddy} from "../entities/buddy";
import {Profile} from "../entities/profile/profile";
import {environment} from "../../environments/environment";
import {CryptoService} from "./crypto.service";

@Injectable()
export class PrivateNodeService {

  private storageAdapter: ICloudStorageAdapterInterface;

  private buddies: Buddy[] = [];
  private profile: Profile;

  constructor(private cryptoService: CryptoService) {
  }

  /**
   * @param storageAdapter
   */
  public initialize(storageAdapter: ICloudStorageAdapterInterface): void {
    this.storageAdapter = storageAdapter;
  }

  /**
   * TODO: Test
   * Loads PrivateNode data from storage
   * @returns {Promise<any>}
   */
  public loadFromStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageAdapter.getFile(environment.PRIVATE_NODE).then((serializedPrivateNode) => {
        let decryptedPrivateNode = this.cryptoService.decrypt(serializedPrivateNode);
        let parsedNode = JSON.parse(decryptedPrivateNode);

        let profile = new Profile();
        profile.fillWithJSONData(JSON.parse(parsedNode.profile));
        this.profile = profile;

        for (let buddyData of parsedNode.buddies) {
          let buddy = new Buddy();
          buddy.fillWithJSONData(JSON.parse(buddyData));
          this.buddies.push(buddy);
        }
        resolve();
      }).catch((error) => {
        console.log("the private note does not exist yet or json file is damaged: " + error);
        reject(error);
      });
    });
  }


  /**
   * TODO: Test
   * @param buddyToSave
   * @returns {Promise<any>}
   */
  public saveBuddy(buddyToSave: Buddy): Promise<any> {

    let index = this.buddies.findIndex((buddy) => {
      return buddy.id === buddyToSave.id;
    });

    if (index === -1) {
      this.buddies.push(buddyToSave);
    } else {
      this.buddies[index] = buddyToSave;
    }

    return this.save();
  }

  /**
   * TODO: Test
   * @param id
   * @returns {null}
   */
  public getBuddy(id: string): Buddy {
    let retBuddy = this.buddies.filter(buddy => {
      return buddy.id === id;
    });

    if (retBuddy.length > 0) {
      return retBuddy[0];
    } else {
      return null;
    }
  }

  /**
   * Returns all buddies
   * @returns {Buddy[]}
   */
  public getBuddies(): Buddy[] {
    return this.buddies;
  }

  /**
   * saves the profile
   * @param profile
   * @returns {Promise<any>}
   */
  public saveProfile(profile: Profile): Promise<any> {
    this.profile = profile;
    return this.save();
  }

  /**
   * @returns {Profile}: The saved profile
   */
  public getProfile(): Profile {
    return this.profile;
  }


  /**
   * Saves the PrivateNode on the CloudStorage
   * @returns {Promise<any>}
   */
  private save(): Promise<any> {

    let json: string = this.toJSON();

    let encryptedData = this.cryptoService.encrypt(json);

    return this.storageAdapter.writeFile(environment.PRIVATE_NODE, encryptedData);
  }

  /**
   * Transform the object to a JSON-string
   * @returns {string}
   */
  private toJSON(): string {

    let jsonObj = {profile: this.profile, buddies: this.buddies};

    return JSON.stringify(jsonObj);
  }
}
