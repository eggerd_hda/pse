import {Injectable} from '@angular/core';
import {ICloudStorageAdapterInterface} from './interfaces/cloud-storage-adapter.interface';

let Dropbox = require('dropbox');

@Injectable()
export class DropboxAdapterService implements ICloudStorageAdapterInterface {

  public userToken: string = null;

  private filePath = '/';

  private clientId = 'x8qfkcpvxavv9b0';

  private dbx = new Dropbox({clientId: this.clientId});

  constructor() {

    if (localStorage.getItem('userToken')) {
      this.userToken = localStorage.getItem('userToken');
    } else if (this.hasAuthenticatedInformationInUrl() && !this.userToken) {
      this.userToken = this.getAccessTokenFromUrl().access_token;
      localStorage.setItem('userToken', this.userToken);
    }
    if (this.userToken) {
      this.dbx.setAccessToken(this.userToken);
    }

    if (this.hasAuthenticatedInformationInUrl()
      && null === this.userToken) {
      /*
       We just logged in and returned from the dropbox service.
       Set the access token and the {@link CloudStorageProviderService}
       */
      this.userToken = this.getAccessTokenFromUrl().access_token;
      this.dbx.setAccessToken(this.userToken);
    }
  }

  public login(): void {
    window.location.href = this.dbx.getAuthenticationUrl(window.location.origin + '/');
  }

  public logout(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.dbx.authTokenRevoke();
      this.userToken = null;
      localStorage.clear();
      // window.location.href = 'http://localhost:4200/';
      resolve();
    });
  }

  /**
   * Checks if we have an access token stored in this class.
   * Will not look in the url if there is an access token provided.
   *
   * @returns {boolean}
   */
  public isAuthenticated(): boolean {
    //console.log("Authenticated", this.userToken !== null);
    return null !== this.userToken;
  }

  public writeFile(fileName: string, data: string): Promise<any> {
    return this.dbx.filesUpload({
      contents: data,
      mode: 'overwrite',
      path: this.filePath + fileName,
    });
  }

  public deleteFile(fileName: string): Promise<any> {
    return this.dbx.filesDelete({
      path: this.filePath + fileName,
    });
  }

  public createSharedLink(path: string): Promise<any> {
    return this.dbx.sharingCreateSharedLink({
      path: this.filePath + path,
      short_url: false,
    });
  }

  /**
   * @param path
   * @returns {Promise<string>}
   */
  public getSharedLink(path: string): Promise<any> {
    return new Promise<string>((resolve, reject) => {
      this.createSharedLink(path).then(res => {
        let url = res.url.replace('dl=0', 'dl=1').replace('www.dropbox.com', 'dl.dropboxusercontent.com');
        resolve(url);
      }).catch(err => {
        reject(err);
      });
    });
  }

  public getFreeSpace(): any {
    this.dbx.usersGetSpaceUsage().then((response) => {
      console.log(response);
    }).catch((error) => {
      console.log(error);
    });
  }

  public getFile(file: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = 'https://api-content.dropbox.com/1/files/auto/' + file;
      let xhr = new XMLHttpRequest();
      xhr.onreadystatechange = handleStateChange;
      xhr.open('GET', url, true);
      xhr.setRequestHeader('Authorization', 'Bearer ' + this.userToken);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send();
      function handleStateChange(): void {
        if (this.readyState === 4) {
          if (200 === this.status) {
            resolve(this.responseText);
          } else {
            reject(this.status);
          }
        }
      }
    });
  }

  private hasAuthenticatedInformationInUrl(): boolean {
    return !!this.getAccessTokenFromUrl().access_token;
  }

  private getAccessTokenFromUrl(): any {
    let url = window.location.hash;
    let ret = Object.create(null);
    if (typeof url !== 'string') {
      return ret;
    }

    url = url.trim().replace(/^(\?|#|&)/, '');
    if (!url) {
      return ret;
    }

    url.split('&').forEach((param) => {
      let parts = param.replace(/\+/g, ' ').split('=');
      // Firefox (pre 40) decodes `%3D` to `=`
      // https://github.com/sindresorhus/query-string/pull/37
      let key = parts.shift();
      let val = parts.length > 0 ? parts.join('=') : undefined;
      key = decodeURIComponent(key);
      // missing `=` should be `null`:
      // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
      val = val === undefined ? null : decodeURIComponent(val);

      if (ret[key] === undefined) {
        ret[key] = val;
      } else if (Array.isArray(ret[key])) {
        ret[key].push(val);
      } else {
        ret[key] = [ret[key], val];
      }
    });
    return ret;
  }

}
