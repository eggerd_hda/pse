
import {TestBed, async, inject} from '@angular/core/testing';
import {CryptoService} from "./crypto.service";
import {DropboxAdapterService} from "./dropbox-adapter.service";
import {NotificationService} from "./notification.service";
import {PublicFileSystem} from "./public-filesystem.service";

describe('Service: PublicFileSystem', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CryptoService, DropboxAdapterService, NotificationService , PublicFileSystem],

    });
  });

});
