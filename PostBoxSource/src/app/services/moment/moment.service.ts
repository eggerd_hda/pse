import {Injectable} from "@angular/core";

declare let moment: any;

/**
 * A simple service to be used by any component that needs to format dates.
 *
 * Documentation of momentjs can be found here:
 *  https://momentjs.com/docs/
 */
@Injectable()
export class MomentService {

  /**
   * Converts a time/date object to a formatted date time string.
   * The predefined format is a date with the time.
   *
   * @param original    Tbe original date object/string.
   * @param format      The format that should be used to format.
   *                    Formats can be found here: https://momentjs.com/docs/#/displaying/format/
   *
   *
   * @returns           The formatted datetime string.
   */
  public toDateTimeString(original: any, format = "DD.MM.YYYY HH:mm"): string {
    return moment(original).format(format);
  }

  /**
   * Converts a time/date object to a formatted date string.
   * The predefined format is a date without time.
   *
   * @param original    Tbe original date object/string.
   * @param format      The format that should be used to format.
   *                    Formats can be found here: https://momentjs.com/docs/#/displaying/format/
   *
   *
   * @returns           The formatted date string.
   */
  public toDateString(original: any, format = "DD.MM.YYYY"): string {
    return moment(original).format(format);
  }

  /**
   * Converts a time/date object to a formatted time string.
   * The predefined format is a time without the date
   *
   * @param original    Tbe original date object/string.
   * @param format      The format that should be used to format.
   *                    Formats can be found here: https://momentjs.com/docs/#/displaying/format/
   *
   *
   * @returns           The formatted time string.
   */
  public toTimeString(original: any, format = "HH:mm"): string {
    return moment(original).format(format);
  }


  /**
   * Converts a time/date object to a formatted date string.
   * The predefined format is either date with time, or just time.
   *
   * If the original date is today, it will only display the time. If the
   * date is not today, it will display time and date.
   *
   * @param original    Tbe original date object/string.
   * @param format      The format that should be used to format.
   *                    Formats can be found here: https://momentjs.com/docs/#/displaying/format/
   *
   *
   * @returns           The formatted date string.
   */
  public toHumanReadableString(original: any): string {
    let now = moment();
    let object = moment(original);

    if (!now.isSame(object, 'day')) {
      return this.toDateTimeString(original);
    }

    return this.toTimeString(original);
  }

  /**
   * Converts a date object/string to a moment object so that we can use momentjs.
   *
   * @param original      The original date string/object.
   *
   * @returns {any}       The moment object representing the original date object/string.
   */
  public toMoment(original: any): any {
    return moment(original);
  }
}
