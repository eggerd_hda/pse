import {Injectable} from '@angular/core';
import {Buddy} from '../entities/buddy';
import {BuddyRepositoryService} from '../repositories/buddy-repository.service';
import {Observable, BehaviorSubject} from 'rxjs';
import {environment} from '../../environments/environment';
import {PublicFileSystem} from "./public-filesystem.service";
import {ChannelMeta, PrivateProfileInformation} from "./publicNode.service";
import {User} from "../entities/interfaces/user.interface";
import {FileSystemService, EntityType} from "./filesystem.service";
import {ChannelType} from "../entities/channel";
import {Semaphore} from 'await-semaphore';


export class ScanResult {

  public buddyHasAcceptedMyInvitation: boolean  = false;
  public channelMetaHasChanged: boolean = false;
  public profileInfoHasChanged: boolean = false;

  public allChannelMeta: ChannelMeta[];
  public changedChannelMeta: ChannelMeta[];

  public privateProfileInformation: PrivateProfileInformation;

  constructor(public buddy: Buddy) {}

  public hasSomethingChanged(): boolean {
    return  (!this.buddy.isAccepted && this.buddyHasAcceptedMyInvitation) ||
            this.channelMetaHasChanged ||
            this.profileInfoHasChanged;
  }
}

@Injectable()
export class NotificationService {
  private isInitialised: boolean = false;
  private notificationSubscribers: BehaviorSubject<ScanResult>;

  /**
   * A simple counter, that gets incremented every time our buddy-list has changed.
   * Is used to stop the polling of a deleted buddy.
   */
  private globalPollingId: number;

  private buddyRepoService: BuddyRepositoryService;
  private publicFileSystem: PublicFileSystem;
  private channelMetaCache: any = {};
  private pollingSemaphores: any = {};
  private pollingThreadIds: any = {};

  constructor(private fileSystem: FileSystemService) {
    // Initialising here would causes conflicts with dependency injection!

  }

  public getPollingSemaphore(userId: string): Semaphore {
    let result: Semaphore = null;

    result = this.pollingSemaphores[userId];

    if (! result) {
      result = new Semaphore(1);
      this.pollingSemaphores[userId] = result;
    }
    return result;
  }

  /**
   * Initialises the NotificationService
   * @param buddyRepoService
   * @param publicFileSystem
   */
  public initialise(buddyRepoService: BuddyRepositoryService, publicFileSystem: PublicFileSystem): void {
    if (this.isInitialised) {
      return;
    }
    this.buddyRepoService = buddyRepoService;
    this.publicFileSystem = publicFileSystem;

    this.notificationSubscribers = new BehaviorSubject<ScanResult>(null);

    this.fileSystem.load(EntityType.CHANNELMETAS).then(metas => {
        console.log("PreCached meta", metas);

        for(let meta in metas) {
            if(!metas.hasOwnProperty(meta)) {
              continue;
            }
            this.channelMetaCache[metas[meta].id] = metas[meta];
        }

    });

    buddyRepoService.findAll().subscribe(buddyList => {

      if (buddyList === null) {
        return;
      }

      // start polling for all buddies
      for (let buddy of buddyList) {
        if (buddy !== null) {
          this.StartPollingOfBuddy(buddy, false);
        }
      }
    });


    this.isInitialised = true;
  }

  public saveMetaToCache(meta: ChannelMeta): void {
    this.channelMetaCache[meta.id] = meta;
  }

  /**
   * Subscribe here for notification callbacks
   * Used to getPrivate a notifications whenever a buddy has made changes to his public-node
   * @returns {Observable<ScanResult>}
   */
  public getNotificationObservable(): Observable<ScanResult> {
    if (!this.isInitialised) {
      throw new Error("NotificationService needs to be manually initialised");
    }
    return this.notificationSubscribers.asObservable();
  }


  /**
   * Reads the passed publicFile Object and generates a ScanResult
   * @param buddy
   * @param publicFile
   * @returns {any}
   */
  private processPublicFile(buddy: Buddy, publicFile: any): ScanResult {

    try {
      let scanResult = new ScanResult(buddy);

      // Wen only care for invited buddies!
      if (! buddy.isChatOnlyBuddy) {

        let profileHasHasBeenInitialized = buddy.hasProfileHasBeenInitialized(); // executing order is important!
        let profileHasChanged = buddy.hasProfileHashChanged(publicFile.profileHash); // do nut run this before buddy.hasProfileHasBeenInitialized()

        if ( !profileHasHasBeenInitialized || profileHasChanged) {
          scanResult.profileInfoHasChanged = profileHasHasBeenInitialized;
          scanResult.privateProfileInformation = this.publicFileSystem.parsePrivateProfileInforamtions(publicFile);

          if (!buddy.isAccepted && scanResult.privateProfileInformation != null) {
            scanResult.buddyHasAcceptedMyInvitation = true;
          }
        }
      }

      // Unknown buddies, we only share the same chat with, get asked for changes in the channelMata.
      scanResult.allChannelMeta = this.publicFileSystem.parseChannelMetas(publicFile);
      scanResult.changedChannelMeta = this.filterAlreadySeenChannelMeta(scanResult.allChannelMeta);
      scanResult.channelMetaHasChanged = scanResult.changedChannelMeta.length > 0;

      return scanResult;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  /**
   * Get current thread id of running buddy poll.
   * incrementing this id will stop all ongoing threads of this buddy.
   * @param buddy
   * @returns {number}
   */
  private getPollingThreadIdOfBuddy( buddy: Buddy): number{
    let pollingThreadId: number = this.pollingThreadIds[buddy.id];
    if (! pollingThreadId) {
      pollingThreadId = 100;
    }
    this.pollingThreadIds[buddy.id] = pollingThreadId;
    return pollingThreadId;
  }


  /***
   * incrementing the counter => stop all ongoing polling threads of this buddy.
   * @param buddy
   */
  private increasePollingThreadIdOfBuddy(buddy: Buddy): number{
    let pollingThreadId: number = this.getPollingThreadIdOfBuddy(buddy);
    pollingThreadId++;
    this.pollingThreadIds[buddy.id] = pollingThreadId;
    return pollingThreadId;
  }

  /***
   * This will start a new polling session for delivered buddy.
   * Can be used by WebRTC start polling a buddy.
   *
   * Notification Service will recognize if the same buddy is
   * polled twice and stop the older thread.
   *
   * @param buddy
   * @param startPollingImidiatley
   * @constructor
   */
  public StartPollingOfBuddy(buddy: Buddy, startPollingImmediately: boolean): void {

    // Stop all ongoing polling threads of this buddy, by increasing the threadId.
    let newPollingThreadId: number = this.increasePollingThreadIdOfBuddy(buddy);

    // Start new polling thread.
    this.pollBuddyForNewContent(buddy, !startPollingImmediately, newPollingThreadId);
  }

  /**
   * Initialises the polling of a given buddy for new Posts
   *
   * This function is called for every buddy once, and then
   * calls itself recursively after a calculated timeout.
   *
   * When new content is found, the subscribers getPrivate informed
   *
   * @param buddy
   * @param randomizeTimeout
   * @param pollingId
   */
  private pollBuddyForNewContent(buddy: Buddy, randomizeTimeout: boolean, pollingThreadId: number): void {
    if (this.getPollingThreadIdOfBuddy(buddy) !== pollingThreadId) {
      // this buddy got deleted or a different thread has started polling
      // stop recursion
      return;
    }
    // calculate timeout
    let timeout: number = buddy.getPollingFrequency() + (randomizeTimeout ? Math.floor((Math.random() * 1000)) : 0);

    // wait the calculated timeout;
    setTimeout(() => {

      // to make sure we don't poll simultaneously with a thread from the WebRTC, we use a semaphore for thread safety
      let semaphore: Semaphore = this.getPollingSemaphore(buddy.id);

      semaphore.acquire() // this will freese the current thread
      // TODO check if this also freezes the frontend, if yes, make sure NotificationService is started in the background
        .then(release => {
           // console.log("[NotificationService}: polling buddy " + buddy.name);

            // now we read the publicNode
            this.publicFileSystem.readPublicFile(buddy.id).subscribe(
              publicFile => {
                try {
                  // poll the buddies box and check if it contains new content
                  let scanResult: ScanResult = this.processPublicFile(buddy, publicFile);

                  if (scanResult == null) {
                    // reading the publicNode failed!
                    console.error("[NotificationService}: polling of buddy url failed: " + buddy.id);

                    // punish buddy for error with greater polling timeout
                    buddy.setPollingFrequency(this.calculateChangeFrequence(buddy, false));

                  } else
                  // we were able to read the public node.

                  if (scanResult.buddyHasAcceptedMyInvitation) {
                    console.log("[NotificationService}: buddy " + buddy.name + " accepted my invitation!");
                  }

                  let buddyHasNewContent: boolean = scanResult.hasSomethingChanged();

                  //console.debug("[NotificationService}: scanResult for poll of " + buddy.name + ":", scanResult);

                  if (buddyHasNewContent) {
                   // console.log("Something has changed!", scanResult);
                    this.informAllSubscribersAboutChanges(scanResult);
                  }
                  // calculate and set timeout for the next poll of this buddy
                  buddy.setPollingFrequency(this.calculateChangeFrequence(buddy, buddyHasNewContent));

                } catch (err) {
                  console.error(err);
                }
                finally {

                  // release the semaphore, so the next thread can do its work safely
                  release();

                  // recursive call: poll buddy
                  this.pollBuddyForNewContent(buddy, false, pollingThreadId);
                }
              },
              error => {
                // reading buddie's publicFile failed
                console.log("[NotificationService] polling " + buddy.name+ " failed; Error: ", error);

                // recalculate timeout and keep polling
                buddy.setPollingFrequency(this.calculateChangeFrequence(buddy, false));

                // release the semaphore, so the next thread can do its work safely
                release();

                // keep polling. Error could have been just a network problem!
                //if(buddy.getPollingFrequency() !== environment.maximumBuddyPollingFrequencyInMs) {
                  this.pollBuddyForNewContent(buddy, false, pollingThreadId);
               // }
              });
        });
    }, timeout);
  }


  /**
   * Calculates a waiting period in milliseconds which is used for polling intervals per buddy.
   * @param buddy
   * @param uploadedNewContent
   * @returns {number} timeout in ms
   */
  private calculateChangeFrequence(buddy: Buddy, uploadedNewContent: boolean): number {
    const minimum = environment.minimumBuddyPollingFrequencyInMs;
    const maximum = environment.maximumBuddyPollingFrequencyInMs;
    const multiplier = environment.multiplicationRateOfExponentialPollingTimeout;

    let result: number = buddy.getPollingFrequency();
    // if the buddy has not uploaded new content, we are going to poll less frequent next time.
    result = (uploadedNewContent ? minimum : (result * multiplier));

    result = Math.max(Math.min(result, maximum), minimum);
    return result;
  }

  /**
   * Iterates over an array of ChannelMeta
   * Determines if each channelMeta has been updated since last polling attempt
   * @param buddy
   * @param allChannelMeta
   * @returns {ChannelMeta[]}
   */
  private filterAlreadySeenChannelMeta(allChannelMeta: ChannelMeta[]): ChannelMeta[] {
    let changedChannels: ChannelMeta[] = [];

    for (let polledResource of allChannelMeta) {
      let found = false;

      for (let meta in this.channelMetaCache) {
        if (!this.channelMetaCache.hasOwnProperty(meta)) {
          continue;
        }

        let cachedResource = this.channelMetaCache[meta];
        if (cachedResource.id === polledResource.id) {
          found = true;

          if (polledResource.head > cachedResource.head) {
            console.log("Diffrent Head", cachedResource, polledResource);
            cachedResource.head = polledResource.head;
            cachedResource.hashOfLastPost = polledResource.hashOfLastPost;
            changedChannels.push(polledResource);

          } else if (polledResource.head === cachedResource.head && polledResource.hashOfLastPost !== cachedResource.hashOfLastPost) {
            console.log("Diffrent Hash", cachedResource, polledResource);
            cachedResource.hashOfLastPost = polledResource.hashOfLastPost;
            changedChannels.push(polledResource);
          }

          if (polledResource.participants.length !== cachedResource.participants.length) {
            console.log("Participants changed");
            // Find new participants, poll them
            for (let user of polledResource.participants) {
              let found = cachedResource.participants.find(participant => {
                return participant.identifier === user.identifier;
              });
              if (!found) {
                let chatBuddy = new Buddy();
                chatBuddy.id = user.identifier;
                chatBuddy.publicKey = user.publicKey;
                chatBuddy.isChatOnlyBuddy = true;
                this.StartPollingOfBuddy(chatBuddy, false);
              }
            }
            cachedResource.participants = polledResource.participants;
          }

          break;
        }
      }
      if (!found) {
        this.channelMetaCache[polledResource.id] = polledResource;
        changedChannels.push(polledResource);
        console.log("Found for the first time", polledResource);
      }
    }
    return changedChannels;
  }


  /**
   * Informs all Subscribers about changes in buddie's public-node
   * @param scanResult
   */
  private informAllSubscribersAboutChanges(scanResult: ScanResult): void {
    this.notificationSubscribers.next(scanResult);
  }

}
