/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {NotificationService} from './notification.service';
import {BuddyRepositoryService} from '../repositories/buddy-repository.service';
import {DropboxAdapterService} from './dropbox-adapter.service';
import {ProfileRepositoryService} from "../repositories/profile-repository.service";
import {CryptoService} from "./crypto.service";
import {PublicFileSystem} from "./public-filesystem.service";
import {FileSystemService} from "./filesystem.service";
import {PrivateNodeService} from "./privateNode.service";
import {PublicNodeService} from "./publicNode.service";

describe('Service: Notification', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicNodeService
        , PrivateNodeService, FileSystemService, NotificationService, BuddyRepositoryService, DropboxAdapterService, ProfileRepositoryService, CryptoService, PublicFileSystem],
    });
  });

  it('should ...', inject([NotificationService], (service: NotificationService) => {
    expect(service).toBeTruthy();
  }));
});
