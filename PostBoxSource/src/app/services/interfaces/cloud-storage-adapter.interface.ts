export interface ICloudStorageAdapterInterface {

  userToken: string;

  login(): void;
  logout(): Promise<any>;
  writeFile(fileName: string, data: string): Promise<any>;
  deleteFile(fileName: string): Promise<any>;
  createSharedLink(path: string): Promise<any>;
  getSharedLink(path: string): Promise<string>;
  getFile(file: string): Promise<any>;
  isAuthenticated(): boolean;
}
