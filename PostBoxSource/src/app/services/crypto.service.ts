/// <reference path='../../../typings/globals/crypto-js/index.d.ts'/>
/// <reference path='../../../typings/globals/sjcl/index.d.ts'/>
import {Injectable} from '@angular/core';
//import {DropboxAdapterService} from "../services/dropbox-adapter.service";
import {ICloudStorageAdapterInterface} from "./interfaces/cloud-storage-adapter.interface";
import {environment} from "../../environments/environment";
import * as CryptoJS from 'crypto-js';
let sjcl = require('../../assets/js/sjcl');
let entropy = require('binary-shannon-entropy');
declare let LZMA: any;

/**
 * Additional documentation can be found on the GitLab Wiki (in German):
 * https://gitlab.com/eggerd_hda/pse/-/wikis/encryption
 *
 * Clarification of some used terms:
 * - Crypto-Tuple = an associative array used _ONLY_ inside the CryptoService to manage keys and the corresponding
 *                  cipher. See encrypt() for an example.
 * - Crypto-String = the stringified Crypto-Tuple. Crypto-Strings are the only return value (besides plaintext), that
 *                   will be returned by the CryptoService!
 * - Session-Key = a hashed random generated number that will be used as passphrase for symmetric encryption.
 */

@Injectable()
export class CryptoService {
  // The ECC curve to use for asymmetric encryption
  private readonly curve = sjcl.ecc.curves.c384;
  // 0 = off, low+fast 1..9 high+slow
  private readonly compressionLevel = 5;
  // only content that doesn't have a entropy higher than this will be compressed
  private readonly compressionEntropyThreshold = 5.4;

  // Will be used to symmetrically encrypt output of asymmetric encryption,
  // to "hide" the created array that contains some "plaintext" information.
  // Have a look at the wiki entry for "encryptAsym" to see an example output
  private readonly serializationPassphrase = 'd3dd3237efd1228c300427bd55091b8b137b66a0';

  private asymKeys: ICryptoKeys = null; // have a look at the description of the ICryptoKeys interface
  private storageAdapter: ICloudStorageAdapterInterface;

  constructor() {
    // nothing to do
  }

  /**
   * This function has to be called BEFORE any en-/decryption related function can be called!
   *
   * If this function is called for the first time ever (aka. first-times-setup for a new user),
   * to pairs of asymmetric keys will be generated randomly (one for encryption and the other for
   * signing). These key pairs will then be stringified, symmetrically encrypted with the users
   * passphrase and lastly saved to the online storage.
   *
   * If this function is called after a first-time-setup has already been completed, the saved
   * keys will be loaded from the online storage. Only if the user provides the same passphrase
   * as during the first-time-setup, the keys can be decrypted and then be parsed.
   *
   * @param passphrase: string = is used to symmetrically encrypt the asymmetric-keys after serializing them
   * @returns {Promise<boolean>} = will be TRUE, if the passphrase was correct or FALSE, if it wasn't
   */
  init(passphrase: string, storageAdapter: ICloudStorageAdapterInterface): Promise<boolean> {
    return new Promise((resolve, reject) => {
      console.log('[crypto] initializing');

      this.storageAdapter = storageAdapter;

      try {
        this.requireInit();
        console.log('[crypto] has already been initialized!');
        resolve(true);
      } catch (error) {
        this.storageAdapter.getFile(this.filenameHash('asymmetric-keys')).then((result) => {
          console.log('[crypto] asymmetric-keys loaded from storage');
          console.log('[crypto] decrypting asymmetric-keys with users passphrase');

          try {
            let keyString = this.decryptSym(result, passphrase);

            console.log('[crypto] parsing asymmetric-keys');
            this.asymKeys = this.parseKeys(keyString);

            console.log('[crypto] initialization finished');
            resolve(true);
          } catch (error) {
            console.log('[crypto] incorrect passphrase!');
            resolve(false);
          }
        }).catch(() => {
          console.log('[crypto] generating asymmetric-keys');
          let tmpKeys: ICryptoKeys = {
            'encryption': sjcl.ecc.elGamal.generateKeys(this.curve),
            'signing': sjcl.ecc.ecdsa.generateKeys(this.curve)
          };

          console.log('[crypto] saving asymmectic-keys to storage');
          this.storageAdapter.writeFile(this.filenameHash('asymmetric-keys'), this.stringifyKeys(tmpKeys, passphrase)).then(() => {
            this.asymKeys = tmpKeys;

            console.log('[crypto] initialization finished');
            resolve(true);
          }).catch((error) => {
            reject(new Error('Failed to save asymmetric-keys to storage: ' + error));
          });
        });
      }
    });
  }

  /**
   * Encrypts the passed content symmetric with a random generated key (called "session-key").
   * The session-key will then be encrypted asymmetric with our own encryption-public-key, so
   * that we can decrypt the session-key again. The asymmetric encrypted session-key is then
   * stored in the header, under our own unique identifier.
   * In case the encrypted content should also be available to buddies, an array with CryptoBuddy's
   * can be passed (optional). All buddies you pass within this array will be added to the header, too.
   *
   * A signature for the encrypted content will always be created and appended to the crypto-tuple.
   *
   * @param content: string = stuff that you want to encrypt
   * @param buddies: Array<CryptoBuddy> = a list of buddies that you want to have access, too
   * @returns {string} = the crypto-string
   */
  encrypt(content: string, buddies?: Array<CryptoBuddy>): string {
    if (this.requireInit()) {
      if (!this.validateTupleString(content)) {
        // console.log('[crypto] encrypting content: ' + content);

        let uniqueIdentifier = this.getUniqueIdentifier(),
          randomNumberSeed = Math.random() * (9000000 - 1000000) + 1000000,
          sessionKey = this.hash(randomNumberSeed.toString() + Date.now()),

          /**
           * Crypto-Tuple:
           * The following structure is referenced as a crypto-tuple. The tuple contains three main entries,
           * which would be "header", "cipher" and "signature". The "header"-entry contains a associative list of
           * people, that will be able to decrypt the cipher. The "cipher"-entry contains the cipher.. surprise.
           * The "signature"-entry represents a hash of the content in plaintext, that was signed with our
           * signature-private-key.
           *
           * The header-entry contains at least one entry; for the person that encrypted the content.
           * Each header-entry has the following structure:
           *  ["unique_user_identifier"] = encrypt_asymmetric("symmetric_key_for_cipher", "public_key_for_this_person");
           *
           * Example for a crypto-tuple:
           * Array {
           *    ["header"] = Array {
           *        ["unique_identifier_of_owner"] = "XMXnmo30veOr9pe+yEBnVF0XhWagv6MMwadirfCpT3AL43Pbovi/CMBbIP...",
           *        ["unique_identifier_of_buddy1"] = "cWP56em04RZHSKakEJBlxRu3y0F/WTyWjhVWs/6Ftpe3SV9RiaTKa8hbEl...",
           *        ["unique_identifier_of_buddy2"] = "a2G8kIhNJ2xqJhnCERovrvA0kPUtYk0dPP2CIbvDkJesJQbYRBJbI/SntS...",
           *        [... and so on ...] = "... and so on ...]
           *    },
           *    ["cipher"] = "U2FsdGVkX1/CUqBzZVF9o3G0pPQ75weDAzlmmNkVPQJA8hbElexE/1ksufUl3yow...",
           *    ["signature"] = "I+JlfgjOXMQLBSFmAdG7d5tBVcyA43c/bWNtP8XY2LIkf9/DGS4u8VGFcO3xcXMQl1uva..."
           * }
           *
           * This crypto-tuple will be stringified and returned (= crypto-string). To decrypt the content, the
           * whole crypto-string has to be passed to the decrypt function. There it would be parsed back into an
           * crypto-tuple, our header-entry would be read and then asymmetric decrypted with our encryption-private-key
           * in order to getPrivate the session-key for the cipher. And this works exactly the same for buddies that
           * want to decrypt the content and have been added to the header.
           */
          tuple: ICryptoTuple = {'header': {}, 'cipher': null, 'signature': this.createSignature(content)};

        tuple.header[uniqueIdentifier] = this.encryptAsym(sessionKey);

        if (Array.isArray(buddies)) {
          for (let buddy of buddies) {
            tuple.header[this.hash(buddy.publicKey)] = this.encryptAsym(sessionKey, buddy.publicKey);
          }
        }

        tuple.cipher = this.encryptSym(this.compress(content), sessionKey);

        return this.stringifyTuple(tuple);
      } else {
        console.log("Error with", content);
        throw new Error('The passed content seems to be already encrypted!');
      }
    }
  }

  /**
   * Expects a crypto-string (= return value of the encrypt-function), that will be parsed back into
   * an crypto-tuple (associative array). After that, our unique identifier will be used to read our
   * entry in the header. The header entry will then be decrypted (asym) with our encryption-private-key
   * in order to getPrivate the session-key for the cipher contained in the crypto-tuple. After decrypting
   * the cipher with the session-key, the signature will be verified, if not explicitly prevented by
   * passing FALSE as second parameter. Only if the signature can be verified, the plaintext result
   * will be returned.
   *
   * @param cryptoString: string = the crypto-string
   * @param signaturePublicKey: string | boolean = (optional) Signature-Public-Key to verify signature with. If no key
   *                                                is provided, our own key will be used by default. If FALSE is passed
   *                                                the signature will not be verified at all!
   * @returns {string} = plaintext of the encrypted content
   */
  decrypt(cryptoString: string, signaturePublicKey?: string | boolean): string {
    if (this.requireInit()) {
      let tuple = this.parseTuple(cryptoString),
        tupleEntry = this.getTupleEntry(tuple),
        sessionKey = this.decryptAsym(tupleEntry);

      let result = this.decompress(this.decryptSym(tuple.cipher, sessionKey));
      // console.log("[crypto] decrypted content: " + result);

      if (signaturePublicKey !== false) { // only if the verification has not been prevented
        let signatureKey: sjcl.SjclEcdsaPublicKey;

        if (typeof signaturePublicKey === 'string') {
          signatureKey = this.parseSignaturePublicKey(<string> signaturePublicKey);
        } else {
          signatureKey = this.asymKeys.signing.pub;
        }

        if (this.verifySignatureHash(tuple.signature, result, signatureKey) === false) {
          throw new Error('The crypto-string has been corrupted!');
        }
      }

      return result;
    }
  }

  /**
   * If you want one or more buddy to be able to decrypt content, that you already encrypted, this is the
   * function to go. You can pass a list of CryptoBuddy that you want to be able to decrypt the content.
   * This buddies will then be added to the header of the crypto-tuple.
   *
   * @param cryptoString: string = the crypto-string
   * @param buddies: Array<CryptoBuddy> = the list of CryptoBuddy you want to be able to decrypt the content
   * @returns {string} = the updated crypto-string
   */
  addBuddy(cryptoString: string, buddies: Array<CryptoBuddy>): string {
    if (this.requireInit()) {
      let tuple = this.parseTuple(cryptoString),
        tupleEntry = this.getTupleEntry(tuple),
        sessionKey = this.decryptAsym(tupleEntry);

      for (let buddy of buddies) {
        if (!tuple.header[buddy.identifier]) {
          tuple.header[this.hash(buddy.publicKey)] = this.encryptAsym(sessionKey, buddy.publicKey);
        }
      }

      return this.stringifyTuple(tuple);
    }
  }

  /**
   * If one or more buddies should no longer be able to decrypt the content, they have to be removed from
   * the header in the crypto-tuple, the content has to be re-encrypted and the header to be updated for
   * all remaining entries.
   *
   * The list of buddies you want to remove is passed as an Array (second parameter). This array simply
   * contains the unique identifier of the corresponding buddies. Example:
   *    Array {
   *      "unique_identifier_buddy1",
   *      "unique_identifier_buddy1234"
   *    }
   *
   * @param cryptoString: string = the crypto-string
   * @param buddies: Array<string> = a list containing the unique identifier of all buddies that you want to remove
   * @returns {Promise<string>} = will return the updated crypto-string, on resolve()
   */
  removeBuddy(cryptoString: string, buddies: Array<CryptoBuddy>): Promise<string> {
    return new Promise((resolve, reject) => {
      if (this.requireInit()) {
        let uniqueIdentifier = this.getUniqueIdentifier(),
          tuple = this.parseTuple(cryptoString),
          remainingIdentifiers: Array<string> = Array();

        // for every buddy saved in the header
        for (let identifier in tuple.header) {
          if (tuple.header.hasOwnProperty(identifier)) {
            if (identifier !== uniqueIdentifier) {
              let shouldBeRemoved = false;

              for (let toBeRemoved of buddies) {
                if (this.hash(toBeRemoved.publicKey) === identifier) {
                  shouldBeRemoved = true;
                }
              }

              if (shouldBeRemoved === false) { // if buddy isn't contained in remove-list
                remainingIdentifiers.push(identifier);
              }
            }
          }
        }

        this.fetchBuddyPublicKey(remainingIdentifiers).then((remainingBuddies) => {
          let content = this.decrypt(cryptoString);

          if (remainingBuddies.length > 0) {
            resolve(this.encrypt(content, remainingBuddies));
          } else {
            resolve(this.encrypt(content));
          }
        }).catch((error) => {
          reject(error);
        });
      }
    });
  }

  /**
   * Updates / replaces the old content with the new content. The new content will be
   * encrypted with the same session-key as the old content. Therefor, only the cipher and
   * the signature have to be replaced.
   *
   * @param cryptoString: string = the crypto-string containing the old content
   * @param updatedContent: string = the new content that will be encrypted and replace the old one
   * @returns {string} = the updated crypto-string
   */
  updateContent(cryptoString: string, updatedContent: string): string {
    if (this.requireInit()) {
      let tuple = this.parseTuple(cryptoString),
        tupleEntry = this.getTupleEntry(tuple),
        sessionKey = this.decryptAsym(tupleEntry);

      tuple.cipher = this.encryptSym(this.compress(updatedContent), sessionKey);
      tuple.signature = this.createSignature(updatedContent);

      return this.stringifyTuple(tuple);
    }
  }

  /**
   * Creates a SHA-256 hash for the passed content. This hash is then signed with our
   * signature-private-key and represents the signature for the passed content.
   *
   * @param content: string = the content that should be signed
   * @returns {string} = the signature for the passed content
   */
  createSignature(content: string): string {
    if (this.requireInit()) {
      try {
        let hash = sjcl.hash.sha256.hash(content),
          signature = this.asymKeys.signing.sec.sign(hash, 0, false);

        return sjcl.codec.base64.fromBits(signature);
      } catch (error) {
        throw new Error('Unexpected error while creating signature: ' + error);
      }
    }
  }

  /**
   * Only calls verifySignatureHash(), after the passed signature-public-key has been parsed.
   * Have a look at the description of verifySignatureHash().
   *
   * @param signature: string = the signature that was returned by createSignature()
   * @param plaintextContent = the content the signature was allegedly created for; in plaintext!
   * @param signaturePublicKey = the signature-public-key of the person that created the signature
   * @return {boolean} = will be TRUE if the signature is valid, otherwise it will be FALSE
   */
  verifySignature(signature: string, plaintextContent: string, signaturePublicKey: string): boolean {
    if (this.requireInit()) {
      return this.verifySignatureHash(signature, plaintextContent, this.parseSignaturePublicKey(signaturePublicKey));
    }
  }

  /**
   * Simply hashes (SHA-256) your content. If desired a salt can be passed that will be appended
   * to your content, before the hash is created.
   *
   * @param content: string = the content that you want to hash
   * @param salt: string = will be appended to your content
   * @returns {string} = the hash of your content
   */
  hash(content: string, salt = ''): string {
    try {
      return CryptoJS.SHA256(salt + content).toString();
    } catch (error) {
      throw new Error('Unexpected error during hashing: ' + error);
    }
  }

  /**
   * Simply hashes your content using SHA-256 and returns the first 16 characters. It should only
   * be used to hash filenames, before creating / reading a file from the online storage!
   * Therefor no salt-option is provided.
   *
   * @param filename: string = the filename that you need to be hashed
   * @return {string} = the first 16 characters of a SHA-256 hash
   */
  filenameHash(filename: string): string {
    try {
      return CryptoJS.SHA256(filename).toString().substr(0, 16);
    } catch (error) {
      throw new Error('Unexpected error while hashing filename: ' + error);
    }
  }

  /**
   * Returns the own encryption-public-key. Is mainly used during the first-time-setup, by the profile-repository,
   * to be able to save the encryption-public-key into the public-node.
   *
   * @returns {string}
   */
  getPublicKey(): string {
    if (this.requireInit()) {
      return this.stringifyPublicKey(this.asymKeys.encryption.pub);
    }
  }

  /**
   * Returns the own signature-public-key. Is mainly used during the first-time-setup, by the profile-repository,
   * to be able to save the signature-public-key into the public-node.
   *
   * @returns {string}
   */
  getSignaturePublicKey(): string {
    if (this.requireInit()) {
      return this.stringifyPublicKey(this.asymKeys.signing.pub);
    }
  }

  /**
   * Can be used to check if the service has already been initialized.
   *
   * @returns {boolean}
   */
  public isInitialized(): boolean {
    return this.asymKeys !== null;
  }

  // +++++ +++++ Helper Functions +++++ +++++

  /**
   * Serializes the passed asymmetric-keys and encrypts them symmetrically with the passed
   * passphrase (that was provided by the user), so that the keys can be saved to the online storage.
   *
   * @param keys: ICryptoKeys = the asymmetric-keys
   * @param passphrase: string = the users passphrase
   * @return {string} = the symmetrically encrypted keys
   */
  private stringifyKeys(keys: ICryptoKeys, passphrase: string): string {
    let obj = {
      'encryption': {
        'private': this.stringifyPrivateKey(keys.encryption.sec),
        'public': this.stringifyPublicKey(keys.encryption.pub)
      },
      'signing': {
        'private': this.stringifyPrivateKey(keys.signing.sec),
        'public': this.stringifyPublicKey(keys.signing.pub)
      }
    };

    return this.encryptSym(JSON.stringify(obj), passphrase);
  }

  /**
   * Stringifies a public-key.
   *
   * @param key: SjclECCPublicKey = the public-key (encryption or signing)
   * @returns {string} = the stringified key
   */
  private stringifyPublicKey(key: sjcl.SjclECCPublicKey): string {
    try {
      return sjcl.codec.base64.fromBits(key.get().x) + sjcl.codec.base64.fromBits(key.get().y);
    } catch (error) {
      throw new Error('Failed to stringify public-key: ' + error);
    }
  }

  /**
   * Stringifies a private-key
   *
   * @param key: SjclECCSecretKey = the private-key (encryption or signing)
   * @returns {string} = the stringified key
   */
  private stringifyPrivateKey(key: sjcl.SjclECCSecretKey): string {
    try {
      return sjcl.codec.base64.fromBits(key.get());
    } catch (error) {
      throw new Error('Failed to stringify private-key: ' + error);
    }
  }

  /**
   * Parses the stringified asymmetric-keys back into encryption- & signing-keys, so that they can be used again.
   *
   * @param keyString: string = the stringified keys; have to be already decrypted!
   * @returns {ICryptoKeys} = the object containing the asymmetric-keys (encryption & signature)
   */
  private parseKeys(keyString: string): ICryptoKeys {
    let keyData;

    try {
      keyData = JSON.parse(keyString);
    } catch (error) {
      throw new Error('Failed to parse asymmetric-key data that was loaded from the storage!');
    }

    if (keyData.hasOwnProperty("encryption") && keyData.hasOwnProperty("signing")) {
      return {
        'encryption': {
          'sec': this.parseEncryptionPrivateKey(keyData.encryption.private),
          'pub': this.parseEncryptionPublicKey(keyData.encryption.public)
        },
        'signing': {
          'sec': this.parseSignaturePrivateKey(keyData.signing.private),
          'pub': this.parseSignaturePublicKey(keyData.signing.public)
        }
      };
    } else {
      throw new Error('The asymmetric-key data loaded from the storage has a invalid structure!');
    }
  }

  /**
   * Parses the stringified encryption-public-key back into a key-object that can be used for encryption.
   *
   * @param keyString: string = the stringified encryption-public-key
   * @return {SjclElGamalPublicKey} = the key-object
   */
  private parseEncryptionPublicKey(keyString: string): sjcl.SjclElGamalPublicKey {
    try {
      return new sjcl.ecc.elGamal.publicKey(this.curve, sjcl.codec.base64.toBits(keyString));
    } catch (error) {
      throw new Error('Failed to parse encryption-public-key: ' + error);
    }
  }

  /**
   * Parses the stringified encryption-private-key back into a key-object that can be used for decryption.
   *
   * @param keyString: string = the stringified encryption-private-key
   * @return {SjclElGamalSecretKey} = the key-object
   */
  private parseEncryptionPrivateKey(keyString: string): sjcl.SjclElGamalSecretKey {
    try {
      return new sjcl.ecc.elGamal.secretKey(
        this.curve,
        this.curve.field.fromBits(sjcl.codec.base64.toBits(keyString))
      );
    } catch (error) {
      throw new Error('Failed to parse encryption-private-key: ' + error);
    }
  }

  /**
   * Parses the stringified signature-public-key back into a key-object that can be used to verify signatures.
   *
   * @param keyString: string = the stringified signature-public-key
   * @return {SjclEcdsaPublicKey} = the key-object
   */
  private parseSignaturePublicKey(keyString: string): sjcl.SjclEcdsaPublicKey {
    try {
      return new sjcl.ecc.ecdsa.publicKey(this.curve, sjcl.codec.base64.toBits(keyString));
    } catch (error) {
      throw new Error('Failed to parse signature-public-key: ' + error);
    }
  }

  /**
   * Parses the stringified signature-private-key back into a key-object that can be used to create signatures.
   *
   * @param keyString: string = the stringified signature-private-key
   * @return {SjclEcdsaSecretKey} = the key-object
   */
  private parseSignaturePrivateKey(keyString: string): sjcl.SjclEcdsaSecretKey {
    try {
      return new sjcl.ecc.ecdsa.secretKey(
        this.curve,
        this.curve.field.fromBits(sjcl.codec.base64.toBits(keyString))
      );
    } catch (error) {
      throw new Error('Failed to parse signature-private-key: ' + error);
    }
  }

  /**
   * Fetch our own unique identifier from somewhere..
   *
   * @returns {string} = our own unique identifier
   */
  private getUniqueIdentifier(): string {
    return this.hash(this.getPublicKey());
  }

  /**
   * Parses the crypto-string back into a crypto-tuple (associative array).
   *
   * @param cryptoString: string = the crypto-string
   * @returns {ICryptoTuple} = the associative array representing our crypto-tuple
   */
  private parseTuple(cryptoString: string): ICryptoTuple {
    if (this.validateTupleString(cryptoString)) {
      try {
        let tupleArray = JSON.parse(cryptoString),
          tuple: ICryptoTuple = {'header': {}, 'cipher': null, 'signature': null};

        tuple.header = tupleArray[0];
        tuple.cipher = tupleArray[1];
        tuple.signature = tupleArray[2];

        return tuple;
      } catch (error) {
        throw new Error('Failed to parse crypto-string: ' + error);
      }
    } else {
      throw new Error('Invalid crypto-string has been passed!' + cryptoString);
    }
  }

  /**
   * Validates the crypto-string, that gets passed to various functions, to ensure
   * that it has the correct structure or even is a crypto-string at all (in case the input had
   * never been encrypted in the first place).
   *
   * @param cryptoString: string = the crypto-string
   * @returns {boolean} = TRUE if it's a valid crypto-string, otherwise FALSE
   */
  private validateTupleString(cryptoString: string): boolean {
    try {
      let obj = JSON.parse(cryptoString);
      return obj.length === 3 && typeof obj[0] === 'object' && typeof obj[1] === 'string' && typeof obj[2] === 'string';
    } catch (e) {
      return false;
    }
  }

  /**
   * Converts the passed crypto-tuple into a simple numeric array, so that "header" and "cipher"
   * aren't visible in files (was requested), and then stringifies the tuple using JSON.
   *
   * @param tuple: ICryptoTuple = the crypto-tuple
   * @returns {string} = the crypto-string
   */
  private stringifyTuple(tuple: ICryptoTuple): string {
    try {
      let tupleArray = [tuple.header, tuple.cipher, tuple.signature];
      return JSON.stringify(tupleArray);
    } catch (error) {
      throw new Error('Failed to stringify crypto-tuple: ' + error);
    }
  };

  /**
   * Tries to find and read a specific entry in the header of a crypto-tuple.
   * If the entry isn't found an error will be thrown, because the file can't
   * be decrypted without the corresponding header entry!
   *
   * @param tuple: ICryptoTuple = the crypto-tuple
   * @param uniqueIdentifier: string = (optional) the unique identifier for the entry to read
   * @returns {string} = the content of the header entry (= asymmetric encrypted session-key)
   */
  private getTupleEntry(tuple: ICryptoTuple, uniqueIdentifier?: string): string {
    if (uniqueIdentifier) {
      if (tuple.header[uniqueIdentifier]) {
        return tuple.header[uniqueIdentifier];
      } else {
        throw new Error('You are missing from the encryption-header!');
      }
    } else {
      let uniqueIdentifier = this.getUniqueIdentifier();
      if (tuple.header[uniqueIdentifier]) {
        return tuple.header[uniqueIdentifier];
      } else {
        throw new Error('You are missing from the encryption-header!');
      }
    }
  }

  /**
   * This function is used by removeBuddy() in order to fetch the public key of all buddies
   * that shall remain in the header of the crypto-tuple. This is required to re-build the
   * header by encrypting the newly generated session-key for each remaining buddy. Therefor
   * their encryption-public-key is required.
   *
   * @param identifier: string = unique identifier of the buddy whose encryption-public-key is required
   * @returns {Promise<string>} = the public key of this buddy
   */
  private fetchBuddyPublicKey(identifiers: Array<string>): Promise< Array<CryptoBuddy> > {
    return new Promise((resolve, reject) => {
      let results: Array<CryptoBuddy> = Array();

      if (identifiers.length > 0) {
        this.storageAdapter.getFile(environment.PRIVATE_NODE).then((file) => {
          let decryptedFile = this.decrypt(file),
            parsedFile: any,
            buddies: Array<any> = Array();

          try {
            parsedFile = JSON.parse(decryptedFile);
          } catch (error) {
            reject('Failed to parse private-node');
          }

          for (let buddy of parsedFile.buddies) {
            try {
              buddies.push(JSON.parse(buddy));
            } catch (error) {
              reject(new Error('Failed to parse buddy!'));
            }
          }

          for (let requested of identifiers) {
            for (let buddy of buddies) {
              if (this.hash(buddy.publicKey) === requested) {
                results.push(new CryptoBuddy(buddy.id, buddy.publicKey));
              }
            }
          }

          resolve(results);
        }).catch((error) => {
          reject(new Error('Failed to load private-node from storage: ' + error));
        });
      } else {
        resolve(results);
      }
    });
  }

  /**
   * Encrypt content asymmetric. If no encryption-public-key is passed, our own
   * encryption-public-key will be used. The result of the asymmetric encryption will be
   * encrypted symmetrically with the hardcoded "serializationPassphrase". This is only to
   * "hide" the plaintext information that are contained in the result of asymmetric encryption.
   *
   * @param content: string = the content to encrypt
   * @param publicKey: string = (optional) to public key to use
   * @returns {string} = the cipher for the encrypted content
   */
  private encryptAsym(content: string, publicKey?: string): string {
    let key: sjcl.SjclECCPublicKey;

    if (!publicKey) {
      key = this.asymKeys.encryption.pub;
    } else {
      key = this.parseEncryptionPublicKey(publicKey);
    }

    try {
      return this.encryptSym(sjcl.encrypt(key, content), this.serializationPassphrase);
    } catch (error) {
      throw new Error('Unexpected error during asymmetric encryption: ' + error);
    }
  }

  /**
   * Decrypt content asymmetric. Because asymmetrically encrypted content is also symmetrically
   * encrypted (have a look at encryptAsym() for a hint), the passed cipher is decrypted
   * symmetrically first, before it can be decrypted asymmetrically.
   *
   * @param cipher: string = the cipher of the encrypted content
   * @returns {string} = the plaintext
   */
  private decryptAsym(cipher: string): string {
    try {
      return sjcl.decrypt(this.asymKeys.encryption.sec, this.decryptSym(cipher, this.serializationPassphrase));
    } catch (error) {
      throw new Error('Unexpected error during asymmetric decryption: ' + error);
    }
  }

  /**
   * Verifies if the passed content matches the content the passed signature has been
   * created for. The signature-public-key of the user, that created the signature, has
   * to be passed.
   *
   * @param signedHash: string = the signature for the content
   * @param content: string = the content the signature was allegedly created for; in plaintext!
   * @param key = the signature-public-key to verify the signature with
   * @returns [boolean} = will be TRUE if the signature is valid, otherwise FALSE
   */
  private verifySignatureHash(signedHash: string, content: string, key: sjcl.SjclEcdsaPublicKey): boolean {
    let hash: sjcl.BitArray,
      signature: sjcl.BitArray;

    try {
      hash = sjcl.hash.sha256.hash(content);
      signature = sjcl.codec.base64.toBits(signedHash);
    } catch (error) {
      throw new Error('Unexpected error while preparing the signature for verification: ' + error);
    }

    try {
      key.verify(hash, signature, false);
      return true;
    } catch (error) { // CORRUPT: signature didn't check out
      if (error.message.match(/signature didn't check out/) !== null) {
        return false;
      } else {
        throw new Error('Unexpected error while verifying the signature: ' + error);
      }
    }
  }

  /**
   * Encrypt content symmetric with the given passphrase (aka session-key).
   *
   * @param content: string = the content to encrypt
   * @param passphrase: string = the session-key
   * @returns {string} = the cipher
   */
  private encryptSym(content: string, passphrase: string): string {
    try {
      return CryptoJS.AES.encrypt(content, passphrase).toString();
    } catch (error) {
      throw new Error('Unexpected error during symmetric encryption: ' + error);
    }
  }

  /**
   * Decrypt content symmetric with the given passphrase (aka session-key).
   *
   * @param cipher: string = the cipher of the encrypted content
   * @param passphrase: string = the session-key
   * @returns {string} = the plaintext
   */
  private decryptSym(cipher: string, passphrase: string): string {
    let plaintext;

    try {
      plaintext = CryptoJS.AES.decrypt(cipher, passphrase).toString(CryptoJS.enc.Utf8);
    } catch (error) {
      throw new Error('Unexpected error during symmetric decryption: ' + error);
    }

    if (plaintext.length > 0) {
      return plaintext;
    } else {
      throw new Error('Incorrect passphrase used for symmetric decryption!');
    }
  }

  /**
   * Compresses the passed content, if compression is enabled and the entropy of the passed
   * content doesn't exceed the configured threshold. Nonetheless, a stringyfied array will
   * be returned. This array contains a flag (index 0) that shows if the content (index 1)
   * is compressed or not - so that the decompression function knows what it has to do.
   *
   * @param content: string = the content that should be compressed
   * @returns {string} = might be the compressed content, but could also be the uncompressed content again
   */
  private compress(content: string): string {
    let container = Array(); // [0] => boolean: content compressed? / [1] => string: content

    if (this.compressionLevel > 0 && this.compressionLevel < 10) { // only if compression is enabled
      let contentEntropy: number;

      try {
        contentEntropy = entropy(Buffer.from(content)); // used to decide if content should be compressed or not
      } catch (error) {
        throw new Error('Unexpected error while calculating entropy: ' + error);
      }

      if (contentEntropy <= this.compressionEntropyThreshold) { // entropy of content has to be below the threshold!
        try {
          container[0] = true;
          container[1] = new Buffer(LZMA.compress(content, this.compressionLevel)).toString('binary');
        } catch (error) {
          throw new Error('Unexpected error while compressing: ' + error);
        }
      } else {
        container[0] = false;
        container[1] = content;
      }
    } else {
      container[0] = false;
      container[1] = content;
    }

    return JSON.stringify(container);
  }

  /**
   * Decompresses the passed content, even if compression has been disabled. This was, compression
   * can be disabled in the middle of a production environment, without breaking anything. Checks
   * if content has really been compressed (via the flag) and then decompresses it, otherwise it
   * will simply return the already uncompressed content.
   *
   * @param content: string = the return value that was generated by compressed()
   * @returns {string} = the uncompressed content
   */
  private decompress(content: string): string {
    let container = Array();

    try {
      container = JSON.parse(content);
    } catch (error) {
      throw new Error('Failed to parse compressed content back into array: ' + error);
    }

    if (container.hasOwnProperty(0) && container.hasOwnProperty(1)) {
      if (container[0] === true) { // only if it got actually compressed, otherwise just return the content
        try {
          return LZMA.decompress(Buffer.from(container[1], 'binary'));
        } catch (error) {
          throw new Error('Unexpected error while decompressing: ' + error);
        }
      } else {
        return container[1];
      }
    } else {
      throw new Error('Decompression failed, because the content seems to be malformed!');
    }
  }

  /**
   * Simply checks if the init()-function has already been called in order to be able to use
   * most of the other functions of this service.
   *
   * @returns {boolean} = TRUE if the service has been initialized, otherwise a error is thrown
   */
  private requireInit(): boolean {
    if (this.asymKeys !== null) {
      return true;
    } else {
      throw new Error('CryptoService has to be initialized first via init()!');
    }
  }
}

// +++++ +++++ Interfaces / Helper-Classes +++++ +++++

interface ICryptoTuple {
  header: {}; // key: identifier => value: encrypted file-key
  cipher: string;
  signature: string;
}
/**
 * We are using two different pairs of asymmetric keys! One pair is only for encryption and the
 * other is only for signing! This comes due to the fact that we are using two different
 * algorithms for those two tasks. Encryption is done with ECDH where as signing is done by
 * ECDSA, therefore we need different keys.
 */
interface ICryptoKeys {
  encryption: sjcl.SjclKeyPair<sjcl.SjclElGamalPublicKey, sjcl.SjclElGamalSecretKey>;
  signing: sjcl.SjclKeyPair<sjcl.SjclEcdsaPublicKey, sjcl.SjclEcdsaSecretKey>;
}

/**
 * Can be used when constructing a list of buddies you want to add [encrypt() / showAddBuddyDialog()] or
 * remove [removeBuddy()].
 */
export class CryptoBuddy {
  public identifier: string;
  public publicKey: string;

  /**
   * @param identifier: string = the unique identifier of this buddy
   * @param publicKey: string = the public key of this buddy
   */
  constructor(identifier: string, publicKey: string) {
    this.identifier = identifier;
    this.publicKey = publicKey;
  }
}
