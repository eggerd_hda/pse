import {Injectable}       from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import {DropboxAdapterService} from './dropbox-adapter.service';
import {CryptoService} from './crypto.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private authService: DropboxAdapterService, private router: Router, private cryptoService: CryptoService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkLogin();
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    isLoggedIn(): boolean {
        return this.authService.isAuthenticated() && this.cryptoService.isInitialized();
    }

    checkLogin(): boolean {
        if (this.isLoggedIn()) {
            return true;
        }

        // TODO: Check if registered
        this.router.navigate(['/login']);
        return false;
    }
}
