/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {BuddyRepositoryService} from '../repositories/buddy-repository.service';
import {DropboxAdapterService} from './dropbox-adapter.service';
import {ProfileRepositoryService} from "../repositories/profile-repository.service";
import {CryptoService} from "./crypto.service";
import {PrivateNodeService} from "./privateNode.service";

describe('Service: PrivateNode', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuddyRepositoryService, PrivateNodeService, DropboxAdapterService, ProfileRepositoryService, CryptoService],
    });
  });

  it('should ...', inject([PrivateNodeService], (service: PrivateNodeService) => {
    expect(service).toBeTruthy();
  }));
});
