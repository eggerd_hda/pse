import {Injectable} from "@angular/core";
import {Entity} from "../entities/interfaces/entity.interface";
import {PrivateNodeService} from "./privateNode.service";
import {PublicNodeService, ChannelMeta} from "./publicNode.service";
import {Profile} from "../entities/profile/profile";
import {Buddy} from "../entities/buddy";
import {ICloudStorageAdapterInterface} from "./interfaces/cloud-storage-adapter.interface";
import {Channel} from "../entities/channel";
import {BuddyRepositoryService} from "../repositories/buddy-repository.service";
import {environment} from "../../environments/environment";

export enum EntityType {
  PROFILE,
  BUDDY,
  CHANNEL,
  CHANNELMETA,
  BUDDIES,
  CHANNELMETAS
}

@Injectable()
export class FileSystemService {

  constructor(private privateNode: PrivateNodeService, private publicNode: PublicNodeService) {
  }

  public initalize(storageAdapter: ICloudStorageAdapterInterface, buddyRepo: BuddyRepositoryService): Promise<any> {
      return new Promise((resolve, reject) => {
        this.privateNode.initialize(storageAdapter);
        this.publicNode.initialize(storageAdapter, buddyRepo, true)
          .then(() => { resolve(); })
          .catch(err => { reject(err); });
      });
  }

  public initializeFromStorage(storageAdapter: ICloudStorageAdapterInterface, buddyRepo: BuddyRepositoryService): Promise<any> {
      return new Promise((resolve, reject) => {
        this.privateNode.initialize(storageAdapter);
        this.publicNode.initialize(storageAdapter, buddyRepo, false).then(() => {
          let l1 = this.privateNode.loadFromStorage();
          let l2 = this.publicNode.loadFromStorage();
          Promise.all([l1, l2]).then(() => {
            console.log("[FILESYSTEM] InitFromStorage", storageAdapter);
            resolve();
          }).catch(err => {
            reject(err);
          });
        }).catch(err => {
          reject(err);
        });
      });
  }

  /**
   * TODO: Channels
   * @param type
   * @param id
   * @returns {any}
   */
  public load(type: EntityType, id?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      switch (type) {
        case EntityType.PROFILE:
          resolve(<Profile>this.privateNode.getProfile());
          break;
        case EntityType.BUDDY:
          resolve(this.privateNode.getBuddy(id));
          break;
        case EntityType.BUDDIES:
          resolve(this.privateNode.getBuddies());
          break;
        case EntityType.CHANNEL:
          this.publicNode.getChannel(id)
            .then(res => { resolve(res); })
            .catch(err => { reject(err); });
          break;/*
        case EntityType.CHANNELS:
          this.publicNode.getChannels()
            .then(res => { resolve(res); })
            .catch(err => { reject(err); });*/
        case EntityType.CHANNELMETA:
          this.publicNode.getChannelMeta(id)
            .then(res => { resolve(res); })
            .catch(err => { reject(err); });
          break;
        case EntityType.CHANNELMETAS:
          this.publicNode.getChannelMetas()
            .then(res => { resolve(<ChannelMeta[]>res); })
            .catch(err => { reject(err); });
              break;
        default:
          console.log("Type not implemented yet");
          reject();
      }
    });
  }

  /**
   *
   * @param type
   * @param entity
   * @returns {any}
   */
  public save(type: EntityType, entity: Entity): Promise<any> {
    return new Promise((resolve, reject) => {
      switch (type) {
        case EntityType.PROFILE:
          this.privateNode.saveProfile(<Profile>entity).then(() => {
            this.publicNode.saveProfileInformation(<Profile>entity)
              .then(() => {resolve(); })
              .catch(err => { reject(err); });
          }).catch(err => { reject(err); });
          break;
        case EntityType.BUDDY:
          this.privateNode.saveBuddy(<Buddy>entity)
            .then(() => {resolve(); })
            .catch(err => { reject(err); });
          break;
        case EntityType.CHANNEL:
          this.publicNode.saveChannel(<Channel>entity)
            .then(() => {resolve(); })
            .catch(err => { reject(err); });
          break;
        case EntityType.CHANNELMETA:
          this.publicNode.saveChannelMeta(entity)
            .then(() => {resolve(); })
            .catch(err => { reject(err); });
          break;
        default:
          console.log("Could not find type of ", entity);
          return;
      }
    });
  }


  public getLinkToPublicNode(): Promise<string> {
    return this.publicNode.getLinkToID(environment.PUBLIC_NODE);
  }

  public getLinkToID(id: string): Promise<string> {
    return this.publicNode.getLinkToID(id);
  }

}
