import {Injectable} from "@angular/core";


/**
 * A simple service to pipe data from components into dialogs.
 */
@Injectable()
export class PostboxPipeService {
  private data: PostboxPipeData[] = [ ];

  public set(key: string, value: any): void {
    let element = this.getPrivate(key);
    if (!element) {
      this.data.push(new PostboxPipeData(key, value));
    } else {
      element.value = value;
    }
  }

  /**
   * Returns the value with the given key.
   * If there is no data, it will return null.
   *
   * Removes the data from the list of available data.
   *
   * @param key
   *
   * @returns {PostboxPipeData}
   */
  public get(key: string): any {
    let index = this.data.findIndex((element) => { return element.key === key; });
    if (-1 === index) {
      return null;
    }

    let data = this.data[index];
    this.data.splice(index, 1);

    return data.value;
  }

  private getPrivate(key: string): any {
    return this.data.find((element) => { return element.key === key; });
  }
}

class PostboxPipeData {
  constructor(public key: string, public value: any) { }
}
