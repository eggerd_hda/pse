import {TestBed, async, inject} from '@angular/core/testing';
import {CryptoService, CryptoBuddy} from './crypto.service';
import {DropboxAdapterService} from './dropbox-adapter.service';
import {environment} from "../../environments/environment";

// flags to toggle the success for all activities performed by the MockedDropboxAdapterService
let SIMULATE_DROPBOX_READ_FAILURE = false;
let SIMULATE_DROPBOX_WRITE_FAILURE = false;

// mocked service.. could be done with a spy, but didn't getPrivate it to work :-/
class MockedDropboxAdapterService extends DropboxAdapterService {
  public getFile(file: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (SIMULATE_DROPBOX_READ_FAILURE === false) {
        // NOTE: if the symmetric encrypted asymmetric-keys have to be replaced, the encrypted private-node has to be
        // replaced as well, because it is encrypted with those keys! Also, if the private-node has to be updated, make
        // sure to update "buddyTestData" as well, because it has to use identifiers and public-keys that exist in the
        // private-node!
        if (file === environment.PRIVATE_NODE) {
          resolve('[{"d7b9b61e31b51503bb650517ed59d2d00985e2998c0414208d092644d09ebaa6":"U2FsdGVkX1+aqbu1MsXA+nAF0YVHDhWdlICMi5wSNrK4QEScO/AYMW9Z57yxju0kpl26T5F43wWgLqyCDCEA+8KN37f88eTfgO8t4KubGSxFDiTVzySw49aAXfsq4i3m+mef7/KfMY4jwTGvYZeowVUoCcgjZpfLkm8uOFAazPfqFqqcEqgerB/JlblDYpnhlJEoNQJnC9moR/7sfPqHKBUEDuc7vWA3/ryjK1J3VYFVCuHFFXf+vO3jcIeA9f+zYp41P/0Zzu1A9v98dD/gnypDrRLn01r1oGIuAL8jnxvhUmrjE6gHLNwoFuVCsolo6zmUH0fw7pwJjFYetRrofk0GB4ICG6oUczhH1OSWRMUad9I6z+SVtUcGWbXQxetk8gSxbdRUjSmqEF1o3pVlvBnMW9QdlSOEdaHHhFQuRyHABKBfp2smIMV7vvurRsxjUB38+dR/hVZtyf/AA+uDVyA3LSAWuGqGL0oxqCHOO+uCfSCQfqlR/VnOMcXaVVtv"},"U2FsdGVkX1/C0Dw7UP8JtvPqz5yhchQdkF4p95mP9lJuPulfTotOx7dB8GZM/yuF5ld4PKhgMBDyI54CciOpvvGAHHjbXzbFHzuYgJ/WtMKoeHgJvRHUdEmQGbUTsMDHmdOjfAjH7nAlX58HB84It0BvQrAtb3X7HRMmM2A1UTGQWT4MZF3TGgWlndACiq6V+uYC0uziNdNWbaP6pYHrBHa1rOlDswJe3v76CJkwJbBrdekqVLQPH/yZpnWfil+j8jZHBuMeTDHnpfru+qshAiGPdcZvyhjdmZwb9s2cT2uR0w4v9WDBuDLKuo81m6PSfYwfKVO1WV5jd+Ef8Qho/lLp3v24ZcnMk1Fj0KrsZVCNVVxACgLF56ZnIXDkWsWwyMIhfkVuM7pHLu7CIFrIfi0zLGlF9o9eBC07yqg6NYX5OkUSaMXQF8OPOtx/kSDUDuAV5Mp4UJ/LfB3THK/IiW2mu2xPCYqKkksiH0BbG6uo/gtWz/Vn2HMa+1HKthoH32BHni900QDPTptQhRYR20txSYuobRkeQWRI0XOrJ1cHZAilUurGkQ2TQ5juG+YLy9aXbRfLT5cBSjpVCVUpRZwY/ZtVSkrc5hqsgW54f3MPJWWLK5EJnJwwceZ5yQbGLteFiPe8d91Gt2bTWPqSbDi3iPJZGg4py3Xpb+WralIoBxOZs2zSd/1F0nnAhck1R2Fub3mjd6wxF+lsnG2wqIqwIOxUx051UqaK2oyweOy/rUWN2RW9X1b60BAa5GDL0LKTHqw1AqWH4Q2GGvflnA2+ilOnkbnevp2V8qFdeBcx5FzHxalM/YV+HaF1EMRm56xcVdT99JZ7WmW4xs/R2we+RfSLylNIw6tvt6SMQWiwF88NGqmTu1F+sbaiHex8O037WjwLc5uPf3Z0wenJZRA1/Mcqm945yrTG5H06QpbxeiA7/Lfbox+WpyVa78t7yVsZyWc88F4oQ/9kt3tMiOqpR+JjyLBfSB30BVcMorCdyxWS+x1DHEzK1A/u8X7PbmWYvSzqn2j43Uf/SFeVvkA8DPKecvgnOsZtG8sQxBLhKsXTimvk7ZAdmTV0o1N9Lz8V6257BiIoadGk7DsUKw5WNOkt5JDOsYp9VExS6udK3EdJ6uldb9bodOG8qfgx/mC+m0UJvcvGBHEu6SLg1dTjgbftBqDizgN01wiiaD3FwhQLXpow9GMaqCPR7lN7u5wyKfApeZBnhAbbeMn+2ik3xMheaZrLRbivlCy55buClXPdhpnJP0xDTmxPqBwpaxdEoC5kPIvp1CLLa52WCR0g0lY3o+iJk4Ht/0xjKT8zYI3jZ+OXsk5ylH6y9Uzkbeg3vtOvkajHYBqAy8E2iSeLzPp9sotb5ZEhhyIz9niEbT0eBiGcyNtOPcpVpDQ3F9MiJ0ui7qaHmhtep14JXaY41OsiysV2iBYB13oRzSGyKT+JaR/l+5z5tUSM2ajaqY6NPPwKKEqorZmPmOBwldvpLYmNa/aOMjg/5xeX4qW8i2Shkyx2ay9NhHMr2sNvCrgxn7SYcigtx1wnmoI1rIGubAVzyJMw/oRbgk4CiybBTschmxw/Ngvhz4X3QjQBnUuZCnjdHbmaxrqEHOHKxu3AIj2NQW6hjiAfAP2jkml/xWv0dNNO659o38d18VtyT+UBlsfeympHEpx/JlzWwIJ9upiypBDI882ktoDF3Mi4YIU2MVavjhw55hylGo03Yx4+nrMwUy0hPvFtwAbQw/ZpAYuLbsGlmbIqs1ekQdLr8TV67aDksSMN3PNUcqs6CLABil/BIErZU28mmaCnsoHItpp6lCxfkqbvWeqwLRpFUYI5k2BhM2t2wqW2ANuCVW0M76huhXoEBNQDUIqCu2MwFqjMoqfrZMs/42sK6EozRxJzwBDKgiU5yzG3DPKK2nBrw/PL15UvonLhSZQcNM5W7fy5eD3L17qTWX7EE4cU95zx4xCWLImtRYAsxcWDLXZebLncTn7NnpvRidL9NcrTcqjryUME41ICkr3OPuT5lOkMqEl330KyEhh6M8LasaM18AT6sI6Dl6mycQwYdu4+cipxgOxLqsdUn3FCa0t48AaFcLQek8uwip1CpGD1V5ArRhRMmLOKXVTWGiWtLC+PsZimDJ3jsHfkj7gkJFQaXL3SR7HAURuh6KH9VVsOQ2ERvz1fSQJxmhy1bVzRLfzJvca2E+EDs25+zx5g9gQCGGSY5DioNjYsiK8cdBPhSurTVAcLsElp1yBx5aqOqFOrERcscF1NQ7ApqyvMDlz6dX3xAwkqIcF2933xmZjDGGg/ExCAFc3MWFcD1J2L+eeP3gxzSNlRFb5jgs7A2iRZkWdPM84cPn7HOZIFdvRCsMfaNtSGW2engJC0BAXXiFwvhgsHjtx+pArI96yupmpfOkr8Arh6797i+6DhFBDEupawRDoE8/uI7RI26cJZKY8Z8KRjlwrrf34As0O/tL2GRIEUSSvqoL3ZHRR3F/DIEGDSWc0W4xsdIt+THLYLh0Q/O70aeDHLVgQR6/Do4d5r3kXwfHiOjeGeayYGtZ/3rkrdii00XXLTw18au16Pu0SMih0OtyRanfD7KVeDLkdm+loYHVHvB4z5iNETugJbekAVSVH+jF7bynLlKdeojB/N9iIUif4VEQqYgr4oXzLORYe4KMOyfgSpY/tP8s6FaEgFfKsyyRSNYbKUM4AxGJRqM3sMM4Cgbp+l4vaYEf2fC8YTReYhfarnDXCL7x1k6idXpX5bJhIC2e1Sp0XGYhvbMurW5TX5XzC1MfjeoHUopldfjmLb8x3waaIvxQqK6Uoac+HlUhD8YSEtZAUZObFIgc7UsYqyX7TBR2YVZYIBWvH+Q00tuTbnBdAeHEA6dEhAAnIlrvkwPokxzOunoLa+QV27c3MT7bzMxW6F6/iZRexxsn6YA28VPKd/x2PKm/6rpye7Tl4bZfqjEx73pcRZCs6KYsDwkfHTZFUrsXU=","1VGcsBN2p/vyPc9FM3/ksEWRwiZ/8UxdmbGQoUiTDf6cGBUfYokUxe4L0J84MOhvpIWGI3QDJbd593M1nyx+01/slLu0IbfpmzOx7rWjg8MwCMfO7FQ4mX20/6QBDMrk"]');
        } else {
          // symmetric encrypted asymmetric-keys. encrpyted with passphrase "userPassphrase"
          resolve('U2FsdGVkX188VZCrcHWg4npZBq5PXgSXBQY6rCgAcaO6itFMr2NW1V21sut3G1xJkvrwPlxObHcNkmUs0fG47NwISOBi9bQv0zuM/47FqyJ6TLzBvSW5/KbQrH1kGlhJrfA+Z+xx4y0++7HsxHjTpnHfVOSti29m/oy5AJCvQVmAkowpFuRme0/x/f9Az2O1TNaoTFbuZz17SfZy0rJvEKb54qIxkLszmIrfc63pVSSYtQBqFUgqjrfOcKBhgadkphZ6cqhnnpQj1mlolAcTOwHtsZUTFZ1rVQMkGvHx6nS1zYacub0a9AnaBn7AyJfJJvLf2LMuMxS8564W5hnpcY4NDeqI0qDi2+wtSGTwDi/JCvPdm3ERU0gUvB0Rr9Dmb0xHnfpR55aXXA8enOTND4X28BeNVYm1Yimceo9M1F4/lqdDtY6aDMDtgnoFSYUvz3P/jsco6Un2TaFfDdO4cADdIWfcC4argE3Y/rXxXUcxgwrMwPXmNs5Nz2XcxismlKV+VRACBzUfQZq0e80VvUo6qkt4cqoQAuYrUOddSttO4iJQTJ1MIbShKG+YT4bnBWyadYU3o13+xAxeIgVK9kw0F+bIm7Ny3MKCTKiBSLiAYpSv25S4bNAT/mUnIDxz');
        }
      } else {
        SIMULATE_DROPBOX_READ_FAILURE = false; // reset flag to default
        reject();
      }
    });
  }

  public writeFile(fileName: string, data: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (SIMULATE_DROPBOX_WRITE_FAILURE === false) {
        resolve();
      } else {
        SIMULATE_DROPBOX_WRITE_FAILURE = false; // reset flag to default
        reject();
      }
    });
  }
}

// this data has to exist in the encrypted private-node that is returned by our mocked dropbox-adapter!
const buddyTestData = {
  buddy1_identifier: 'https://dl.dropboxusercontent.com/s/ztztahh5p32ixb1/public-node?dl=1',
  buddy1_publicKey: 'FS6hAfFKC8IGRWXhHLBtTMlWgEpKUQrqU5lv0Iq74wWi042hAvKMXuq3LkmPKBBOesPNB6XVO8wgNKBeCk/E2fYDlLuVPc/DXtPIe17eu8JpPjAB+Asrntyo0UQjS9Hp',
  buddy2_identifier: 'https://dl.dropboxusercontent.com/s/5ym60ib4g6jhtpg/public-node?dl=1',
  buddy2_publicKey: 'sFuZWY/Xxy6cXS+my29nnkJx7xX/MDiJ0So0aT1o1sHRwPPC0kFPb8sQvxs2syV/2benItsX1sBVa/yAwDQ1w9jgJ9Tgw9oj5O5lSqQfV3wFQWnnvxU9lMYd0/aD1KaJ'
};

describe('Service: Crypto', () => {
  let SERVICE: CryptoService = null;

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1200000; // required to prevent timeout of some async calls. encryption takes its time..
    TestBed.configureTestingModule({
      providers: [
        {provide: DropboxAdapterService, useClass: MockedDropboxAdapterService },
        CryptoService,
      ],
    });
  });

  it('should construct successfully', inject([CryptoService], (cs: CryptoService) => {
    SERVICE = cs;
    expect(SERVICE).toBeDefined();
  }));

  // +++++ Service has not been initialized +++++

  it('should not allow to use any function that uses encryption, because init() has not been called yet', async(() => {
    let error = new Error('CryptoService has to be initialized first via init()!');

    expect(() => {
      SERVICE.encrypt('some content');
    }).toThrow(error);

    expect(() => {
      SERVICE.decrypt('the crypto-string');
    }).toThrow(error);

    expect(() => {
      SERVICE.addBuddy('the crypto-string', [new CryptoBuddy('buddies unique identifier', 'buddies public-key')]);
    }).toThrow(error);

    SERVICE.removeBuddy('the crypto-string', [new CryptoBuddy('buddies unique identifier', 'buddies public-key')]).then((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    }).catch((results) => {
      expect(results).toEqual(error);
    });

    expect(() => {
      SERVICE.updateContent('the current crypto-string', 'the new content in plaintext');
    }).toThrow(error);

    expect(() => {
      SERVICE.getPublicKey();
    }).toThrow(error);

    expect(() => {
      SERVICE.createSignature('lorem ipsum');
    }).toThrow(error);

    expect(() => {
      SERVICE.verifySignature('signature', 'plaintextContent', 'publicKey');
    }).toThrow(error);

    expect(() => {
      SERVICE.getSignaturePublicKey();
    }).toThrow(error);
  }));

  it('should allow to use the hash functions, even so init() has not been called yet', () => {
    expect(SERVICE.hash('plaintext').length).toEqual(64);
    expect(SERVICE.filenameHash('plaintext').length).toEqual(40);
  });

  // +++++ Initializing service +++++
  // Injecting new instance of service, instead of using SERVICE, because service can only be initialized once!
  // So, only the last test for init() will use SERVICE!

  it('.init() should generate and persist new asymmetric-keys during first-time-setup', async(inject([CryptoService], (cs: CryptoService) => {
    SIMULATE_DROPBOX_READ_FAILURE = true; // simulate: asymmetric-keys have not been saved on the online storage, yet

    cs.init('passphrase the user has entered').then((result) => {
      expect(result).toBeTruthy();
    }).catch((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    });
  })));

  it('.init() should fail if newly generated asymmetric-keys could not be persited during first-time-setup', async(inject([CryptoService], (cs: CryptoService) => {
    SIMULATE_DROPBOX_READ_FAILURE = true; // simulate: asymmetric-keys have not been saved on the online storage, yet
    SIMULATE_DROPBOX_WRITE_FAILURE = true; // simulate: newly generated asymmetric-keys could not be saved on online storage

    cs.init('passphrase the user has entered').then((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    }).catch((result) => {
      expect(result.message).toContain('Failed to save asymmetric-keys to storage:');
    });
  })));

  it('.init() should fail because the wrong passphrase was provided', async(inject([CryptoService], (cs: CryptoService) => {
    cs.init('this is not the correct passphrase').then((result) => {
      expect(result).toBeFalsy();
    }).catch((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    });
  })));

  it('.init() should load the asymmetric-keys from the storage and parse them', async(() => {
    SERVICE.init('userPassphrase').then((result) => {
      expect(result).toBeTruthy();
    }).catch((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    });
  }));

  // +++++ Encryption +++++

  let ENCRYPTED_CONTENT_PLAINTEXT = 'lorem ipsum';
  let ENCRYPTED_CONTENT = null; // will be used by some other tests
  let ENCRYPTED_CONTENT_SHARED = null; // will be used by some other tests

  it('.encrypt() should fail, because the passed content is already encrypted', () => {
    let alreadyEncryptedContent = '[{"2c5a3f34d37a6d27554389ce4ca03207238f56307164f026eeb794264bcd4edb":"U2FsdGVkX18+IuG5v8AdifuETuJRwuh7qnHa6yHoVZhHFIL5Wf0+xGBKj9SLmrVjbLUp10LEbYI1h/Nun9pWOQXi2dT+Th2Z4OF+M21KloniTHIeK8by5QbSg9fWjmnP7c/7/gbg7p5oYvMKevLHTZx4AzU4vH81rZmq70vMT58uJiPTFCdjAoZ2/+8jLBDDcSjJoPrZEjo3L92VyYuDlHBC4byLygMaI8RhfQPle3aF/Ln6o0gP/i9wqnvvBwSeBXfRfMSjC1VmOsAIwgPO/FJQzdvi8C2/cXehaqCaPihILiCg5Wjsd9i5PiBUyDyojKdBURf/gkix0HL4gFkhRZtMDR6U9J+hI8LFyr4ugD5kgHYOjAxPVIz5oxeZr/Jb223PVs1u377t6wD3U57KJ2QIi7sm/acJgn4w75VCYxDfwXCrAj7SiDyCcCyTO4AESbTzHwpnMLCmOP2JO0sBwwR5h1pRvzJVnPsizWbidXp5M3ONmoyxwge9qlrnqEEE"},"U2FsdGVkX1/LER6yUapJa8klxwGAmvkyh1ZVMGfZAAJosGMSVyIgZBZxyL5ZIzf2+wYGiAJA2an7+QuF5kLBUXn01pppPKD+EghbYh7h2x5BoJFFUGcpI2S3fPa6dozh4/B7i8k94DG2yRnaeMAybrMwqW6KhFpFeKC4+5rJpgjWoProWU89DmW22vbzlfmIRQVJ7Bo9AlMF5nrRWRaToAvli5M4Dkvz16UdiKt+c3gofZw0HTOIWghBUAUi23S/f1OwhfkSnWBMyzWCJpCtco0FrNnFiSnN9+N2S2VdWjMcAk2QNJmy3mmUU21rdDfgtiCjRlz7xjQKzoDGaF6/1NyRk1/XDZEEeeHaDzDj0SvmciG6C5cTXCBL7deF6/MrgkoD7Qf+8gr2hqa+jh7wrrVY67SEF8IWYDtunQ01XjY/q+2U1+PWIQ2eHJTUzdktt7xOnTRhki1NbA78uY9FBQUp2TjvkYWDjm7UVUL1KgKkCJ4RG8NQKBfm0wLM2i0k7EIX8YHsrnHTZJiycKlrBG/WUpzMPT70LzocKsq5lSa/CaC78RRhmhhQeya+CByYqZtwWXZjMsd1JHD0Dh8ugIBwYTjzG4nK0R32eNqmD8Z/hgsxBeiC03fA08z9/j+FarpNnKnfoVTTdTF0WIPBRLy24t7vzuvwQ0K1KZhp2JvKTo01tCPOUIR6XuZxigds","3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL"]';

    expect(() => {
      SERVICE.encrypt(alreadyEncryptedContent);
    }).toThrow(new Error('The passed content seems to be already encrypted!'));
  });

  it('.encrypt() should return a valid crypto-string', () => {
    ENCRYPTED_CONTENT = SERVICE.encrypt(ENCRYPTED_CONTENT_PLAINTEXT);

    expect(ENCRYPTED_CONTENT).toMatch(/^\[\{".*":".*"},".*",".*"]$/);
  });

  // has to be tested later, when the unique identifiers are completely implemented
  /*
  it('.encrypt() should return a valid crypto-string, containing my buddies', () => {
    // define test here
  });
  */

  // +++++ Decryption +++++

  it('.decrypt() should fail, because an invalid crypto-string has been passed', () => {
    expect(() => {
      SERVICE.decrypt('definitively not encrypted stuff');
    }).toThrowError(/Invalid crypto-string has been passed!/);
  });

  it('.decrypt() should fail, because we are not allowed to decrypt it (missing from the header)', () => {
    let alreadyEncryptedContent = '[{"definitively not our unique identifier":"U2FsdGVkX18+IuG5v8AdifuETuJRwuh7qnHa6yHoVZhHFIL5Wf0+xGBKj9SLmrVjbLUp10LEbYI1h/Nun9pWOQXi2dT+Th2Z4OF+M21KloniTHIeK8by5QbSg9fWjmnP7c/7/gbg7p5oYvMKevLHTZx4AzU4vH81rZmq70vMT58uJiPTFCdjAoZ2/+8jLBDDcSjJoPrZEjo3L92VyYuDlHBC4byLygMaI8RhfQPle3aF/Ln6o0gP/i9wqnvvBwSeBXfRfMSjC1VmOsAIwgPO/FJQzdvi8C2/cXehaqCaPihILiCg5Wjsd9i5PiBUyDyojKdBURf/gkix0HL4gFkhRZtMDR6U9J+hI8LFyr4ugD5kgHYOjAxPVIz5oxeZr/Jb223PVs1u377t6wD3U57KJ2QIi7sm/acJgn4w75VCYxDfwXCrAj7SiDyCcCyTO4AESbTzHwpnMLCmOP2JO0sBwwR5h1pRvzJVnPsizWbidXp5M3ONmoyxwge9qlrnqEEE"},"U2FsdGVkX1/LER6yUapJa8klxwGAmvkyh1ZVMGfZAAJosGMSVyIgZBZxyL5ZIzf2+wYGiAJA2an7+QuF5kLBUXn01pppPKD+EghbYh7h2x5BoJFFUGcpI2S3fPa6dozh4/B7i8k94DG2yRnaeMAybrMwqW6KhFpFeKC4+5rJpgjWoProWU89DmW22vbzlfmIRQVJ7Bo9AlMF5nrRWRaToAvli5M4Dkvz16UdiKt+c3gofZw0HTOIWghBUAUi23S/f1OwhfkSnWBMyzWCJpCtco0FrNnFiSnN9+N2S2VdWjMcAk2QNJmy3mmUU21rdDfgtiCjRlz7xjQKzoDGaF6/1NyRk1/XDZEEeeHaDzDj0SvmciG6C5cTXCBL7deF6/MrgkoD7Qf+8gr2hqa+jh7wrrVY67SEF8IWYDtunQ01XjY/q+2U1+PWIQ2eHJTUzdktt7xOnTRhki1NbA78uY9FBQUp2TjvkYWDjm7UVUL1KgKkCJ4RG8NQKBfm0wLM2i0k7EIX8YHsrnHTZJiycKlrBG/WUpzMPT70LzocKsq5lSa/CaC78RRhmhhQeya+CByYqZtwWXZjMsd1JHD0Dh8ugIBwYTjzG4nK0R32eNqmD8Z/hgsxBeiC03fA08z9/j+FarpNnKnfoVTTdTF0WIPBRLy24t7vzuvwQ0K1KZhp2JvKTo01tCPOUIR6XuZxigds","3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL"]';

    expect(() => {
      SERVICE.decrypt(alreadyEncryptedContent);
    }).toThrowError(/You are missing from the encryption-header!/);
  });

  it('.decrypt() should fail, because the crypto-string has been corrupted', () => {
    // overwriting the signature to simulate a corrupted crypto-string
    let alreadyEncryptedContent = JSON.parse(ENCRYPTED_CONTENT);
    alreadyEncryptedContent[2] = '3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL';
    alreadyEncryptedContent = JSON.stringify(alreadyEncryptedContent);

    expect(() => {
      SERVICE.decrypt(alreadyEncryptedContent);
    }).toThrow(new Error('The crypto-string has been corrupted!'));
  });

  it('.decrypt() should be able to decrypt a valid crypto-string and ignore the signature', () => {
    // overwriting the signature to simulate a corrupted crypto-string
    let alreadyEncryptedContent = JSON.parse(ENCRYPTED_CONTENT);
    alreadyEncryptedContent[2] = '3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL';
    alreadyEncryptedContent = JSON.stringify(alreadyEncryptedContent);

    expect(ENCRYPTED_CONTENT).not.toBeNull();
    expect(SERVICE.decrypt(alreadyEncryptedContent, false)).toEqual(ENCRYPTED_CONTENT_PLAINTEXT);
  });

  it('.decrypt() should be able to decrypt a valid crypto-string', () => {
    expect(ENCRYPTED_CONTENT).not.toBeNull();
    expect(SERVICE.decrypt(ENCRYPTED_CONTENT)).toEqual(ENCRYPTED_CONTENT_PLAINTEXT);
  });

  // +++++ Adding Buddies +++++

  it('.addBuddy() should fail, because an invalid crypto-string has been passed', () => {
    expect(() => {
      SERVICE.addBuddy('definitively not encrypted stuff', [new CryptoBuddy('buddy_1_identifier', 'buddy_1_public_key')]);
    }).toThrowError(/Invalid crypto-string has been passed!/);
  });

  it('.addBuddy() should fail, because we are not even in the crypto-header ourself', () => {
    let alreadyEncryptedContent = '[{"definitively not our unique identifier":"U2FsdGVkX18+IuG5v8AdifuETuJRwuh7qnHa6yHoVZhHFIL5Wf0+xGBKj9SLmrVjbLUp10LEbYI1h/Nun9pWOQXi2dT+Th2Z4OF+M21KloniTHIeK8by5QbSg9fWjmnP7c/7/gbg7p5oYvMKevLHTZx4AzU4vH81rZmq70vMT58uJiPTFCdjAoZ2/+8jLBDDcSjJoPrZEjo3L92VyYuDlHBC4byLygMaI8RhfQPle3aF/Ln6o0gP/i9wqnvvBwSeBXfRfMSjC1VmOsAIwgPO/FJQzdvi8C2/cXehaqCaPihILiCg5Wjsd9i5PiBUyDyojKdBURf/gkix0HL4gFkhRZtMDR6U9J+hI8LFyr4ugD5kgHYOjAxPVIz5oxeZr/Jb223PVs1u377t6wD3U57KJ2QIi7sm/acJgn4w75VCYxDfwXCrAj7SiDyCcCyTO4AESbTzHwpnMLCmOP2JO0sBwwR5h1pRvzJVnPsizWbidXp5M3ONmoyxwge9qlrnqEEE"},"U2FsdGVkX1/LER6yUapJa8klxwGAmvkyh1ZVMGfZAAJosGMSVyIgZBZxyL5ZIzf2+wYGiAJA2an7+QuF5kLBUXn01pppPKD+EghbYh7h2x5BoJFFUGcpI2S3fPa6dozh4/B7i8k94DG2yRnaeMAybrMwqW6KhFpFeKC4+5rJpgjWoProWU89DmW22vbzlfmIRQVJ7Bo9AlMF5nrRWRaToAvli5M4Dkvz16UdiKt+c3gofZw0HTOIWghBUAUi23S/f1OwhfkSnWBMyzWCJpCtco0FrNnFiSnN9+N2S2VdWjMcAk2QNJmy3mmUU21rdDfgtiCjRlz7xjQKzoDGaF6/1NyRk1/XDZEEeeHaDzDj0SvmciG6C5cTXCBL7deF6/MrgkoD7Qf+8gr2hqa+jh7wrrVY67SEF8IWYDtunQ01XjY/q+2U1+PWIQ2eHJTUzdktt7xOnTRhki1NbA78uY9FBQUp2TjvkYWDjm7UVUL1KgKkCJ4RG8NQKBfm0wLM2i0k7EIX8YHsrnHTZJiycKlrBG/WUpzMPT70LzocKsq5lSa/CaC78RRhmhhQeya+CByYqZtwWXZjMsd1JHD0Dh8ugIBwYTjzG4nK0R32eNqmD8Z/hgsxBeiC03fA08z9/j+FarpNnKnfoVTTdTF0WIPBRLy24t7vzuvwQ0K1KZhp2JvKTo01tCPOUIR6XuZxigds","3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL"]';

    expect(() => {
      SERVICE.addBuddy(alreadyEncryptedContent, [new CryptoBuddy('buddy_1_identifier', 'buddy_1_public_key')]);
    }).toThrowError(/You are missing from the encryption-header!/);
  });

  it('.addBuddy() shoul successfully add our buddy to the crypto-header', () => {
    ENCRYPTED_CONTENT_SHARED = SERVICE.addBuddy(ENCRYPTED_CONTENT, [
      new CryptoBuddy(buddyTestData.buddy1_identifier, buddyTestData.buddy1_publicKey),
      new CryptoBuddy(buddyTestData.buddy2_identifier, buddyTestData.buddy2_publicKey)
    ]);

    expect(ENCRYPTED_CONTENT_SHARED).toContain(SERVICE.hash(buddyTestData.buddy1_publicKey));
    expect(ENCRYPTED_CONTENT_SHARED).toContain(SERVICE.hash(buddyTestData.buddy2_publicKey));
  });

  // +++++ Removing a Buddy +++++

  it('.removeBuddy() should successfully remove one of two buddies', async(() => {
    expect(ENCRYPTED_CONTENT_SHARED).not.toBeNull();

    SERVICE.removeBuddy(ENCRYPTED_CONTENT_SHARED, [new CryptoBuddy(buddyTestData.buddy1_identifier, buddyTestData.buddy1_publicKey)]).then((result) => {
      expect(result).not.toContain(SERVICE.hash(buddyTestData.buddy1_publicKey));
      expect(result).toContain(SERVICE.hash(buddyTestData.buddy2_publicKey));
      expect(SERVICE.decrypt(result, false)).toEqual(ENCRYPTED_CONTENT_PLAINTEXT); // and we should still be able to decrypt it
    }).catch((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    });
  }));

  it('.removeBuddy() should successfully remove all buddies', async(() => {
    expect(ENCRYPTED_CONTENT_SHARED).not.toBeNull();

    SERVICE.removeBuddy(ENCRYPTED_CONTENT_SHARED, [
      new CryptoBuddy(buddyTestData.buddy1_identifier, buddyTestData.buddy1_publicKey),
      new CryptoBuddy(buddyTestData.buddy2_identifier, buddyTestData.buddy2_publicKey)
    ]).then((result) => {
      expect(result).not.toContain(SERVICE.hash(buddyTestData.buddy1_publicKey));
      expect(result).not.toContain(SERVICE.hash(buddyTestData.buddy2_publicKey));
      expect(SERVICE.decrypt(result, false)).toEqual(ENCRYPTED_CONTENT_PLAINTEXT); // and we should still be able to decrypt it
    }).catch((result) => {
      console.log(result);
      expect(false).toBeTruthy(); // should never be called during this test!
    });
  }));

  // +++++ Updating Already Encrypted Content +++++

  it('.updateContent() should fail, because an invalid crypto-string has been passed', () => {
    expect(() => {
      SERVICE.updateContent('definitively not encrypted stuff', 'my new content');
    }).toThrowError(/Invalid crypto-string has been passed!/);
  });

  it('.updateContent() should fail, because we are not even in the crypto-header ourself', () => {
    let alreadyEncryptedContent = '[{"definitively not our unique identifier":"U2FsdGVkX18+IuG5v8AdifuETuJRwuh7qnHa6yHoVZhHFIL5Wf0+xGBKj9SLmrVjbLUp10LEbYI1h/Nun9pWOQXi2dT+Th2Z4OF+M21KloniTHIeK8by5QbSg9fWjmnP7c/7/gbg7p5oYvMKevLHTZx4AzU4vH81rZmq70vMT58uJiPTFCdjAoZ2/+8jLBDDcSjJoPrZEjo3L92VyYuDlHBC4byLygMaI8RhfQPle3aF/Ln6o0gP/i9wqnvvBwSeBXfRfMSjC1VmOsAIwgPO/FJQzdvi8C2/cXehaqCaPihILiCg5Wjsd9i5PiBUyDyojKdBURf/gkix0HL4gFkhRZtMDR6U9J+hI8LFyr4ugD5kgHYOjAxPVIz5oxeZr/Jb223PVs1u377t6wD3U57KJ2QIi7sm/acJgn4w75VCYxDfwXCrAj7SiDyCcCyTO4AESbTzHwpnMLCmOP2JO0sBwwR5h1pRvzJVnPsizWbidXp5M3ONmoyxwge9qlrnqEEE"},"U2FsdGVkX1/LER6yUapJa8klxwGAmvkyh1ZVMGfZAAJosGMSVyIgZBZxyL5ZIzf2+wYGiAJA2an7+QuF5kLBUXn01pppPKD+EghbYh7h2x5BoJFFUGcpI2S3fPa6dozh4/B7i8k94DG2yRnaeMAybrMwqW6KhFpFeKC4+5rJpgjWoProWU89DmW22vbzlfmIRQVJ7Bo9AlMF5nrRWRaToAvli5M4Dkvz16UdiKt+c3gofZw0HTOIWghBUAUi23S/f1OwhfkSnWBMyzWCJpCtco0FrNnFiSnN9+N2S2VdWjMcAk2QNJmy3mmUU21rdDfgtiCjRlz7xjQKzoDGaF6/1NyRk1/XDZEEeeHaDzDj0SvmciG6C5cTXCBL7deF6/MrgkoD7Qf+8gr2hqa+jh7wrrVY67SEF8IWYDtunQ01XjY/q+2U1+PWIQ2eHJTUzdktt7xOnTRhki1NbA78uY9FBQUp2TjvkYWDjm7UVUL1KgKkCJ4RG8NQKBfm0wLM2i0k7EIX8YHsrnHTZJiycKlrBG/WUpzMPT70LzocKsq5lSa/CaC78RRhmhhQeya+CByYqZtwWXZjMsd1JHD0Dh8ugIBwYTjzG4nK0R32eNqmD8Z/hgsxBeiC03fA08z9/j+FarpNnKnfoVTTdTF0WIPBRLy24t7vzuvwQ0K1KZhp2JvKTo01tCPOUIR6XuZxigds","3hELgXor3Bukw5ABijNetHbH2yO7rSr+Gyb06LWyAYoP4c0BVvTypomtr/d02/YiyhSe3XHFmyphhBUbZ0SinjRSTYl+S+GW4UnWWk2ZawTnmT3CGsGIH0Sim+hIgzsL"]';

    expect(() => {
      SERVICE.updateContent(alreadyEncryptedContent, 'my new content');
    }).toThrowError(/You are missing from the encryption-header!/);
  });

  it('.updateContent() should successfully overwrite the old content in the passed crypto-string', () => {
    let newContent = 'my new content',
      updatedCryptoString = SERVICE.updateContent(ENCRYPTED_CONTENT, newContent);

    expect(SERVICE.decrypt(updatedCryptoString)).toEqual(newContent);
  });

  // +++++ Hashing +++++

  it('.hash() should return a string with the length of 64', () => {
    expect(SERVICE.hash('my content').length).toEqual(64);
  });

  it('.hash() should always return the same hash for the same content', () => {
    let content = 'my content',
      firstHash = SERVICE.hash(content),
      secondHash = SERVICE.hash(content);

    expect(firstHash).toEqual(secondHash);
  });

  it('.hash() called with salt should be different from call without a salt', () => {
    let content = 'my content',
      saltedHash = SERVICE.hash(content, 'my salt'),
      normalHash = SERVICE.hash(content);

    expect(normalHash).not.toEqual(saltedHash);
  });

  it('.fielnameHash() should return a string with the length of 40', () => {
    expect(SERVICE.filenameHash('my_filename').length).toEqual(40);
  });

  it('.fielnameHash() should always return the same hash for the same content', () => {
    let content = 'my_filename',
      firstHash = SERVICE.filenameHash(content),
      secondHash = SERVICE.filenameHash(content);

    expect(firstHash).toEqual(secondHash);
  });

  // +++++ Getter +++++

  it('.getPublicKey() should return a string with the length of 128', () => {
    expect(SERVICE.getPublicKey().length).toEqual(128);
  });

  it('.getSignaturePublicKey() should return a string with the length of 128', () => {
    expect(SERVICE.getSignaturePublicKey().length).toEqual(128);
  });

});
