import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './services/auth-guard.service';

import {LoginPage} from './pages/login/login.page';
import {HomePage} from './pages/home/home.page';
import {BuddylistPageComponent} from './pages/buddylist/buddylist.page';
import {UserPage} from './pages/user/user.page';
import {OptionsPage} from './pages/options/options.page';
import {ChatPage} from "./pages/chat/chat.page";

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: LoginPage
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomePage
  },
  {
    path: 'buddyList',
    canActivate: [AuthGuard],
    component: BuddylistPageComponent
  },
  {
    path: 'user/:id',
    canActivate: [AuthGuard],
    component: UserPage
  },
  {
    path: 'options',
    canActivate: [AuthGuard],
    component: OptionsPage
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'chat',
    canActivate: [AuthGuard],
    component: ChatPage
  },
  {
    path: '**',
    canActivate: [AuthGuard],
    component: LoginPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
