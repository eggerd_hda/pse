import {Component, Input} from '@angular/core';
import {ProfileRepositoryService} from '../../repositories/profile-repository.service';
import {Profile} from '../../entities/profile/profile';
import {DropboxAdapterService} from '../../services/dropbox-adapter.service';
import {Router, ActivatedRoute, RoutesRecognized, NavigationStart, NavigationEnd} from '@angular/router';
import {ChannelRepositoryService} from "../../repositories/channel-repository.service";
import {AuthGuard} from "../../services/auth-guard.service";

@Component({
    selector: 'app-navigation-bar',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
    public showFullHeader: boolean = false;

    private profile: Profile;

    constructor(private profileRepository: ProfileRepositoryService,
                private channelRepository: ChannelRepositoryService,
                private authService: AuthGuard,
                private router: Router,
                private dropbox: DropboxAdapterService) {
        this.showFullHeader = this.dropbox.isAuthenticated();

        /*
         * This litte part of code prevents other pages
         * but the login page from being visited
         * if the current user is not authenticated.
         */
        this.router.events.subscribe(
            (value) => {
                if (value instanceof NavigationEnd) {
                    if (!this.dropbox.isAuthenticated() &&
                        -1 === this.router.url.indexOf('login')) {
                        console.log("User not authenticated");
                        this.router.navigate(['/login']);
                    }
                }
            }
        );
    }

    navigateProfile(): void {
        this.router.navigate(['/user', this.profileRepository.profile.id]);
    }

    logout(): void {
        this.dropbox.logout().then(() => {
            window.location.href = window.location.origin;
        });
    }
}
