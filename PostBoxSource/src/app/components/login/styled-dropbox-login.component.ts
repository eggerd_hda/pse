import {Component} from '@angular/core';
import {DropboxAdapterService} from '../../services/dropbox-adapter.service';
import {Router} from '@angular/router';
import {ProfileRepositoryService} from '../../repositories/profile-repository.service';

@Component({
  selector: 'dropbox-login',
  templateUrl: './dropbox-login.component.html',
  styleUrls: [ './dropbox-login.component.scss' ],
})
export class DropboxLoginComponent {

  private authenticated: boolean = false;

  constructor(private dropbox: DropboxAdapterService,
              private router: Router,
              private profileRepository: ProfileRepositoryService) {

    this.authenticated = this.dropbox.isAuthenticated();
  }

  login(): void {

    if (this.authenticated) {
      console.log("wird ausgeloggt");
      this.dropbox.logout()
        .then((result) => {
          this.authenticated = this.dropbox.isAuthenticated();
          console.log("erfolgreich ausgeloggt");
          localStorage.removeItem('userToken');
          window.location.reload();
        })
        .catch((error) => {
          console.log("logout nicht erfolgreich");
        });
    } else {
      this.dropbox.login();
    }
  }
}
