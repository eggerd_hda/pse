import {Component, Input, SimpleChange} from '@angular/core';

/**
 * A small component that can be injected in any page we
 */
@Component({
  selector: 'full-screen-loader',
  styleUrls: ['full-screen-loader.component.scss'],
  templateUrl: 'full-screen-loader.component.html',
})
export class FullScreenLoaderComponent {
  @Input()
  isVisible: boolean = false;



  constructor() {

  }

  show(): void {
    this.isVisible = true;
  }

  hide(): void {
    this.isVisible = false;
  }

  isActive(): boolean {
    return this.isVisible;
  }
}
