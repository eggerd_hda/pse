import {Component, HostListener, Input} from "@angular/core";
import {BuddyRepositoryService} from "../../../repositories/buddy-repository.service";
import {Buddy} from "../../../entities/buddy";
import {MdlDialogReference} from "angular2-mdl";
import {SelectableListComponent} from "./selectable-list.component";
import {ChannelRepositoryService} from "../../../repositories/channel-repository.service";
import {SingleChat} from "../../../entities/chat/single-chat";
import {CryptoBuddy} from "../../../services/crypto.service";
import {PostboxPipeService} from "../../../services/pipe/pipe.service";

declare let componentHandler;

@Component({
  selector: 'list-dialog',
  templateUrl: 'list-dialog.component.html',
  styleUrls: [ 'list-dialog.component.scss' ],
})
export class ListDialogComponent {

  @Input() private title: string;

  @Input() private data: any;

  constructor(private dialog: MdlDialogReference,
              private pipeService: PostboxPipeService) {

    this.data = this.pipeService.get("pb-list-dialog-buddies");
    this.title = this.pipeService.get("pb-list-dialog-title");
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.dialog.hide();
  }
}
