import {Component, Input, Output, EventEmitter} from "@angular/core";
@Component({
  selector: 'selectable-list-item',
  templateUrl: './selectable-list.component.html',
  styleUrls: [ './selectable-list.component.scss' ],
})
export class SelectableListComponent {
  /**
   * The value that will we displayed in the component.
   */
  @Input() public value: string;

  /**
   * The key by which the listener can identfy the component.
   */
  @Input() public key: any;

  /**
   * Whether or not this component is selected.
   *
   * @type {boolean}
   */
  @Input() public active: boolean = false;

  @Output() selected = new EventEmitter<this>();

  public elementClicked(): void {
    this.active = !this.active;

    // Trigger the event to inform the parent about the state change.
    this.selected.emit(this);
  }
}
