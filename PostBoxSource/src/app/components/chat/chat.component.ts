import {Component} from '@angular/core';
import {SingleChat} from "../../entities/chat/single-chat";
import {Message} from "../../entities/chat/message";
import {MockChatAI} from "./mock-chat-ai";
import {ChannelRepositoryService} from "../../repositories/channel-repository.service";
import {Observable} from "rxjs";
import {Channel} from "../../entities/channel";
import {ProfileRepositoryService} from "../../repositories/profile-repository.service";
import {PostType} from "../../entities/enums/post-type.enum";
import {BuddyRepositoryService} from "../../repositories/buddy-repository.service";
import {AddChatComponent} from "./modals/add-chat.component";
import {MdlDialogService} from "angular2-mdl";
import {PostboxPipeService} from "../../services/pipe/pipe.service";
import {MomentService} from "../../services/moment/moment.service";
import {toUnicode} from "punycode";
import {EmojiService} from "../../services/emoji/emoji.service";
import {ListDialogComponent} from "./selectable-list/list-dialog.component";

declare let $: any;

@Component({
  selector: 'chat',
  templateUrl: 'chat.component.html',
  styleUrls: [ 'chat.component.scss' ],
})
export class ChatComponent {

  /**
   * All single chats.
   */
  private chats: SingleChat[] = [ ];

  /**
   * The chat that is currently being displayed.
   */
  private currentChatIndex: number = 0;

  private newMessage: string;

  private sending: boolean = false;

  constructor(private channelRepo: ChannelRepositoryService,
              private profileRepo: ProfileRepositoryService,
              private buddyRepo: BuddyRepositoryService,
              private dialogService: MdlDialogService,
              private pipeService: PostboxPipeService,
              private momentService: MomentService,
              private emojiService: EmojiService) {
    this.initializeEmojiPicker();
  }

  initializeEmojiPicker(): void {
    $(document).ready(() => {
      $("#chat-write-message input").emojiPicker({
        height: '300px',
        width: '200px'
      })
      .focus(() => {
        // Okay, we need to access the value here with jQuery, because for
        // some fucked up reason, angular doesn't recognize a single emoji
        // as text, therefore the newMessage will not be set.
        this.messageInputChanged($("#chat-write-message input").val());
      })
      .keyup((event) => {
        if (event.keyCode === 13) {
          this.sendMessage();
        }
      });
    });
  }

  messageInputChanged(originalValue: string): void {
    this.detectAndReplaceEmojis(originalValue);
  }

  detectAndReplaceEmojis(value): void {
    this.newMessage = this.emojiService.replaceWithShortcodes(value);

    let regex = /(\ud83c[\udf00-\udfff]|\ud83d[\udc00-\ude4f]|\ud83d[\ude80-\udeff])/g,
      match;

    // query through all matches
    // while (match = regex.exec(value)) {
    //   value = this.replaceEmoji(value, match);
    // }
    //
    // this.newMessage = value;
  }

  replaceEmoji(original, match): string {
    console.log(match);

    let index = match.index;
    let emoji = match[0];

    let originalEmojiData = $.fn.emojiPicker.emojis;
    for (let row of originalEmojiData) {
      let originalEmoji = this.toUnicode(row.unicode.apple);

      if (emoji === originalEmoji) {
        let replaced = original.substr(0, index) + "{:" + row.shortcode + ":}" + original.substr(index + 2);
        return replaced;
      }
    }

    return original;
  }

  toUnicode(code): string {
    try {
      let codes = code.split('-').map(function (value, index) {
        return parseInt(value, 16);
      });
      return String.fromCodePoint.apply(null, codes);
    } catch (error) {
      console.log(error);
    }
    return "";
  }

  ngOnInit(): void {
    this.channelRepo.newMessages = false;
    this.fillWithData();
  }

  ngOnDestroy(): void {
    if (this.chats.length === 0) {
      return;
    }

    // Just if new messages came, but you were on the chat page
    this.channelRepo.newMessages = false;
    this.chats[this.currentChatIndex].channel.newMessages = 0;
  }

  public sendMessage(): void {
    if (!this.newMessage || this.sending) {
      // the current message is not set
      return;
    }

    this.sending = true;

    this.channelRepo.addPostToChannel(PostType.Text, this.newMessage.trim(), this.chats[this.currentChatIndex].channel.id)
      .then(() => {
        this.sending = false;
      }).catch(err => {
        this.sending = false;
        console.log("Error", err);
      });
    this.newMessage = "";
  }

  private changeActiveChat(index: number): void {
    this.chats[this.currentChatIndex].channel.newMessages = 0;
    this.currentChatIndex = index;
    this.chats[this.currentChatIndex].channel.newMessages = 0;
  }

  /**
   * Used by our template to get the current messages and to prevent
   * an error if there is no current chat.
   *
   * @returns {any}     The current chat messages or an empty array
   *                    if there is no current chat.
   */
  private currentChatMessages(): Message[] {
    if (!!this.chats[this.currentChatIndex]) {
      return this.chats[this.currentChatIndex].messages;
    }

    return [ ];
  }

  private canAddToChat(chat: SingleChat): boolean {
    if (!chat.channel.ownerID) {
      // The chat is not moderated. We can add new participants.
      return true;
    }

    return chat.channel.ownerID === this.profileRepo.profile.id;
  }

  /**
   * TODO: This is mega-ugly
   * Find another solution for updating chats
   */
  private fillWithData(): void {

    this.channelRepo.findChatChannels().subscribe(channels => {

      if (!channels) {
        return;
      }

      for (let index in channels) {
        if (channels.hasOwnProperty(index)) {
          let found = this.chats.find(item => {
            return item.channel.id === index;
          });
          if (!found) {
            console.log("New Chat!");
            this.chats.push(new SingleChat(this.buddyRepo, channels[index], this.profileRepo.profile));
          }
        }
      }
    });
  }

  private showUpdateChatDialog(): void {
    this.pipeService.set("pb-update-chat", this.chats[this.currentChatIndex]);

    this.showAddChatDialog();
  }

  private showAddChatDialog(): void {
    let addDialog = this.dialogService.showCustomDialog({
      component: AddChatComponent,
      isModal: true
    });
  }

  private showBuddiesInChat(): void {
    let data = this.chats[this.currentChatIndex].channel.participants.map((row) => {
      return row.name;
    });

    this.pipeService.set("pb-list-dialog-buddies", data);
    this.pipeService.set("pb-list-dialog-title", "Chatteilnehmer");

    let buddiesDialog = this.dialogService.showCustomDialog({
      component: ListDialogComponent,
      isModal: true
    });
  }
}
