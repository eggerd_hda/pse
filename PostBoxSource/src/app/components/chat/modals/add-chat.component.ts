import {Component, HostListener, Input} from "@angular/core";
import {BuddyRepositoryService} from "../../../repositories/buddy-repository.service";
import {Buddy} from "../../../entities/buddy";
import {MdlDialogReference} from "angular2-mdl";
import {SelectableListComponent} from "../selectable-list/selectable-list.component";
import {ChannelRepositoryService} from "../../../repositories/channel-repository.service";
import {SingleChat} from "../../../entities/chat/single-chat";
import {CryptoBuddy} from "../../../services/crypto.service";
import {PostboxPipeService} from "../../../services/pipe/pipe.service";

declare let componentHandler;

@Component({
  selector: 'add-chat',
  templateUrl: 'add-chat.component.html',
  styleUrls: [ 'add-chat.component.scss' ],
})
export class AddChatComponent {

  addBuddiesToExistingChat: boolean = false;
  chat: SingleChat;

  private buddies: Buddy[] = [ ];

  /**
   * The buddies the user selected.
   *
   * @type {Array}
   */
  private selectedBuddies: Buddy[] = [ ];

  /**
   * The name of the chat. Will be provided by an input text
   * in the component.
   */
  private chatName: string;

  private title: string = "Neuen Chat erstellen";

  /**
   * The error message that could be shown below the textfield.
   */
  private errorMessage: string = "Kein Namen angegeben";
  private showError: boolean = false;

  private isCreating: boolean = false;

  private moderated: boolean = false;

  constructor(private buddyRepository: BuddyRepositoryService,
              private dialog: MdlDialogReference,
              private channelRepository: ChannelRepositoryService,
              private pipeService: PostboxPipeService) {

    let existingChat = this.pipeService.get("pb-update-chat");
    if (!!existingChat) {
      this.addBuddiesToExistingChat = true;
      this.chat = existingChat;

      this.title = "Buddy zum Chat hinzufügen";
    } else {
      this.addBuddiesToExistingChat = false;
      this.title = "Neuen Chat erstellen";
    }

    // Load the buddies.
    buddyRepository.findAll()
      .subscribe(buddyList => {
        this.buddies = Array.from(buddyList);

        if (this.addBuddiesToExistingChat) {
          for (let i = this.buddies.length - 1; i >= 0; i--) {
            if (this.chat.isInChat(this.buddies[i])) {
              this.buddies.splice(i, 1);
            }
          }
        }
      });
  }

  /**
   * Will be called by the {@link SelectableListComponent} if the user
   * selects or deselects a buddy.
   *
   * @param listComponent
   */
  onBuddySelected(listComponent: SelectableListComponent): void {
    let buddy = listComponent.key;

    if (listComponent.active) {
      this.selectedBuddies.push(buddy);
    } else {
      let index = this.selectedBuddies.indexOf(buddy);
      this.selectedBuddies.splice(index, 1);
    }

    if ((!this.chatName || this.chatName.length === 0) && this.selectedBuddies.length === 1) {
      this.chatName = this.selectedBuddies[0].name;
    }

    if (this.selectedBuddies.length === 0) {
      this.chatName = null;
    }
  }

  /**
   * Creates a new chat with the selected buddies and closes the dialog.
   */
  createChat(): void {
    if (this.isCreating) {
      return;
    }

    this.showError = false;
    this.errorMessage = "";

    if (!this.addBuddiesToExistingChat && (!this.chatName || this.chatName.trim().length === 0)) {
      // There is no name provided for the chat.
      this.errorMessage = "Du hast keinen Namen für den Chat angegeben.";
      this.showError = true;
      return;
    }

    if (this.selectedBuddies.length === 0) {
      // No buddies have been selected.
      this.errorMessage = "Du hast keine Buddies für den Chat ausgewählt";
      this.showError = true;
      return;
    }

    this.isCreating = true;

    if (this.addBuddiesToExistingChat) {
      // Add buddies to existing chat.

      let channel = this.chat.channel;
      let promises = [ ];
      for (let buddy of this.selectedBuddies) {
        // add the buddies to the channel
        promises.push(this.channelRepository.addBuddyToChannel(channel.id, buddy));
        this.chat.buddies.push(buddy);
      }

      Promise.all(promises)
        .then(() => {
            this.isCreating = false;
            this.dialog.hide();
        });

    } else {
      // Create the chat.
      this.channelRepository.createNewChatChannel(this.selectedBuddies, this.chatName, this.moderated)
        .then(() => {
          this.isCreating = false;
          this.dialog.hide();
        });
    }
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.dialog.hide();
  }
}
