/**
 * Not really an ai. But will provide answers.
 */
export class MockChatAI {

  private defaultAnswer = "42";

  private intents = {
    greetings: [
      "hi", "hallo", "moin", "guden"
    ],
    smalltalkHealth: [
      "wie gehts", "alles fit", "alles gut"
    ],
    smalltalk: [
      "was machst du", "was geht"
    ]
  };

  private results = {
    greetings: [
      "Hallo", "Hi", "Moin", "Guden"
    ],
    smalltalkHealth: [
      "Mir gehts gut. Und dir so?", "Alles super. Wie gehts dir?"
    ],
    smalltalk: [
      "Bin gerade am lernen.", "Netflix."
    ]
  };


  public getAnswer(question: string): string {
    question = question.toLowerCase().trim();

    let availableIntents = Object.keys(this.intents);
    for (let availableIntent of availableIntents) {
      for (let i = 0; i < this.intents[availableIntent].length; i++) {
        let intent = this.intents[availableIntent][i];
        if (question.indexOf(intent) !== -1) {
          // yeah man.
          return this.getRandomAnswer(availableIntent);
        }
      }
    }

    return this.defaultAnswer;
  }

  private getRandomAnswer(intent: string) {
    let answers = this.results[intent];
    let random = Math.floor(Math.random() * (answers.length));

    console.log(random);
    console.log(answers);
    console.log(intent);


    return answers[random];
  }

}
