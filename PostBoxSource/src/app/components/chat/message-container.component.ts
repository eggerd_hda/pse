import {Component, Input, SimpleChange} from "@angular/core";
import {Message} from "../../entities/chat/message";
import {SingleChat} from "../../entities/chat/single-chat";
import {MomentService} from "../../services/moment/moment.service";

@Component({
  selector: 'message-container',
  templateUrl: 'message-container.component.html',
  styleUrls: [ 'message-container.component.scss' ],
})
export class MessageContainerComponent {

  /**
   * The messages that should be displayed.
   * @type {Array}
   */
  @Input() messages: Message[] = [ ];


  constructor(private momentService: MomentService) {  }

  ngOnChanges(changes: {[propertyKey: string]: SimpleChange}): void {
    for (let change in changes) {
      if ('messages' === change) {
        // There was a message change. Scroll to bottom.

        // We need a timeout, because at this point, the messages
        // are not yet added to the DOM.
        setTimeout(() => {
          let objDiv = document.getElementById("pb-chat-messages");
          objDiv.scrollTop = objDiv.scrollHeight;
        }, 100);
      }
    }
  }

  private formatDate(date): string {
    return this.momentService.toHumanReadableString(date);
  }
}
