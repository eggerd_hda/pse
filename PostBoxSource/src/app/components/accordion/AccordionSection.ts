
import {Buddy} from "../../entities/buddy";
export class AccordionSection {
  public isClosed = false;
  public calculatedHeight: number = 0;


  constructor(public header: string,
              public buddies: Buddy[] = [ ]) {
    this.calculatedHeight = buddies.length * 56;
  }


  public add(buddy: Buddy): void {
    this.buddies.push(buddy);
    // the 56 is the height of a single list element
    this.calculatedHeight += 56;
  }
}
