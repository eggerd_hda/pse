import {Component, Input, SimpleChange, EventEmitter} from '@angular/core';
import {Buddy} from "../../entities/buddy";
import {AccordionSection} from "./AccordionSection";
import {Router} from "@angular/router";
import {ChannelRepositoryService} from "../../repositories/channel-repository.service";

@Component({
  selector: 'buddy-accordion',
  styleUrls: ['buddy-list.component.scss'],
  templateUrl: 'buddy-list.component.html',
})
export class AccordionComponent {

  @Input()
  private sections: AccordionSection[] = [ ];

  constructor(private router: Router, private channelRepo: ChannelRepositoryService) {}

  ngOnInit(): void {
  }

  public createSection(name: string, buddies: Buddy[] = [ ]): AccordionSection {
    let section = new AccordionSection(name, buddies);
    this.sections.push(section);
    return section;
  }

  private toggleSection(section: AccordionSection): void {
    section.isClosed = !section.isClosed;
  }

  private onBuddySelect(buddy: Buddy): void {
    /// TEMP //
   //this.channelRepo.createNewChatChannel([buddy],"TestChat");
    /////////

    this.router.navigate(['/user', buddy.id]);
  }
}
