import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";
import {Post} from "../../../entities/post";
import {User} from "../../../entities/interfaces/user.interface";
import {BuddyRepositoryService} from "../../../repositories/buddy-repository.service";
import {ProfileRepositoryService} from "../../../repositories/profile-repository.service";
import {MomentService} from "../../../services/moment/moment.service";

@Component({
  selector: 'app-post',
  templateUrl: 'post.component.html',
  styleUrls: [ 'post.component.scss'],
})
export class PostComponent {
  @Input()
  postIn: Post;

  @Input()
  showCreator: boolean = true;

  @Input()
  showOptions: boolean = false;

  @Input()
  type: number;

  isImage: boolean = false;
  isText: boolean =  false;

  private creator: User;

  constructor(private router: Router,
              private buddyRepo: BuddyRepositoryService,
              private profileRepo: ProfileRepositoryService,
              private momentService: MomentService) {}

  ngOnInit(): void {
    if (this.type === 1) {
      this.isText = true;
    }else {
      this.isImage = true;
    }

    if (this.showCreator) {
      this.buddyRepo.findAll().subscribe((buddies) => {
        this.creator = buddies.filter(buddy => {
          return buddy.id === this.postIn.creatorID;
        })[0];
      }, err => {
        console.log("Did not find buddy with id", this.postIn.creatorID, err);
      });
    } else {
      this.creator = this.profileRepo.profile;
    }

  }

  onPageSelect(): void {
    this.router.navigate(['/user', this.postIn.creatorID]);
  }


  formatDate(original): string {
    return this.momentService.toDateTimeString(original);
  }
}
