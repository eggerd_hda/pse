import { Component } from '@angular/core';
import {Post} from '../../entities/post';
import {Input} from '@angular/core/src/metadata/directives';
import {MomentService} from "../../services/moment/moment.service";

/**
 * The wall is written in a ways as that is has no clue about the actual posts
 * or whom they are associated with.
 *
 * Just displays the posts. The page the wall is displayed in needs to
 * see which posts should be loaded.
 */
@Component({
  selector: 'app-wall',
  templateUrl: 'wall.component.html',
  styleUrls: [ 'wall.component.scss' ],
})
export class WallComponent {

  /**
   * The posts this wall should display.
   */
  @Input() posts: Post[] = [];

  @Input() showCreator: boolean = true;

  constructor() {}
}
