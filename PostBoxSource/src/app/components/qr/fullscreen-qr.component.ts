import {Component, Input, EventEmitter, Output} from "@angular/core";
@Component({
  selector: 'fullscreen-qr',
  templateUrl: 'fullscreen-qr.component.html',
  styleUrls: [ 'fullscreen-qr.component.scss' ],
})
export class FullscreenQRComponent {
  @Input() qrData: string = "";
  @Input() active: boolean = false;


  @Output() clicked = new EventEmitter();

  private closeQRCode(): void {
    this.clicked.emit();
  }

}
