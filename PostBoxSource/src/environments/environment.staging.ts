export const environment = {
  maximumBuddyPollingFrequencyInMs: (1000 * 4),
  minimumBuddyPollingFrequencyInMs: (1000 /  4),
  multiplicationRateOfExponentialPollingTimeout: 1.5,
  production: false,
  PUBLIC_NODE: "db971316d4b51d1b", // cryptoService.filenameHash() for 'public-node'
  PRIVATE_NODE: '7775fedc68ed3f0a', // cryptoService.filenameHash() for 'private-node'
  currentTestChannelID: 'MyFancyChannelID',
};
