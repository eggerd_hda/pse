// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  maximumBuddyPollingFrequencyInMs: (1000 * 4),
  minimumBuddyPollingFrequencyInMs: (1000 /  4),
  multiplicationRateOfExponentialPollingTimeout: 1.5,
  production: false,
  PUBLIC_NODE: "db971316d4b51d1b", // cryptoService.filenameHash() for 'public-node'
  PRIVATE_NODE: '7775fedc68ed3f0a', // cryptoService.filenameHash() for 'private-node'
  currentTestChannelID: 'MyFancyChannelID',
};
