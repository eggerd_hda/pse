
# Project Setup

## Installation
+ Update Node 
  + if already installed
	  + sudo npm install -g n
	  + sudo n stable
	 + if not
	  + Install Node from Browser
	  + Install Git from Browser
+ Update npm if already installed
	+ npm install -g npm@latest
+ Install Angular-CLI
	+ npm install -g angular-cli tslint typescript
+ Activate tslint
  + Settings -> search for tslint
  + Enable tslint
  
### Docker
+ Install requirements
  + [docker](https://docker.com/)
  + [docker-compose](https://docs.docker.com/compose/)
+ Pull container from gitlab registry and start it
  + `docker-compose up`
+ Move `docker-compose.override.yml.dist` to `docker-compose.override.yml` and edit for your needs
  + with the override file activated, the docker container is build locally with `docker-compose up --build`
  
## First time project-setup
+ Clone the repository:
	+ git clone https://gitlab.com/eggerd_hda/pse.git
+ Switch to develop branch and download the source
	+ git checkout develop
+ Install node modules
	+ npm install
+ Run it
	+ ng serve

## Developing a new feature
+ Creating new branch
	+ git checkout -b [myfeature] develop
+ Merging it
	+ git checkout develop
		+ switch to develop branch
	+ git merge --no-ff [myfeature]
		+ merge branch without removing history
	+ git branch -d [myfeature]
		+ delete branch
	+ git push origin develop
		+ push changes
	
## Overall commands
	
+ Pull develop source
	+ git pull origin develop
+ Run tests
	+ ng test


## Links
Angular-CLI Referenz: https://cli.angular.io/reference.pdf
Branching-Model: http://nvie.com/posts/a-successful-git-branching-model/#




# PostBoxSource

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.18.



## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
