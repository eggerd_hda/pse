const https = require('https');
const express = require('express');
const fs = require('fs');
const path = require('path');
const url = require('url');
const app = express();

const options = {
    key: fs.readFileSync(path.join(__dirname, 'ssl/server.key')),
    cert: fs.readFileSync(path.join(__dirname, 'ssl/server.crt')),
    ca: fs.readFileSync(path.join(__dirname, 'ssl/rootCA.crt')),
    requestCert: true,
    rejectUnauthorized: false
};

module.exports = {
    start() {
        return new Promise((resolve, reject) => {
            const listener = https.createServer(options, app).listen(5000, '127.0.0.1', () => {
                /**
                 * Serve single page application on all routes
                 */
                app.use('/', express.static(path.join(__dirname, '/../dist')));
                app.get('*', (req, res) => {
                    res.sendFile(path.join(__dirname, '/../dist/index.html'));
                });
                let uri = url.format({
                    protocol: 'https',
                    hostname: listener.address().address,
                    port: listener.address().port
                });
                resolve(uri);
            });
            if (!listener) {
                reject();
            }
        });
    }
};